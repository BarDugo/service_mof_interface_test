package com.novidea.mof.calculations;

import java.util.ArrayList;
import java.util.List;

import org.joda.time.DateTime;
import org.joda.time.Months;

import com.novidea.mof.calculations.result.GemelResult;
import com.novidea.mof.calculations.result.GeneralResult;
import com.novidea.mof.calculations.result.InsuranceResult;
import com.novidea.mof.calculations.result.PensionResult;
import com.novidea.mof.entities.model.Fund;

/**
 * 
 * @author Galil.Zussman
 * @date Aug 17, 2014
 */
public class CalculationExecuter {

	// TODO: global parameter for this app rather to retrieve EOL funds, change WS parameter? 
	public static final boolean EXCLUDE_EOL_FUNDS = true;

	/**
	 * 
	 * @param funds
	 * @param investmentRiskLevel - null if not filtered 
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public static List<GemelResult> getGemelResults(List<Fund> funds,
			Integer investmentRiskLevel) {
		List<? extends GeneralResult> results = new ArrayList<GemelResult>();
		List<GeneralResult> generalResults = GemelResultsBuilder
				.getGemelResults(funds, investmentRiskLevel);
		results = generalResults;
		return (List<GemelResult>) results;

	}

	public static List<PensionResult> getPensionResults(List<Fund> funds,
			Integer investmentRiskLevel) {
		List<PensionResult> pensionResults = PensionResultsBuilder
				.getPensionResults(funds, investmentRiskLevel);
		return pensionResults;
	}

	@SuppressWarnings("unchecked")
	public static List<InsuranceResult> getInsuranceResults(List<Fund> funds,
			Integer investmentRiskLevel) {
		List<? extends GeneralResult> results = new ArrayList<PensionResult>();
		List<GeneralResult> generalResults = InsuranceResultsBuilder
				.getInsuranceResults(funds, investmentRiskLevel);
		results = generalResults;
		return (List<InsuranceResult>) results;
	}

	/**
	 * 
	 * @param fund
	 *            - the fund to calculate the 'VETEK_SHANIM' based on
	 *            'TAARICH_HAKAMA'
	 * @return String.valueOf(roundOff(MonthsDiff/12))
	 */
	static String formulaTaarichHakama(Fund fund) {
		if (fund.getTAARICH_HAKAMA() != null) {
			Months diff = Months.monthsBetween(new DateTime(), new DateTime(
					fund.getTAARICH_HAKAMA()));
			double roundOff = (double) Math.round(-1 * diff.getMonths() / 12
					* 100) / 100;
			return String.valueOf(roundOff);
		} else
			return "";
	}

	/**
	 * formula: STIAT_TEKEN Sqrt(sum(i) = 1..n( TSUA_NOMINALIT_BRUTO_HODSHIT(i)
	 * – avg(TSUA_NOMINALIT_BRUTO_HODSHIT) )^2 /(n-1) ) where
	 * avg(TSUA_NOMINALIT_BRUTO_HODSHIT) is gemelResult.TSUA_MEMUZAAT_LATKUFA
	 * 
	 * @param funds
	 * @param itemsAmount
	 * @param tracker
	 * @param tsuaMemuzaatLatkufa
	 * @return
	 */
	static double formulaStiatTeken(List<Fund> funds, int itemsAmount,
			int tracker, double tsuaMemuzaatLatkufa) {

		int firstItem = tracker - itemsAmount;
		double sum = 0;
		for (int i = 0; i < itemsAmount; i++) {
			double tsuaHodshit = funds.get(i + firstItem).getTSUA_HODSHIT() == null ? 0
					: funds.get(i + firstItem).getTSUA_HODSHIT();
			double val = (tsuaHodshit - tsuaMemuzaatLatkufa)
					* (tsuaHodshit - tsuaMemuzaatLatkufa);
			sum += val;
			// System.out.print(tsuaHodshit + "\n");
		}
		double retVal = Math.sqrt(sum / itemsAmount);
		return retVal;
	}

	/**
	 * formula TSUA_MIZTBERET
	 * 
	 * @param miztLast
	 * @param tsuaHodshitCurrent
	 * @return
	 */
	public static double formulaTsuaMiztberet(double miztLast,
			double tsuaHodshitCurrent) {
		return (1 + miztLast) * (1 + tsuaHodshitCurrent / 100) - 1;
	}

	/**
	 * formula תשואה מצטברת בניכוי דמי ניהול
	 * (TSUA_NOMINALIT_BRUTO_HODSHITi+1)*(תשואה נטו מצטברת לחודש קודם+1)*(1-
	 * SHIUR_DMEI_NIHUL_AHARONi/12)-1
	 * 
	 * @param miztLast
	 * @param tsuaHodshitCurrent
	 * @return
	 */
	static double formulaTsuaMiztaberetDmeiNihulLatkufa(double miztLast,
			double tsuaHodshitCurrent, double shiurDmeiNihulAharoni) {
		return (1 + miztLast) * (1 + tsuaHodshitCurrent)
				* (1 - shiurDmeiNihulAharoni / 12) - 1;
	}

}
