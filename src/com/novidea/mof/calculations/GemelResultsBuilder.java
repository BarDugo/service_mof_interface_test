package com.novidea.mof.calculations;

import java.util.ArrayList;
import java.util.List;

import com.novidea.mof.Log;
import com.novidea.mof.MofProperties;
import com.novidea.mof.calculations.cache.AbstractCachedReport;
import com.novidea.mof.calculations.cache.GemelCachedReport;
import com.novidea.mof.calculations.result.CalculationRecord;
import com.novidea.mof.calculations.result.GemelResult;
import com.novidea.mof.calculations.result.GeneralResult;
import com.novidea.mof.calculations.result.LastMonthResult;
import com.novidea.mof.entities.ReportType;
import com.novidea.mof.entities.model.AssetFullDetailedReport;
import com.novidea.mof.entities.model.AssetMainGroupDetailedReport;
import com.novidea.mof.entities.model.Fund;

/**   
 * @author Galil.Zussman
 * @date   Dec 15, 2014
 */
public class GemelResultsBuilder {
	/*
	 * if TAAGID_SHOLET then 'true', if SHM_HEVRA_MENAHELET then false
	 */
	final static boolean isUSE_TAAGID = false;

	static Double valueOrZero(Double value) {
		return value == null ? 0 : value;
	}

	public static List<GeneralResult> getGemelResults(
			List<Fund> funds, Integer investmentRiskLevel) {

		if (funds == null || funds.size() == 0)
			return null;
		List<GeneralResult> generalResults = new ArrayList<GeneralResult>();

		try {

			int itemsAmount = 0, tracker = 0;
			int currentID = funds.get(0).getID();
			String taarichHakama = "";
			String tkufatDivuach = "";
			double tzviraNeto = 0;
			double tsuaMemuzaatLatkufa = 0;

			double tsuaLatkufaThisMonth = 0;
			double tsuaMiztaberetLatkufa = 0;
			double tsuaMiztaberetDmeiNihulLatkufa = 0;

			//StringBuffer sbResultReturnData = new StringBuffer();
			List<CalculationRecord> calculationRecordList = new ArrayList<CalculationRecord>();

			double sharpLatkufa = 0;
			double hekefMil = 0;

			//	List<Double> tsuaaData = new ArrayList<Double>();
			Fund monthlyFundItem = funds.get(0);

			GeneralResult generalResult = null;

			generalResult = new GeneralResult(monthlyFundItem.getID(),
					monthlyFundItem.getHODESH_DIVUACH(),
					monthlyFundItem.getSUG(), monthlyFundItem.getSHEM(),
					isUSE_TAAGID ? monthlyFundItem.getSHM_TAAGID_SHOLET()
							: monthlyFundItem.getSHM_HEVRA_MENAHELET());

			// funds have to be ordered by ID for this to work!
			for (Fund fund : funds) {
				monthlyFundItem = funds.get(tracker);
				tsuaLatkufaThisMonth = valueOrZero(monthlyFundItem
						.getTSUA_HODSHIT());

				if (!fund.getID().equals(currentID)) {

					//	gemelResult.tsuaaData = tsuaaData;
					generalResult.HEKEF_MIL = hekefMil;
					// SHARP_LATKUFA
					generalResult.SHARP_LATKUFA = sharpLatkufa;
					generalResult.TSUA_MIZTABERET_LATKUFA = tsuaMiztaberetLatkufa * 100;
					// SUM
					// TZVIRA_NETO
					generalResult.TZVIRA_NETO = Double.valueOf(tzviraNeto);
					// AVG
					// TSUA_MEMUZAAT_LATKUFA
					generalResult.TSUA_MEMUZAAT_LATKUFA = Double
							.valueOf(tsuaMemuzaatLatkufa / itemsAmount);

					// STIAT_TEKEN_LATKUFA
					generalResult.STIAT_TEKEN_LATKUFA = CalculationExecuter
							.formulaStiatTeken(funds, itemsAmount, tracker,
									generalResult.TSUA_MEMUZAAT_LATKUFA);
					// VETEK_SHANIM
					generalResult.VETEK_SHANIM = taarichHakama;
					// TSUA_MIZTABERET_DMEI_NIHUL_LATKUFA
					generalResult.TSUA_MIZTABERET_DMEI_NIHUL_LATKUFA = tsuaMiztaberetDmeiNihulLatkufa * 100;

					generalResult.periodicData = calculationRecordList;

					///////////////!!!!!!!!!!!!!!*******************!!!!!!!!!!!!!!!!!!!///////////////
					generalResult.TKUFAT_DIVUACH = tkufatDivuach;
					///////////////!!!!!!!!!!!!!!*******************!!!!!!!!!!!!!!!!!!!///////////////

					//TODO: FIX LIST
					/*
					gemelResult.periodicDataJsonStr = "["
							+ sbResultReturnData.toString()
									.substring(
											0,
											sbResultReturnData.toString()
													.length() - 1) + "]";
					*/

					// conditions to return this result
					if (addGemelLastMonthReportData(generalResult)) // have last month report - fund EOL
						if (investmentRiskLevel == null
								|| investmentRiskLevel.equals(0))
							generalResults.add(generalResult);
						else if (generalResult.RISK_LEVEL == null
								|| generalResult.RISK_LEVEL
										.contains(investmentRiskLevel
												.toString())) // will work only if investmentRiskLevel is numbered (i.e "דרגת סיכון 2")
							generalResults.add(generalResult);

					/* start a new result and reset all temporal */
					currentID = fund.getID();
					itemsAmount = 0;
					tzviraNeto = 0;
					tsuaMemuzaatLatkufa = 0;
					hekefMil = 0;
					sharpLatkufa = 0;
					tsuaMiztaberetDmeiNihulLatkufa = 0;
					taarichHakama = "";

					tkufatDivuach = "";
					//	tsuaaData = new ArrayList<Double>();
					//TODO: FIX LIST
					//sbResultReturnData = new StringBuffer();
					calculationRecordList = new ArrayList<CalculationRecord>();

					tsuaLatkufaThisMonth = valueOrZero(monthlyFundItem
							.getTSUA_HODSHIT());

					generalResult = new GeneralResult(monthlyFundItem.getID(),
							monthlyFundItem.getHODESH_DIVUACH(),
							monthlyFundItem.getSUG(), funds.get(tracker)
									.getSHEM(), isUSE_TAAGID ? funds.get(
									tracker).getSHM_TAAGID_SHOLET() : funds
									.get(tracker).getSHM_HEVRA_MENAHELET());

				}
				// TZVIRA_NETO
				tzviraNeto += valueOrZero(fund.getTZVIRA_NETO());
				// TSUA_LATKUFA
				tsuaMemuzaatLatkufa += valueOrZero(fund.getTSUA_HODSHIT());
				// SHARP_LATKUFA
				sharpLatkufa = valueOrZero(fund.getSHARP_RIBIT_HASRAT_SIKUN());
				// HEKEF_MIL
				hekefMil = valueOrZero(fund.getYITRAT_NCHASIM_LSOF_TKUFA());

				// VETEK_SHANIM
				taarichHakama = CalculationExecuter.formulaTaarichHakama(fund);

				double shiurDmeiNihul_aharon = valueOrZero(fund
						.getSHIUR_DMEI_NIHUL_AHARON());

				// TSUA_MIZTABERET_DMEI_NIHUL_LATKUFA
				tsuaMiztaberetDmeiNihulLatkufa = CalculationExecuter
						.formulaTsuaMiztaberetDmeiNihulLatkufa(
								tsuaMiztaberetDmeiNihulLatkufa,
								tsuaLatkufaThisMonth / 100,
								shiurDmeiNihul_aharon / 100);
				///////////////!!!!!!!!!!!!!!*******************!!!!!!!!!!!!!!!!!!!///////////////
				tkufatDivuach = fund.getTKUFAT_DIVUACH();
				///////////////!!!!!!!!!!!!!!*******************!!!!!!!!!!!!!!!!!!!///////////////

				//    tsuaaData.add(tsuaLatkufaThisMonth);

				// מצטברת
				if (itemsAmount == 0) {// first in the group

					// TSUA_MIZTABERET_LATKUFA
					tsuaMiztaberetLatkufa = tsuaLatkufaThisMonth / 100;
				} else {
					// TSUA_MIZTABERET_LATKUFA
					tsuaMiztaberetLatkufa = CalculationExecuter
							.formulaTsuaMiztberet(tsuaMiztaberetLatkufa,
									tsuaLatkufaThisMonth);
				}
				CalculationRecord calculationRecord = new CalculationRecord();
				calculationRecord.tsua = tsuaLatkufaThisMonth;
				calculationRecord.tsuaMiztaberetLatkufa = tsuaMiztaberetLatkufa;
				calculationRecord.tsuaMiztaberetDmeiNihul = tsuaMiztaberetDmeiNihulLatkufa;
				calculationRecord.date = monthlyFundItem.getHODESH_DIVUACH();
				calculationRecordList.add(calculationRecord);
				//TODO: FIX LIST
				/*
				sbResultReturnData.append("{\"tsua\":"
						+ tsuaLatkufaThisMonth
						+ ",\"tsuaMiztaberetLatkufa\":"
						+ tsuaMiztaberetLatkufa
						+ ",\"tsuaMiztaberetDmeiNihul\":"
						+ tsuaMiztaberetDmeiNihulLatkufa
						+ ",\"date\": \""
						+ monthlyFundItem.getHODESH_DIVUACH().toString()
								.substring(0, 7) + "\"},");
				 */
				itemsAmount++;
				tracker++;
			}

			//	gemelResult.tsuaaData = tsuaaData;
			generalResult.HEKEF_MIL = hekefMil;
			// SHARP_LATKUFA
			generalResult.SHARP_LATKUFA = sharpLatkufa;
			// Miztaberet
			generalResult.TSUA_MIZTABERET_LATKUFA = tsuaMiztaberetLatkufa * 100;
			// SUM
			// TZVIRA_NETO
			generalResult.TZVIRA_NETO = Double.valueOf(tzviraNeto);
			// AVG
			// TSUA_MEMUZAAT_LATKUFA
			generalResult.TSUA_MEMUZAAT_LATKUFA = Double
					.valueOf(tsuaMemuzaatLatkufa / itemsAmount);

			// STIAT_TEKEN_LATKUFA
			generalResult.STIAT_TEKEN_LATKUFA = CalculationExecuter
					.formulaStiatTeken(funds, itemsAmount, tracker,
							generalResult.TSUA_MEMUZAAT_LATKUFA);
			// VETEK_SHANIM
			generalResult.VETEK_SHANIM = taarichHakama;
			// TSUA_MIZTABERET_DMEI_NIHUL_LATKUFA
			generalResult.TSUA_MIZTABERET_DMEI_NIHUL_LATKUFA = tsuaMiztaberetDmeiNihulLatkufa * 100;
			generalResult.periodicData = calculationRecordList;

			///////////////!!!!!!!!!!!!!!*******************!!!!!!!!!!!!!!!!!!!///////////////
			generalResult.TKUFAT_DIVUACH = tkufatDivuach;
			///////////////!!!!!!!!!!!!!!*******************!!!!!!!!!!!!!!!!!!!/////////////////TODO: FIX LIST

			/*
			gemelResult.periodicDataJsonStr = "["
					+ sbResultReturnData.toString().substring(0,
							sbResultReturnData.toString().length() - 1)
					+ "]";
			 */

			// conditions to return this result
			if (addGemelLastMonthReportData(generalResult)) // don't have last month report - fund EOL
				if (investmentRiskLevel == null
						|| investmentRiskLevel.equals(0))
					generalResults.add(generalResult);
				else if (generalResult.RISK_LEVEL == null
						|| generalResult.RISK_LEVEL
								.contains(investmentRiskLevel.toString()))
					generalResults.add(generalResult);

			return generalResults;
		} catch (Exception e) {
			Log.error("Got exception in CalculationExecuter.getGemelResults, Error: "
					+ e);
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * see {@link GemelResult} for group mapping
	 * @param generalResult
	 */
	private static boolean addGemelLastMonthReportData(
			GeneralResult generalResult) {

		AbstractCachedReport reportHolder = GemelCachedReport.getInstance();

		generalResult.GROUP_1 = 0.0;
		generalResult.GROUP_2 = 0.0;
		generalResult.GROUP_3 = 0.0;
		generalResult.GROUP_4 = 0.0;
		generalResult.GROUP_5 = 0.0;
		generalResult.GROUP_6 = 0.0;
		generalResult.GROUP_7 = 0.0;
		generalResult.GROUP_8 = 0.0;
		generalResult.GROUP_9 = 0.0;
		generalResult.GROUP_10 = 0.0;
		generalResult.GROUP_11 = 0.0;
		generalResult.GROUP_12 = 0.0;
		generalResult.GROUP_13 = 0.0;
		generalResult.GROUP_14 = 0.0;

		double deltaHelper[] = { 0.0, 0.0 };
		Integer[] indexes = reportHolder
				.getIdToIndexesMap()
				.get(ReportType.GEMEL_ASSETS_MAIN_GROUP_DETAILED_REPORT
						.getValue()).get(generalResult.ID_OTZAR);
		if (indexes != null) {
			for (int i = indexes[0]; i <= indexes[1]; i++) {
				AssetMainGroupDetailedReport report = reportHolder
						.getAssetMainGroupDetailedReports().get(i);
				if (report.getID_SUG_NECHES().equals(4701))
					generalResult.GROUP_1 = valueOrZero(report
							.getACHUZ_SUG_NECHES());

				// first assign, then subtract from the ASSET_FULL_DETAILED_REPORTS items sum 
				if (report.getID_SUG_NECHES().equals(4703))
					generalResult.GROUP_2 = valueOrZero(report
							.getACHUZ_SUG_NECHES());

				// first assign, then subtract from the ASSET_FULL_DETAILED_REPORTS items sum 
				if (report.getID_SUG_NECHES().equals(4704))
					generalResult.GROUP_14 = valueOrZero(report
							.getACHUZ_SUG_NECHES());

				if (report.getID_SUG_NECHES().equals(4751)) {
					deltaHelper[0] = valueOrZero(report.getACHUZ_SUG_NECHES());
					generalResult.GROUP_3 = valueOrZero(report
							.getACHUZ_SUG_NECHES());
				}

				if (report.getID_SUG_NECHES().equals(4705)) {
					deltaHelper[1] = valueOrZero(report.getACHUZ_SUG_NECHES());
					generalResult.GROUP_13 = valueOrZero(report
							.getACHUZ_SUG_NECHES());
				}

				if (report.getID_SUG_NECHES().equals(4709))
					generalResult.GROUP_5 = valueOrZero(report
							.getACHUZ_SUG_NECHES());

				if (report.getID_SUG_NECHES().equals(4710))
					generalResult.GROUP_6 = valueOrZero(report
							.getACHUZ_SUG_NECHES());

				if (report.getID_SUG_NECHES().equals(4706)
						|| report.getID_SUG_NECHES().equals(4708))
					generalResult.GROUP_7 += valueOrZero(report
							.getACHUZ_SUG_NECHES());

				if (report.getID_SUG_NECHES().equals(4707))
					generalResult.GROUP_8 = valueOrZero(report
							.getACHUZ_SUG_NECHES());

				if (report.getID_SUG_NECHES().equals(4752))
					generalResult.GROUP_9 = valueOrZero(report
							.getACHUZ_SUG_NECHES());

				if (report.getID_SUG_NECHES().equals(4761))
					generalResult.GROUP_10 = valueOrZero(report
							.getACHUZ_SUG_NECHES());

				if (report.getID_SUG_NECHES().equals(4721))
					generalResult.GROUP_11 = valueOrZero(report
							.getACHUZ_SUG_NECHES());

				if (report.getID_SUG_NECHES().equals(4722))
					generalResult.GROUP_12 = valueOrZero(report
							.getACHUZ_SUG_NECHES());
			}
		}

		Double group2helper = 0.0;
		Double group14helper = 0.0;

		indexes = reportHolder.getIdToIndexesMap()
				.get(ReportType.GEMEL_ASSETS_FULL_DETAILED_REPORT.getValue())
				.get(generalResult.ID_OTZAR);
		if (indexes != null) {
			for (int i = indexes[0]; i <= indexes[1]; i++) {
				AssetFullDetailedReport report = reportHolder
						.getAssetFullDetailedReports().get(i);
				if (report.getID_NATUN().equals(5513)
						|| report.getID_NATUN().equals(5514)
						|| report.getID_NATUN().equals(5515)
						|| report.getID_NATUN().equals(5549)
						|| report.getID_NATUN().equals(5550)
						|| report.getID_NATUN().equals(5551)
						|| report.getID_NATUN().equals(7147)
						|| report.getID_NATUN().equals(7156)
						|| report.getID_NATUN().equals(7174)
						|| report.getID_NATUN().equals(7186)) {

					//int idNatun = report.getID_NATUN();
					double val = valueOrZero(report.getACHUZ());
					//System.out.println("ID: " + report.getID() + "\tidNatun: " + idNatun + "\tval: " + val);
					generalResult.GROUP_4 += val;

				}

				if (report.getID_NATUN().equals(5513)
						|| report.getID_NATUN().equals(5514)
						|| report.getID_NATUN().equals(5515)
						|| report.getID_NATUN().equals(7147)
						|| report.getID_NATUN().equals(7156))
					group2helper += valueOrZero(report.getACHUZ());

				if (report.getID_NATUN().equals(5549)
						|| report.getID_NATUN().equals(5550)
						|| report.getID_NATUN().equals(5551)
						|| report.getID_NATUN().equals(7174)
						|| report.getID_NATUN().equals(7186))
					group14helper += valueOrZero(report.getACHUZ());

				//System.out.println("gemelResult.GROUP_4: "	+ gemelResult.GROUP_4);
			}
		}

		if (reportHolder.getLastMonthResults() != null) {
			LastMonthResult lastMonthResult = reportHolder
					.getLastMonthResults().get(generalResult.ID_OTZAR);
			if (lastMonthResult != null) {
				generalResult.lastMonthResult = lastMonthResult;
				generalResult.TSUA_MITZT_MI_THILAT_SHANA = lastMonthResult.TSUA_MITZT_MI_THILAT_SHANA;
				generalResult.TSUA_SHNATIT_MEMUZAAT_3_SHANIM = lastMonthResult.TSUA_SHNATIT_MEMUZAAT_3_SHANIM;
			} else {//Missing ReportHolder.gemelLastMonthResults for fund: (gemelResult.ID_OTZAR)
				String msg = "Missing ReportHolder.gemelLastMonthResults for fund: "
						+ generalResult.ID_OTZAR;
				Log.warn(msg);
				if (CalculationExecuter.EXCLUDE_EOL_FUNDS)
					return false;
			}
		}

		generalResult.GROUP_2 -= group2helper;
		generalResult.GROUP_14 -= group14helper;
		//gemelResult.GROUP_2 = gemelResult.GROUP_2 - gemelResult.GROUP_4;

		generalResult.DELTA = deltaHelper[0] - deltaHelper[1];
		double sum = generalResult.GROUP_3 + generalResult.GROUP_4;
		generalResult.AGACH_RISK = sum;
		if (sum >= 0 && sum < 1)
			generalResult.RISK_LEVEL = MofProperties.get("RISK_LEVEL_1");
		if (sum >= 1 && sum < 15)
			generalResult.RISK_LEVEL = MofProperties.get("RISK_LEVEL_2");
		if (sum >= 15 && sum < 40)
			generalResult.RISK_LEVEL = MofProperties.get("RISK_LEVEL_3");
		if (sum >= 40 && sum < 70)
			generalResult.RISK_LEVEL = MofProperties.get("RISK_LEVEL_4");
		if (sum >= 70)
			generalResult.RISK_LEVEL = MofProperties.get("RISK_LEVEL_5");

		//applyDeltaCalculation(gemelResult);

		return true;
	}

	/**
	 * Apply some calculation on the data - aggregate deduction of the 'delta' to get the right amount 
	 * 
	 * @param gemelResult
	 */
	@SuppressWarnings("unused")
	private static void applyDeltaCalculation(GemelResult gemelResult) {

		if (gemelResult.DELTA < gemelResult.GROUP_5) { // 4709
			gemelResult.GROUP_5 = gemelResult.GROUP_5 - gemelResult.DELTA;
			return;
		} else {
			gemelResult.DELTA = gemelResult.DELTA - gemelResult.GROUP_5;
			gemelResult.GROUP_5 = 0.0;

			if (gemelResult.DELTA < gemelResult.GROUP_6) { // 4710
				gemelResult.GROUP_6 = gemelResult.GROUP_6 - gemelResult.DELTA;
				return;
			} else {

				gemelResult.DELTA = gemelResult.DELTA - gemelResult.GROUP_6;
				gemelResult.GROUP_6 = 0.0;

				if (gemelResult.DELTA < gemelResult.GROUP_7) { // 4706 + 4708
					gemelResult.GROUP_7 = gemelResult.GROUP_7
							- gemelResult.DELTA;
					return;
				} else {
					gemelResult.DELTA = gemelResult.DELTA - gemelResult.GROUP_7;
					gemelResult.GROUP_7 = 0.0;

					if (gemelResult.DELTA < gemelResult.GROUP_8) { // 4707
						gemelResult.GROUP_8 = gemelResult.GROUP_8
								- gemelResult.DELTA;
						return;
					} else {
						gemelResult.DELTA = gemelResult.DELTA
								- gemelResult.GROUP_8;
						gemelResult.GROUP_8 = 0.0;

					}
				}
			}
		}
	}

}
