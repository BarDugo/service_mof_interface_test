package com.novidea.mof.calculations;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.novidea.mof.Log;
import com.novidea.mof.MofProperties;
import com.novidea.mof.calculations.cache.InsuranceCachedReport;
import com.novidea.mof.calculations.result.CalculationRecord;
import com.novidea.mof.calculations.result.GeneralResult;
import com.novidea.mof.calculations.result.LastMonthResult;
import com.novidea.mof.entities.ReportType;
import com.novidea.mof.entities.model.AssetFullDetailedReport;
import com.novidea.mof.entities.model.AssetMainGroupDetailedReport;
import com.novidea.mof.entities.model.Fund;

/**   
 * @author Galil.Zussman
 * @date   Dec 15, 2014
 */
public class InsuranceResultsBuilder {

	@SuppressWarnings("unused")
	public static List<GeneralResult> getInsuranceResults(List<Fund> funds,
			Integer investmentRiskLevel) {
		if (funds == null || funds.size() == 0)
			return null;
		List<GeneralResult> insuranceResults = new ArrayList<GeneralResult>();

		try {

			int itemsAmount = 0, tracker = 0;
			int currentID = funds.get(0).getID();
			String taarichHakama = "";
			String tkufatDivuach = "";
			double tzviraNetoFirst = 0;
			double tzviraNetoLast = 0;
			double tsuaMemuzaatLatkufa = 0;

			double tsuaLatkufaThisMonth = 0;
			double tsuaMiztaberetLatkufa = 0;
			double tsuaMiztaberetDmeiNihulLatkufa = 0;
			Date hodeshDivuach = null;
			//StringBuffer sbResultReturnData = new StringBuffer();
			List<CalculationRecord> calculationRecordList = new ArrayList<CalculationRecord>();

			double sharpLatkufa = 0;
			double hekefMil = 0;

			//	List<Double> tsuaaData = new ArrayList<Double>();
			Fund monthlyFundItem = funds.get(0);

			GeneralResult insuranceResult = null;

			String[] productInfo = getProductAndCompanyNames(monthlyFundItem);

			String SHEM_MUTZAR = productInfo[0];
			String TAAGID = productInfo[1];

			insuranceResult = new GeneralResult(monthlyFundItem.getID(),
					monthlyFundItem.getHODESH_DIVUACH(),
					monthlyFundItem.getTKUFAT_HAKAMA(), SHEM_MUTZAR, TAAGID);

			for (Fund fund : funds) { // funds have to be ordered by ID for this to work!

				monthlyFundItem = funds.get(tracker);
				tsuaLatkufaThisMonth = valueOrZero(monthlyFundItem
						.getTSUA_HODSHIT());

				if (!monthlyFundItem.getID().equals(currentID)) {
					//TZVIRA_NETO
					insuranceResult.TZVIRA_NETO = tzviraNetoLast
							- tzviraNetoFirst;
					// TSUA_MEMUZAAT_LATKUFA
					insuranceResult.TSUA_MEMUZAAT_LATKUFA = Double
							.valueOf(tsuaMemuzaatLatkufa / itemsAmount);
					// TSUA_MIZTABERET_LATKUFA
					insuranceResult.TSUA_MIZTABERET_LATKUFA = tsuaMiztaberetLatkufa * 100;
					// TSUA_MIZTABERET_DMEI_NIHUL_LATKUFA
					insuranceResult.TSUA_MIZTABERET_DMEI_NIHUL_LATKUFA = tsuaMiztaberetDmeiNihulLatkufa * 100;

					// VETEK_SHANIM
					insuranceResult.VETEK_SHANIM = taarichHakama;

					// STIAT_TEKEN_LATKUFA
					insuranceResult.STIAT_TEKEN_LATKUFA = CalculationExecuter
							.formulaStiatTeken(funds, itemsAmount, tracker,
									insuranceResult.TSUA_MEMUZAAT_LATKUFA);

					// SHARP_LATKUFA
					insuranceResult.SHARP_LATKUFA = sharpLatkufa;

					// collected monthly fund record
					insuranceResult.periodicData = calculationRecordList;

					// set  hodeshDivuach
					insuranceResult.HODESH_DIVUACH = hodeshDivuach;

					// Add the accumulating item
					// conditions to return this result
					if (addInsuranceLastMonthReportData(insuranceResult)) // have last month report - fund EOL
						if (investmentRiskLevel == null
								|| investmentRiskLevel.equals(0))
							insuranceResults.add(insuranceResult);
						else if (insuranceResult.RISK_LEVEL == null
								|| insuranceResult.RISK_LEVEL
										.contains(investmentRiskLevel
												.toString())) // will work only if investmentRiskLevel is numbered (i.e "\u05d3\u05e8\u05d2\u05ea \u05e1\u05d9\u05db\u05d5\u05df 2")
							insuranceResults.add(insuranceResult);

					/**
					 *  start a new result and reset all temporal 
					 */
					currentID = monthlyFundItem.getID();
					itemsAmount = 0;
					tzviraNetoFirst = 0;
					tzviraNetoLast = 0;
					tsuaMemuzaatLatkufa = 0;
					tsuaLatkufaThisMonth = 0;
					tsuaMiztaberetDmeiNihulLatkufa = 0;
					taarichHakama = "";
					sharpLatkufa = 0;
					hodeshDivuach = null;

					calculationRecordList = new ArrayList<CalculationRecord>();

					tsuaLatkufaThisMonth = valueOrZero(monthlyFundItem
							.getTSUA_HODSHIT());
					if (monthlyFundItem.getID().equals(8568))
						System.out
								.println("InsuranceResultsBuilder.getInsuranceResults()");
					productInfo = getProductAndCompanyNames(monthlyFundItem);

					SHEM_MUTZAR = productInfo[0];
					TAAGID = productInfo[1];

					insuranceResult = new GeneralResult(
							monthlyFundItem.getID(),
							monthlyFundItem.getHODESH_DIVUACH(),
							monthlyFundItem.getTKUFAT_HAKAMA(), SHEM_MUTZAR,
							TAAGID);
				}
				/**
				 * data accumulation section
				 */
				// TZVIRA_NETO
				tzviraNetoLast = valueOrZero(monthlyFundItem
						.getYIT_NCHASIM_BFOAL());
				// TSUA_LATKUFA
				tsuaMemuzaatLatkufa += valueOrZero(monthlyFundItem
						.getTSUA_HODSHIT());

				// TSUA_MIZTABERET_DMEI_NIHUL_LATKUFA
				double shiurDmeiNihul_aharon = valueOrZero(monthlyFundItem
						.getSHIUR_D_NIHUL_NECHASIM());
				tsuaMiztaberetDmeiNihulLatkufa = CalculationExecuter
						.formulaTsuaMiztaberetDmeiNihulLatkufa(
								tsuaMiztaberetDmeiNihulLatkufa,
								tsuaLatkufaThisMonth / 100,
								shiurDmeiNihul_aharon / 100);

				// VETEK_SHANIM
				taarichHakama = monthlyFundItem.getTKUFAT_HAKAMA();

				// SHARP_LATKUFA
				sharpLatkufa = valueOrZero(monthlyFundItem
						.getSHARP_RIBIT_HASRAT_SIKUN());

				// \u05de\u05e6\u05d8\u05d1\u05e8\u05ea
				if (itemsAmount == 0) {// first in the group

					// TSUA_MIZTABERET_LATKUFA
					tsuaMiztaberetLatkufa = tsuaLatkufaThisMonth / 100;

					tzviraNetoFirst = valueOrZero(monthlyFundItem
							.getYIT_NCHASIM_BFOAL());

				} else {
					// TSUA_MIZTABERET_LATKUFA
					tsuaMiztaberetLatkufa = CalculationExecuter
							.formulaTsuaMiztberet(tsuaMiztaberetLatkufa,
									tsuaLatkufaThisMonth);
				}

				// always set last hodeshDivuach (overriding) 
				hodeshDivuach = monthlyFundItem.getHODESH_DIVUACH();

				// Collect monthly fund record
				CalculationRecord calculationRecord = new CalculationRecord();
				calculationRecord.tsua = tsuaLatkufaThisMonth;
				calculationRecord.tsuaMiztaberetLatkufa = tsuaMiztaberetLatkufa;
				calculationRecord.tsuaMiztaberetDmeiNihul = tsuaMiztaberetDmeiNihulLatkufa;
				calculationRecord.date = monthlyFundItem.getHODESH_DIVUACH();
				calculationRecordList.add(calculationRecord);

				itemsAmount++;
				tracker++;
			}

			//TZVIRA_NETO
			insuranceResult.TZVIRA_NETO = tzviraNetoLast - tzviraNetoFirst;

			// TSUA_MEMUZAAT_LATKUFA
			insuranceResult.TSUA_MEMUZAAT_LATKUFA = Double
					.valueOf(tsuaMemuzaatLatkufa / itemsAmount);
			// TSUA_MIZTABERET_DMEI_NIHUL_LATKUFA
			insuranceResult.TSUA_MIZTABERET_DMEI_NIHUL_LATKUFA = tsuaMiztaberetDmeiNihulLatkufa * 100;
			// Miztaberet
			insuranceResult.TSUA_MIZTABERET_LATKUFA = tsuaMiztaberetLatkufa * 100;
			// STIAT_TEKEN_LATKUFA
			insuranceResult.STIAT_TEKEN_LATKUFA = CalculationExecuter
					.formulaStiatTeken(funds, itemsAmount, tracker,
							insuranceResult.TSUA_MEMUZAAT_LATKUFA);

			// SHARP_LATKUFA
			insuranceResult.SHARP_LATKUFA = sharpLatkufa;

			// collected monthly fund record
			insuranceResult.periodicData = calculationRecordList;

			// VETEK_SHANIM
			insuranceResult.VETEK_SHANIM = taarichHakama;

			// set  hodeshDivuach
			insuranceResult.HODESH_DIVUACH = hodeshDivuach;

			// Add the accumulating item
			// conditions to return this result
			if (addInsuranceLastMonthReportData(insuranceResult)) // have last month report - fund EOL
				if (investmentRiskLevel == null
						|| investmentRiskLevel.equals(0))
					insuranceResults.add(insuranceResult);
				else if (insuranceResult.RISK_LEVEL == null
						|| insuranceResult.RISK_LEVEL
								.contains(investmentRiskLevel.toString())) // will work only if investmentRiskLevel is numbered (i.e "\u05d3\u05e8\u05d2\u05ea \u05e1\u05d9\u05db\u05d5\u05df 2")
					insuranceResults.add(insuranceResult);

			return insuranceResults;
		} catch (Exception e) {
			Log.error("Got exception in CalculationExecuter.getInsuranceResults, Error: "
					+ e);
			e.printStackTrace();
		}
		return null;
	}

	private static String[] getProductAndCompanyNames(Fund monthlyFundItem) {

		InsuranceCachedReport reportHolder = InsuranceCachedReport
				.getInstance();
		String TAAGID = "";
		String SHEM_MUTZAR = "";
		String[] result = new String[] { "", "" };
		String parts[] = monthlyFundItem.getSHEM().split("-");
		if (parts.length == 1)
			SHEM_MUTZAR = TAAGID = monthlyFundItem.getSHEM();
		else if (parts.length == 2) {
			SHEM_MUTZAR = parts[1];
			TAAGID = parts[0];
		} else if (parts.length > 2) {
			TAAGID = parts[0];
			for (int i = 1; i < parts.length; i++)
				if (i != parts.length - 1 && i != 1)
					SHEM_MUTZAR += "-" + parts[i];
				else if (i == 1)
					SHEM_MUTZAR += parts[i];
				else
					SHEM_MUTZAR += " " + parts[i];
		}
		result[0] = SHEM_MUTZAR;
		result[1] = TAAGID;
		//////////////////////////////////////////////////
		// deprecated functionality for TAAGID in insurance type  #1606 #1551 #1615 #1635
		String companyNameStr = reportHolder.getCompanyIdToName().get(
				Integer.valueOf(monthlyFundItem.getID_HEVRA()));
		result[1] = companyNameStr;
		/////////////////////////////////////////////////

		return result;
	}

	private static boolean addInsuranceLastMonthReportData(
			GeneralResult generalResult) {
		InsuranceCachedReport reportHolder = InsuranceCachedReport
				.getInstance();
		Fund lastMonthFund = reportHolder.getLastMonthFundMap().get(
				generalResult.ID_OTZAR);
		/*
		System.out.println(generalResult.ID_OTZAR);
		System.out.println(lastMonthFund);
				if (!generalResult.HODESH_DIVUACH.equals(lastMonthFund
						.getHODESH_DIVUACH()))
					throw new DateMismatchException(
							"last month fund date don't mach result date\n" + "Fund:\n"
									+ lastMonthFund + "\ngeneralResult:\n"
									+ generalResult + "\n");
		*/
		/*
		System.out.println("lastMonthFund ID: " + lastMonthFund.getID() + "\t"
				+ "HODESH_DIVUACH: " + lastMonthFund.getHODESH_DIVUACH() + "\t"
				+ "YIT_NCHASIM_BFOAL: " + lastMonthFund.getYIT_NCHASIM_BFOAL()
				+ "\t");
		*/
		double yitNchasimBfoal = lastMonthFund == null ? 0
				: valueOrZero(lastMonthFund.getYIT_NCHASIM_BFOAL());

		generalResult.HEKEF_MIL = yitNchasimBfoal;

		generalResult.GROUP_1 = 0.0;
		generalResult.GROUP_2 = 0.0;
		generalResult.GROUP_3 = 0.0;
		generalResult.GROUP_4 = 0.0;
		generalResult.GROUP_5 = 0.0;
		generalResult.GROUP_6 = 0.0;
		generalResult.GROUP_7 = 0.0;
		generalResult.GROUP_8 = 0.0;
		generalResult.GROUP_9 = 0.0;
		generalResult.GROUP_10 = 0.0;
		generalResult.GROUP_11 = 0.0;
		generalResult.GROUP_12 = 0.0;
		generalResult.GROUP_13 = 0.0;
		generalResult.GROUP_14 = 0.0;

		Double group2Sum = 0.0;
		Double group3Sum = 0.0;
		Double group5Sum = 0.0;

		Integer[] indexes = reportHolder
				.getIdToIndexesMap()
				.get(ReportType.INSURANCE_ASSETS_FULL_DETAILED_REPORT
						.getValue()).get(generalResult.ID_OTZAR);
		if (indexes != null) {
			for (int i = indexes[0]; i <= indexes[1]; i++) {

				AssetFullDetailedReport report = reportHolder
						.getAssetFullDetailedReports().get(i);
				if (report.getID_NATUN().equals(7110)
						|| report.getID_NATUN().equals(7111)
						|| report.getID_NATUN().equals(7112)
						|| report.getID_NATUN().equals(7139)
						|| report.getID_NATUN().equals(7148))
					group2Sum += valueOrZero(report.getERECH_NATUN());

				if (report.getID_NATUN().equals(7131)
						|| report.getID_NATUN().equals(7132)
						|| report.getID_NATUN().equals(7133))
					group3Sum += valueOrZero(report.getERECH_NATUN());

				if (report.getID_NATUN().equals(7110)
						|| report.getID_NATUN().equals(7111)
						|| report.getID_NATUN().equals(7112)
						|| report.getID_NATUN().equals(7131)
						|| report.getID_NATUN().equals(7132)
						|| report.getID_NATUN().equals(7133)
						|| report.getID_NATUN().equals(7139)
						|| report.getID_NATUN().equals(7148))
					group5Sum += valueOrZero(report.getERECH_NATUN());

			}
		}
		if (yitNchasimBfoal != 0.0)// avoid Double.NaN
			generalResult.GROUP_5 = (group5Sum / yitNchasimBfoal) * 100;

		indexes = reportHolder
				.getIdToIndexesMap()
				.get(ReportType.INSURANCE_ASSETS_MAIN_GROUP_DETAILED_REPORT
						.getValue()).get(generalResult.ID_OTZAR);
		if (indexes != null) {
			for (int i = indexes[0]; i <= indexes[1]; i++) {

				AssetMainGroupDetailedReport report = reportHolder
						.getAssetMainGroupDetailedReports().get(i);
				if (report.getID_SUG_NECHES().equals(4701))
					generalResult.GROUP_1 = valueOrZero(report
							.getACHUZ_SUG_NECHES());

				if (report.getID_SUG_NECHES().equals(4703))
					generalResult.GROUP_2 = valueOrZero(report
							.getACHUZ_SUG_NECHES())
							- (group2Sum / yitNchasimBfoal);

				if (report.getID_SUG_NECHES().equals(4704))
					generalResult.GROUP_3 = valueOrZero(report
							.getACHUZ_SUG_NECHES())
							- (group3Sum / yitNchasimBfoal);

				if (report.getID_SUG_NECHES().equals(4705))
					generalResult.GROUP_4 = valueOrZero(report
							.getACHUZ_SUG_NECHES());

				if (report.getID_SUG_NECHES().equals(4709))
					generalResult.GROUP_6 = valueOrZero(report
							.getACHUZ_SUG_NECHES());

				if (report.getID_SUG_NECHES().equals(4710))
					generalResult.GROUP_7 = valueOrZero(report
							.getACHUZ_SUG_NECHES());

				if (report.getID_SUG_NECHES().equals(4706)
						|| report.getID_SUG_NECHES().equals(4708))
					generalResult.GROUP_8 += valueOrZero(report
							.getACHUZ_SUG_NECHES());

				if (report.getID_SUG_NECHES().equals(4707))
					generalResult.GROUP_9 = valueOrZero(report
							.getACHUZ_SUG_NECHES());

				if (report.getID_SUG_NECHES().equals(4752))
					generalResult.GROUP_10 = valueOrZero(report
							.getACHUZ_SUG_NECHES());

				if (report.getID_SUG_NECHES().equals(4753))
					generalResult.GROUP_11 = valueOrZero(report
							.getACHUZ_SUG_NECHES());

				if (report.getID_SUG_NECHES().equals(4751))
					generalResult.GROUP_12 = valueOrZero(report
							.getACHUZ_SUG_NECHES());

				if (report.getID_SUG_NECHES().equals(4721))
					generalResult.GROUP_13 = valueOrZero(report
							.getACHUZ_SUG_NECHES());

				if (report.getID_SUG_NECHES().equals(4731))
					generalResult.GROUP_14 = valueOrZero(report
							.getACHUZ_SUG_NECHES());

			}
		}

		if (reportHolder.getLastMonthResults() != null) {
			LastMonthResult lastMonthResult = reportHolder
					.getLastMonthResults().get(generalResult.ID_OTZAR);
			if (lastMonthResult != null) {
				generalResult.lastMonthResult = lastMonthResult;
				generalResult.TSUA_MITZT_MI_THILAT_SHANA = lastMonthResult.TSUA_MITZT_MI_THILAT_SHANA;
				generalResult.TSUA_SHNATIT_MEMUZAAT_3_SHANIM = lastMonthResult.TSUA_SHNATIT_MEMUZAAT_3_SHANIM;
			} else {//Missing ReportHolder.lastMonthResults for fund: (generalResult.ID_OTZAR)
				String msg = "Missing ReportHolder.lastMonthResults for fund: "
						+ generalResult.ID_OTZAR;
				Log.warn(msg);
				if (CalculationExecuter.EXCLUDE_EOL_FUNDS)
					return false;
			}
		}

		double riskLevel = generalResult.GROUP_4 + generalResult.GROUP_5;
		generalResult.AGACH_RISK = riskLevel;
		if ((riskLevel >= 0 && riskLevel < 1) || riskLevel < 0)
			generalResult.RISK_LEVEL = MofProperties.get("RISK_LEVEL_1");
		if (riskLevel >= 1 && riskLevel < 15)
			generalResult.RISK_LEVEL = MofProperties.get("RISK_LEVEL_2");
		if (riskLevel >= 15 && riskLevel < 40)
			generalResult.RISK_LEVEL = MofProperties.get("RISK_LEVEL_3");
		if (riskLevel >= 40 && riskLevel < 70)
			generalResult.RISK_LEVEL = MofProperties.get("RISK_LEVEL_4");
		if (riskLevel >= 70)
			generalResult.RISK_LEVEL = MofProperties.get("RISK_LEVEL_5");

		return true;
	}

	static Double valueOrZero(Double value) {
		return value == null ? 0 : value;
	}
}
