package com.novidea.mof.calculations.result;

import java.util.Date;

/**  
 * Hold the view result of 'Insurance' fund type
 * 
 * @author Galil.Zussman
 * @date   Aug 19, 2014
 */
public class InsuranceResult extends GeneralResult {
	
	// GROUP_1  - 4701 - אג"ח ממשלתיות סחירות  
	// GROUP_2  - 4703 – אג"ח קונצרני מדורג או בדירוג מעל BBB סחיר  
	// GROUP_3  - 4704 - אג"ח קונצרני מדורג או בדירוג מעל BBB – לא סחיר
	// GROUP_4  - 4705 - מניות 
	// GROUP_5  - 7110,7111,7112,7139,7148,7131,7132,7133 - אג"ח בסיכון 
	// GROUP_6  - 4709 - קרנות נאמנות  
	// GROUP_7  - 4710 – נכסים אחרים   
	// GROUP_8  - 4706 4708  – פקדונות ומזומן 
	// GROUP_9; - 4707 - הלוואות 
	
	// GROUP_10 - 4752 - חשיפה לחו"ל במונחי דלתא
	// GROUP_11 - 4753 - חשיפה למט"ח במונחי דלתא 
	// GROUP_12 - 4751 - חשיפה למניות במונחי דלתא 
	
	// GROUP_13 - 4721 - סחיר
	// (100% - GROUP_13) = NOT sachir --- code 4722 
	
	// GROUP_14 - 4731 – נכסים בארץ  
	// (100% - GROUP_14) = NOT נכסים בארץ --- code 4732 
	
	public InsuranceResult() {
		super();
	}

	public InsuranceResult(Integer ID_OTZAR, Date HODESH_DIVUACH,
			String SUG_MUTZAR, String SHEM_MUTZAR, String TAAGID) {
		super(ID_OTZAR, HODESH_DIVUACH, SUG_MUTZAR, SHEM_MUTZAR, TAAGID);
	}
}
