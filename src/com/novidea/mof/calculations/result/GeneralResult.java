package com.novidea.mof.calculations.result;

import java.lang.reflect.Field;
import java.util.Date;
import java.util.List;

public class GeneralResult {

	public Integer ID_OTZAR;
	public Date HODESH_DIVUACH;
	public String SUG_MUTZAR;
	public String SHEM_MUTZAR;
	public String TAAGID;
	public String VETEK_SHANIM;
	public Double TZVIRA_NETO;
	public Double HEKEF_MIL;
	public String RISK_LEVEL;
	public Double TSUA_MEMUZAAT_LATKUFA;
	public Double TSUA_MIZTABERET_LATKUFA;
	public Double TSUA_MITZT_MI_THILAT_SHANA;
	public Double STIAT_TEKEN_LATKUFA;
	public Double SHARP_LATKUFA;
	public Double TSUA_MIZTABERET_DMEI_NIHUL_LATKUFA;
	public Double AGACH_RISK;
	public Double TSUA_SHNATIT_MEMUZAAT_3_SHANIM;

	public String TKUFAT_DIVUACH;

	public Double GROUP_1;
	public Double GROUP_2;
	public Double GROUP_3;
	public Double GROUP_4;
	public Double GROUP_5;
	public Double GROUP_6;
	public Double GROUP_7;
	public Double GROUP_8;
	public Double GROUP_9;
	public Double GROUP_10;

	public Double GROUP_11;
	public Double GROUP_12;
	public Double GROUP_13;
	public Double GROUP_14;

	public Double DELTA;

	public List<CalculationRecord> periodicData;// accumulated data throughout the requested period 
	//public String periodicDataJsonStr;// accumulated data throughout the requested period 
	public LastMonthResult lastMonthResult;//

	public GeneralResult() {
	}

	public GeneralResult(Integer ID_OTZAR, Date HODESH_DIVUACH,
			String SUG_MUTZAR, String SHEM_MUTZAR, String TAAGID) {
		super();
		this.ID_OTZAR = ID_OTZAR;
		this.HODESH_DIVUACH = HODESH_DIVUACH;
		this.SUG_MUTZAR = SUG_MUTZAR;
		this.SHEM_MUTZAR = SHEM_MUTZAR;
		this.TAAGID = TAAGID;
	}

	@Override
	public String toString() {
		Class<GeneralResult> generalClass = GeneralResult.class;
		Field[] fields = generalClass.getDeclaredFields();
		String fieldName;
		Object fieldValue = null;
		StringBuffer sb = new StringBuffer();
		for (int i = 0; i < fields.length; i++) {
			fieldName = fields[i].getName();
			try {
				fieldValue = "" + fields[i].get(this);
			} catch (IllegalArgumentException e) {
				fieldValue = "N\\A";
			} catch (IllegalAccessException e) {
				fieldValue = "N\\A";
			}
			sb.append(fieldName + "=" + fieldValue + ", ");
		}
		return sb.toString();
	}

}
