package com.novidea.mof.calculations.result;

import java.lang.reflect.Field;
import java.util.Date;

import org.joda.time.DateTime;

/**
 * 
 * @author Galil.Zussman
 * @date Oct 21, 2014
 * 
 */
public class LastMonthResult implements java.io.Serializable {

	private static final long serialVersionUID = 1L;

	public static class TsuaRecord {
		public Double value = 0.0;
		public Double GRP_RISK_AVG = null;
		public Date date = null;

		public TsuaRecord() {
		}

		public TsuaRecord(Double tsua_HODSHIT, Double GRP_RISK_AVG,
				Date hodesh_DIVUACH) {
			this.value = tsua_HODSHIT;
			this.GRP_RISK_AVG = GRP_RISK_AVG;
			this.date = (new DateTime(hodesh_DIVUACH.getTime())).toDate();
		}
	}

	public String TYPE;
	public Integer ID;
	public Double TSUA;
	public Double TSUA_12;//MIZTABERET
	public Double TSUA_36;//MIZTABERET
	public Double TSUA_60;//MIZTABERET
	public Double TSUA_MITZT_MI_THILAT_SHANA;//MIZTABERET
	public Double STIAT_TEKEN_12;
	public Double STIAT_TEKEN_36;
	public Double STIAT_TEKEN_60;
	public String RISK_LEVEL;
	public Double TSUA_SHNATIT_MEMUZAAT_3_SHANIM;
	public Double RISK_LEVEL_GRP_AVG;
	public Integer LTIQ_12;
	public Integer LTIQ_36;

	public TsuaRecord[] tsua12MonthArr;
	public TsuaRecord[] tsua36MonthArr;

	public LastMonthResult() {
	}

	public LastMonthResult(String tYPE, Integer iD, Double tSUA,
			Double tSUA_36, Double tSUA_60, Double tSUA_MITZT_MI_THILAT_SHANA,
			Double sTIAT_TEKEN_36, Double sTIAT_TEKEN_60, String RISK_LEVEL, Double TSUA_SHNATIT_MEMUZAAT_3_SHANIM) {
		super();
		this.TYPE = tYPE;
		this.ID = iD;
		this.TSUA = tSUA;
		this.TSUA_36 = tSUA_36;
		this.TSUA_60 = tSUA_60;
		this.TSUA_MITZT_MI_THILAT_SHANA = tSUA_MITZT_MI_THILAT_SHANA;
		this.STIAT_TEKEN_36 = sTIAT_TEKEN_36;
		this.STIAT_TEKEN_60 = sTIAT_TEKEN_60;
		this.RISK_LEVEL = RISK_LEVEL;
		this.TSUA_SHNATIT_MEMUZAAT_3_SHANIM = TSUA_SHNATIT_MEMUZAAT_3_SHANIM;
	}

	/*
		public LastMonthResult(String tYPE, Integer iD, Double tSUA,
				Double tSUA_36, Double tSUA_60, String rISK_LEVEL) {
			super();
			TYPE = tYPE;
			ID = iD;
			TSUA = tSUA;
			TSUA_36 = tSUA_36;
			TSUA_60 = tSUA_60;
			RISK_LEVEL = rISK_LEVEL;
		}
	*/
	@Override
	public String toString() {
		Class<LastMonthResult> fundClass = LastMonthResult.class;
		Field[] fundFields = fundClass.getDeclaredFields();
		String fieldName;
		Object fieldValue = null;
		StringBuffer sb = new StringBuffer();
		for (int i = 1 /* skip serialVersionUID */; i < fundFields.length; i++) {
			fieldName = fundFields[i].getName();
			if (fieldName.equalsIgnoreCase("updateddate")
					|| fieldName.equalsIgnoreCase("fund"))
				continue;
			try {
				fieldValue = "" + fundFields[i].get(this);
			} catch (IllegalArgumentException e) {
				fieldValue = "N\\A";
			} catch (IllegalAccessException e) {
				fieldValue = "N\\A";
			}
			sb.append(fieldName + "=" + fieldValue + ", ");
		}
		return sb.toString();
	}
}