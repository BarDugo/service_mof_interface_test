package com.novidea.mof.calculations.result;

import java.util.Date;

/**
 * 
 * @author Galil.Zussman
 * @date Oct 30, 2014
 */
public class CalculationRecord {
	public Double tsua;
	public Double tsuaMiztaberetLatkufa;
	public Double tsuaMiztaberetDmeiNihul;
	public Date date;

	public CalculationRecord() {
	}

	public CalculationRecord(Double tsuaLatkufaThisMonth,
			Double tsuaMiztaberetDmeiNihul, Date hodesh_DIVUACH) {
		this.tsua = tsuaLatkufaThisMonth;
		this.date = hodesh_DIVUACH;
	}

	@Override
	public String toString() {
		return "CalculationRecord [tsua=" + tsua + ", tsuaMiztaberetLatkufa="
				+ tsuaMiztaberetLatkufa + ", tsuaMiztaberetDmeiNihul="
				+ tsuaMiztaberetDmeiNihul + ", date=" + date + "]";
	}

}