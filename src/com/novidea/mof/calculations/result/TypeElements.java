package com.novidea.mof.calculations.result;

import java.util.Date;
import java.util.List;

/**   
 * @author Galil.Zussman
 * @date   Apr 15, 2015
 */
public class TypeElements {

	public Date lastReportedMonth;
	public List<String> productTypes;
	public List<String> companyNames;
	public List<Integer> allIds;

	public TypeElements() {
		super();
	}

	/**
	 *  full constructor 
	 *  
	 * @param lastReportedMonth
	 * @param productTypes
	 * @param companyNames
	 * @param allIds
	 */
	public TypeElements(Date lastReportedMonth, List<String> productTypes,
			List<String> companyNames, List<Integer> allIds) {
		super();
		this.lastReportedMonth = lastReportedMonth;
		this.productTypes = productTypes;
		this.companyNames = companyNames;
		this.allIds = allIds;
	}


}
