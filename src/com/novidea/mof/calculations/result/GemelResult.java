package com.novidea.mof.calculations.result;

import java.util.Date;

/**  
 * Hold the view result of 'Gemel' fund type
 * 
 * @author Galil.Zussman
 * @date   Aug 17, 2014
 */
public class GemelResult extends GeneralResult {
	
	// GROUP_1  - BigPie // 4701 - אג"ח ממשלתיות סחירות  
	// GROUP_2  - BigPie // 4703 – אג"ח קונצרני מדורג או בדירוג מעל BBB סחיר  
	// GROUP_3  - 4751 - חשיפה למניות
	// GROUP_4  - BigPie // 5513 5514 5515 5549 5550 5551 7147 7156 7174 7186 - אג"ח בסיכון 
	// GROUP_5  - BigPie // 4709 - קרנות נאמנות
	// GROUP_6  - BigPie // 4710 - נכסים אחרים
	// GROUP_7  - BigPie // 4706 4708 – פקדונות ומזומן  
	// GROUP_8  - BigPie // 4707 – הלוואות 
	// GROUP_9; - 4752 - חשיפה לחו"ל
	// GROUP_10 - 4761 - חשיפה למט"ח 
 
	// GROUP_11 - 4721 - נכסים שכירים ונזילים 
	// GROUP_12 - 4722 - נכסים לא שכירים 
	// GROUP_13 - BigPie // 4705 - מניות, אופציות ותעודות סל מנייתיות 
	// GROUP_14 - BigPie // 4704 – אג"ח קונצרני מדורג או בדירוג מעל BBB –לא סחיר   
	
	public GemelResult() {
		super();
	}

	public GemelResult(Integer ID_OTZAR,Date HODESH_DIVUACH, String SUG_MUTZAR, String SHEM_MUTZAR,
			String TAAGID) {
		super(ID_OTZAR,HODESH_DIVUACH, SUG_MUTZAR, SHEM_MUTZAR, TAAGID);
	}

}
