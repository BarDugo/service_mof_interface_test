package com.novidea.mof.calculations.cache;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.novidea.mof.calculations.CalculationExecuter;
import com.novidea.mof.calculations.result.GeneralResult;
import com.novidea.mof.calculations.result.InsuranceResult;
import com.novidea.mof.calculations.result.LastMonthResult;
import com.novidea.mof.calculations.result.LastMonthResult.TsuaRecord;
import com.novidea.mof.entities.LastMonthReportType;
import com.novidea.mof.entities.ReportType;
import com.novidea.mof.entities.model.AssetFullDetailedReport;
import com.novidea.mof.entities.model.Fund;
import com.novidea.mof.entities.service.ServiceHolder;

/**
 * 
 * @author Galil.Zussman
 * @date Dec 11, 2014
 */
public class InsuranceCachedReport extends AbstractCachedReport implements
		Serializable {

	private static final long serialVersionUID = 1L;

	private static volatile InsuranceCachedReport instance;
	private static Map<Integer, Fund> lastMonthFundMap;
	private Map<Integer, String> companyIdToName;

	private InsuranceCachedReport() {
		super(ReportType.INSURANCE_DETAILED_REPORT.getValue(),
				ReportType.INSURANCE_ASSETS_MAIN_GROUP_DETAILED_REPORT
						.getValue(),
				ReportType.INSURANCE_ASSETS_FULL_DETAILED_REPORT.getValue(),
				null,
				LastMonthReportType.INSURANCE_LAST_MONTH_RESULT.val());

		this.companyIdToName = mapCompanyIdToName();

	}

	public Map<Integer, String> getCompanyIdToName() {
		return companyIdToName;
	}

	/**
	 * companies names map by id
	 * 
	 * @return Map<Integer, String>
	 */
	private Map<Integer, String> mapCompanyIdToName() {
		Map<Integer, String> map = new HashMap<Integer, String>();
		List<AssetFullDetailedReport> list = serviceHolder.assetFullDetailedReportService
				.executeNativeSqlCustomQuery("SELECT  distinct(ID_HEVRA),SHEM,SHEM_HEVRA FROM asset_full_detailed_report;");
		for (AssetFullDetailedReport assetFullDetailedReport : list)
			if (assetFullDetailedReport.getID_HEVRA() != null)
				map.put(assetFullDetailedReport.getID_HEVRA(),
						assetFullDetailedReport.getSHEM_HEVRA()
								.replace(")", "").replace("(", ""));

		return map;
	}

	@Override
	List<String> buildTypedCompanyNames() {
		return new ArrayList<String>(companyIdToName.values());
	}

	public static InsuranceCachedReport getInstance() {
		if (instance == null) {
			synchronized (InsuranceCachedReport.class) {
				if (instance == null) {
					instance = new InsuranceCachedReport();
					instance.buildMe();
				}
			}
		}
		return instance;
	}

	// map last month insurance funds by ID
	static void mapLastMonthFunds() {
		lastMonthFundMap = new HashMap<Integer, Fund>();
		List<Fund> lastMonthFunds = ServiceHolder.getInstance().fundService
				.mainFilteredFetch(null,
						ReportType.INSURANCE_DETAILED_REPORT.getValue(), null,
						null, getInstance().getLastReportedMonth(),
						getInstance().getLastReportedMonth(), 0);
		for (Fund fund : lastMonthFunds) {
			lastMonthFundMap.put(fund.getID(), fund);
		}
	}

	public Map<Integer, Fund> getLastMonthFundMap() {
		if (lastMonthFundMap == null)
			mapLastMonthFunds();
		return lastMonthFundMap;
	}

	@Override
	public void rebuild() {
		instance = null;
		lastMonthFundMap = null;
		getInstance();
	}

	/*
	 * @Override protected Double avgTsuaAtMonthlyRiskLevelGroup(Fund fund,
	 * String riskLevel) { return 0.0; }
	 * 
	 * @Override protected Double avgTsuaAtYearlyRiskLevelGroup( List<? extends
	 * GeneralResult> insuranceResults, String riskLevel) { return 0.0; }
	 */

	@Override
	LastMonthResult initLastLonthResult(Fund fundItemLastMonth,
			GeneralResult insuranceResultItemLastMonth) {
		return new LastMonthResult(LAST_MONTH_RESULT_TYPE,
				fundItemLastMonth.getID(), fundItemLastMonth.getTSUA_HODSHIT(),
				fundItemLastMonth.getTSUA_MITZTABERET_36_HODASHIM(),
				fundItemLastMonth.getTSUA_MITZTABERET_60_HODASHIM(),
				fundItemLastMonth.getTSUA_MITZT_MI_THILAT_SHANA(),
				fundItemLastMonth.getSTIAT_TEKEN_36_HODASHIM(),
				fundItemLastMonth.getSTIAT_TEKEN_60_HODASHIM(),
				insuranceResultItemLastMonth.RISK_LEVEL,
				fundItemLastMonth.getTSUA_SHNATIT_MEMUZAAT_3_SHANIM());
	}

	@Override
	void addTypedOparation(LastMonthResult lastMonthResult,
			List<? extends GeneralResult> generalResults,
			Fund fundItemLastMonth, GeneralResult generalResultItemLastMonth) {
		lastMonthResult.RISK_LEVEL_GRP_AVG = avgTsuaAtYearlyRiskLevelGroup(
				generalResults, generalResultItemLastMonth.RISK_LEVEL);
	}

	@Override
	void last12MonthsTsua(LastMonthResult lastMonthResult,
			Fund fundItemLastMonth, GeneralResult insuranceResultItemLastMonth) {

		Integer[] indexes = getIdToIndexesMap().get("yearlyFunds").get(
				fundItemLastMonth.getID());
		List<LastMonthResult.TsuaRecord> tsua12MonthRecordList = null;
		if (indexes != null) {
			tsua12MonthRecordList = new ArrayList<LastMonthResult.TsuaRecord>();
			StringBuilder sb = new StringBuilder();
			sb.append("---------------------------------------------------\nLast 12 months tsua report:\n");
			for (int j = indexes[0]; j <= indexes[1]; j++) {
				Fund fund = yearlyFunds.get(j);

				sb.append("Fund ID: " + fund.getID() + "\t" + "Date: "
						+ fund.getHODESH_DIVUACH() + "\t" + "Tsua: "
						+ fund.getTSUA_HODSHIT() + "\n");

				Double avgMonthlyRiskGroup = avgTsuaAtMonthlyRiskLevelGroup(
						fund, insuranceResultItemLastMonth.RISK_LEVEL);

				tsua12MonthRecordList.add(new LastMonthResult.TsuaRecord(fund
						.getTSUA_HODSHIT(), avgMonthlyRiskGroup, fund
						.getHODESH_DIVUACH()));
			}
			System.out.print(sb);
			System.out.println("size = " + tsua12MonthRecordList.size());
			System.out
					.println("---------------------------------------------------\n");

			lastMonthResult.tsua12MonthArr = new LastMonthResult.TsuaRecord[tsua12MonthRecordList
					.size()];
			tsua12MonthRecordList.toArray(lastMonthResult.tsua12MonthArr);

			lastMonthResult.LTIQ_12 = 0;
			for (TsuaRecord tsuaRecord : tsua12MonthRecordList) {
				if (tsuaRecord.value != null && tsuaRecord.GRP_RISK_AVG != null
						&& (tsuaRecord.value > tsuaRecord.GRP_RISK_AVG))
					lastMonthResult.LTIQ_12++;
			}

			calculateTsuaMiztaberet(lastMonthResult);
			calculateStiatTeken(lastMonthResult);
		}
	}

	@Override
	void last36MonthsTsua(LastMonthResult lastMonthResult,
			Fund fundItemLastMonth, GeneralResult insuranceResultItemLastMonth) {
		Integer[] indexes = getIdToIndexesMap().get("last36MonthFunds").get(
				fundItemLastMonth.getID());
		List<LastMonthResult.TsuaRecord> tsua36MonthRecordList = null;
		if (indexes != null) {
			tsua36MonthRecordList = new ArrayList<LastMonthResult.TsuaRecord>();
			StringBuilder sb = new StringBuilder();
			sb.append("---------------------------------------------------\nLast 36 months tsua report:\n");
			for (int j = indexes[0]; j <= indexes[1]; j++) {
				Fund fund = last36MonthFunds.get(j);

				sb.append("Fund ID: " + fund.getID() + "\t" + "Date: "
						+ fund.getHODESH_DIVUACH() + "\t" + "Tsua: "
						+ fund.getTSUA_HODSHIT() + "\n");

				Double avgMonthlyRiskGroup = avgTsuaAtMonthlyRiskLevelGroup(
						fund, insuranceResultItemLastMonth.RISK_LEVEL);

				tsua36MonthRecordList.add(new LastMonthResult.TsuaRecord(fund
						.getTSUA_HODSHIT(), avgMonthlyRiskGroup, fund
						.getHODESH_DIVUACH()));
			}
			System.out.print(sb);
			System.out.println("size = " + tsua36MonthRecordList.size());
			System.out
					.println("---------------------------------------------------\n");

			lastMonthResult.tsua36MonthArr = new LastMonthResult.TsuaRecord[tsua36MonthRecordList
					.size()];
			tsua36MonthRecordList.toArray(lastMonthResult.tsua36MonthArr);
			lastMonthResult.LTIQ_36 = 0;
			for (TsuaRecord tsuaRecord : tsua36MonthRecordList) {
				if (tsuaRecord.value != null && tsuaRecord.GRP_RISK_AVG != null
						&& (tsuaRecord.value > tsuaRecord.GRP_RISK_AVG))
					lastMonthResult.LTIQ_36++;
			}
		}
	}

	@Override
	List<InsuranceResult> getTypedResults(List<Fund> fundsLastMonth) {
		return CalculationExecuter.getInsuranceResults(fundsLastMonth, null);
	}

	public static void main(String[] args) {
		InsuranceCachedReport.getInstance();

	}

}
