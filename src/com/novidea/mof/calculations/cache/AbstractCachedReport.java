package com.novidea.mof.calculations.cache;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.joda.time.DateTime;

import com.novidea.mof.calculations.CalculationExecuter;
import com.novidea.mof.calculations.result.GeneralResult;
import com.novidea.mof.calculations.result.LastMonthResult;
import com.novidea.mof.entities.model.AssetFullDetailedReport;
import com.novidea.mof.entities.model.AssetMainGroupDetailedReport;
import com.novidea.mof.entities.model.AssetYieldsAndBalancesReport;
import com.novidea.mof.entities.model.DateCoverage;
import com.novidea.mof.entities.model.Fund;
import com.novidea.mof.entities.service.ServiceHolder;
import com.novidea.mof.exception.IdMismatchException;

/**
 * ReportHelper class assuming that all the retrieved reports,
 * AssetFullDetailedReport or AssetMainGroupDetailedReport, are ordered by ID
 * (not the PK).
 * 
 * @author Galil.Zussman
 * @date Sep 7, 2014
 */
abstract public class AbstractCachedReport {

	String DETAILED_REPORT_TYPE;
	String ASSET_MAIN_GROUP_DETAILED_REPORT_TYPE;
	String ASSET_FULL_DETAILED_REPORT_TYPE;
	String ASSET_YIELDS_AND_BALANCES_REPORT_TYPE;
	String LAST_MONTH_RESULT_TYPE;

	ServiceHolder serviceHolder = ServiceHolder.getInstance();

	// exposed getters
	private Date lastReportedDate;
	private List<String> companyNames;
	private List<Integer> allIds;
	private List<String> productTypes;
	private List<AssetFullDetailedReport> assetFullDetailedReports;
	private List<AssetMainGroupDetailedReport> assetMainGroupDetailedReports;
	private List<AssetYieldsAndBalancesReport> assetYieldsAndBalancesReports;
	private List<Fund> lastMonthFunds;

	/**
	 * Map<reportType, <ID,{startIndex, endIndex}> > - will hold helpers types
	 * also
	 */
	private Map<String, Map<Integer, Integer[]>> idToIndexesMap;

	private Map<Integer, LastMonthResult> lastMonthResults;

	// helpers
	protected List<Fund> yearlyFunds;
	protected List<Fund> last36MonthFunds;
	private Map<Date, List<Fund>> dateToFundsMap;
	private Map<Date, List<? extends GeneralResult>> dateToGeneralResultMap;
	private Map<String, Double> riskLevelToAVG;

	/**
	 * 
	 * @param DETAILED_REPORT_TYPE
	 * @param ASSET_MAIN_GROUP_DETAILED_REPORT_TYPE
	 * @param ASSET_FULL_DETAILED_REPORT_TYPE
	 * @param LAST_MONTH_RESULT_TYPE
	 *            - i.e LastMonthReportType.*TYPE*_LAST_MONTH_RESULT.val()
	 */
	protected AbstractCachedReport(String DETAILED_REPORT_TYPE,
			String ASSET_MAIN_GROUP_DETAILED_REPORT_TYPE,
			String ASSET_FULL_DETAILED_REPORT_TYPE,
			String ASSET_YIELDS_AND_BALANCES_REPORT_TYPE,
			String LAST_MONTH_RESULT_TYPE) {
		this.DETAILED_REPORT_TYPE = DETAILED_REPORT_TYPE;
		this.ASSET_MAIN_GROUP_DETAILED_REPORT_TYPE = ASSET_MAIN_GROUP_DETAILED_REPORT_TYPE;
		this.ASSET_FULL_DETAILED_REPORT_TYPE = ASSET_FULL_DETAILED_REPORT_TYPE;
		this.ASSET_YIELDS_AND_BALANCES_REPORT_TYPE = ASSET_YIELDS_AND_BALANCES_REPORT_TYPE;
		this.LAST_MONTH_RESULT_TYPE = LAST_MONTH_RESULT_TYPE;
	}

	public Map<Integer, LastMonthResult> getLastMonthResults() {
		return lastMonthResults;
	}

	public List<String> getCompanyNames() {
		return companyNames;
	}

	/**
	 * 
	 * @return all of the funds ids of this type
	 * @date By Galil.Zussman
	 */
	public List<Integer> getAllIds() {
		return allIds;
	}

	public List<String> getProductTypes() {
		return productTypes;
	}

	/**
	 * this PARAM is crucial across the application to use as an anchor for last
	 * reported date where everything is related to it. i.e - we on Jan 2015 and
	 * last reported date is Dec 2014, that means the TSUA from the beginning of
	 * the year should be calculated since Jan 2014
	 * 
	 * @return lastReportedDate
	 */
	public Date getLastReportedMonth() {
		return lastReportedDate;
	}

	public Map<String, Map<Integer, Integer[]>> getIdToIndexesMap() {
		return idToIndexesMap;
	}

	public List<Fund> getLastMonthFunds() {
		return lastMonthFunds;
	}

	public List<AssetFullDetailedReport> getAssetFullDetailedReports() {
		return assetFullDetailedReports;
	}

	public List<AssetMainGroupDetailedReport> getAssetMainGroupDetailedReports() {
		return assetMainGroupDetailedReports;
	}

	public List<AssetYieldsAndBalancesReport> getAssetYieldsAndBalancesReports() {
		return assetYieldsAndBalancesReports;
	}

	abstract public void rebuild();

	/**
	 * initialize or rebuild this class use when init or when changes are made
	 * to the database, usually you will use it after fetching data from
	 * mof.gov.il
	 */
	void buildMe() {

		this.lastReportedDate = initLastReportedMonth();
		this.allIds = retrieveFundTypeIds();
		this.productTypes = retrieveFundProductTypes();
		this.companyNames = buildTypedCompanyNames();

		// get last month funds
		this.lastMonthFunds = serviceHolder.fundService.mainFilteredFetch(null,
				DETAILED_REPORT_TYPE, null, null, this.lastReportedDate,
				this.lastReportedDate, 0);

		this.assetFullDetailedReports = this.serviceHolder.assetFullDetailedReportService
				.findByHODESH_DIVUACH(lastReportedDate,
						ASSET_FULL_DETAILED_REPORT_TYPE);

		this.assetMainGroupDetailedReports = this.serviceHolder.assetMainGroupDetailedReportService
				.findByHODESH_DIVUACH(lastReportedDate,
						ASSET_MAIN_GROUP_DETAILED_REPORT_TYPE);

		this.assetYieldsAndBalancesReports = this.serviceHolder.assetYieldsAndBalancesReportService
				.findByHODESH_DIVUACH(lastReportedDate,
						ASSET_YIELDS_AND_BALANCES_REPORT_TYPE);

		this.yearlyFunds = this.serviceHolder.fundService
				.fetchTsuaaYearlyResults(DETAILED_REPORT_TYPE,
						lastReportedDate, 1, 50000);

		this.last36MonthFunds = this.serviceHolder.fundService
				.fetchTsuaaYearlyResults(DETAILED_REPORT_TYPE,
						lastReportedDate, 3, 50000);
		buildMap();

		lastMonthResults = buildLastMonthResults();

		clearHalperVariables();
		printCachedItems();

	}

	/**
	 * print some of the cached items to console after successful built
	 * 
	 * @date Apr 16, 2015 By Galil.Zussman
	 */
	private void printCachedItems() {

		System.out.println("Done building cache for Fund type \""
				+ DETAILED_REPORT_TYPE + "\", printing typed items\n");
		System.out.println("LastReportedMonth:\t" + getLastReportedMonth());
		// System.out.println("CompanyIdToName:\t" + getCompanyIdToName());
		System.out.println("AllIds:\t\t\t" + getAllIds());
		System.out.println("ProductTypes:\t\t" + getProductTypes());

	}

	/**
	 * clear helper variables from the cache
	 */
	private void clearHalperVariables() {
		yearlyFunds = null;
		last36MonthFunds = null;
		dateToFundsMap = null;
		dateToGeneralResultMap = null;
		riskLevelToAVG = null;
	}

	/**
	 * public static helper function to check the the last reported month date
	 * of all types based on the database population algorithm logic which
	 * determine {@link DateCoverage} control table
	 * 
	 * @return Last reported month {@link Date}
	 */
	private Date initLastReportedMonth() {
		List<DateCoverage> dateCoverages;

		DateTime searchDate = DateTime.now().withDayOfMonth(1)
				.withTime(0, 0, 0, 0);
		do {
			dateCoverages = serviceHolder.dateCoverageService
					.findMonthOfYearAndType(searchDate.getYear(),
							searchDate.getMonthOfYear(), null);
			searchDate = searchDate.minusMonths(1);
		} while (dateCoverages.size() == 0);

		DateTime dateTime = new DateTime(dateCoverages.get(0).getYear(),
				dateCoverages.get(0).getMonth(), 1, 0, 0);
		return dateTime.toDate();
	}

	/**
	 * build the indexes map based on the desired report
	 * "Map<reportType, <ID,{startIndex, endIndex}>>"
	 * 
	 */
	private void buildMap() {

		idToIndexesMap = new HashMap<String, Map<Integer, Integer[]>>();

		Map<Integer, Integer[]> id2Indexes = new HashMap<Integer, Integer[]>();
		Integer id = assetMainGroupDetailedReports.get(0).getID();

		for (int i = 0, j = 0; i < assetMainGroupDetailedReports.size(); i++, j++) {
			if (!assetMainGroupDetailedReports.get(i).getID().equals(id)) {
				Integer[] indexes = new Integer[] { i - j, i - 1 };
				id2Indexes.put(id, indexes);
				id = assetMainGroupDetailedReports.get(i).getID();
				j = 0;
			}
		}

		idToIndexesMap.put(ASSET_MAIN_GROUP_DETAILED_REPORT_TYPE, id2Indexes);

		id = lastMonthFunds.get(0).getID();
		id2Indexes = new HashMap<Integer, Integer[]>();
		for (int i = 0, j = 0; i < lastMonthFunds.size(); i++, j++) {
			if (!lastMonthFunds.get(i).getID().equals(id)) {
				id2Indexes.put(id, new Integer[] { i - j, i - 1 });
				id = lastMonthFunds.get(i).getID();
				j = 0;
			}
		}
		idToIndexesMap.put(DETAILED_REPORT_TYPE, id2Indexes);

		id = assetFullDetailedReports.get(0).getID();
		id2Indexes = new HashMap<Integer, Integer[]>();
		for (int i = 0, j = 0; i < assetFullDetailedReports.size(); i++, j++) {
			if (!assetFullDetailedReports.get(i).getID().equals(id)) {
				id2Indexes.put(id, new Integer[] { i - j, i - 1 });
				id = assetFullDetailedReports.get(i).getID();
				j = 0;
			}
		}
		idToIndexesMap.put(ASSET_FULL_DETAILED_REPORT_TYPE, id2Indexes);

		id = yearlyFunds.get(0).getID();

		if (assetYieldsAndBalancesReports.size() > 0) {//TODO: remove when having test data
			id = assetYieldsAndBalancesReports.get(0).getID();
			id2Indexes = new HashMap<Integer, Integer[]>();
			for (int i = 0, j = 0; i < assetYieldsAndBalancesReports.size(); i++, j++) {
				if (!assetYieldsAndBalancesReports.get(i).getID().equals(id)) {
					id2Indexes.put(id, new Integer[] { i - j, i - 1 });
					id = assetYieldsAndBalancesReports.get(i).getID();
					j = 0;
				}
			}

			idToIndexesMap.put(ASSET_YIELDS_AND_BALANCES_REPORT_TYPE,
					id2Indexes);
		}
		id = yearlyFunds.get(0).getID();
		id2Indexes = new HashMap<Integer, Integer[]>();
		for (int i = 0, j = 0; i < yearlyFunds.size(); i++, j++) {
			if (!yearlyFunds.get(i).getID().equals(id)) {
				id2Indexes.put(id, new Integer[] { i - j, i - 1 });
				id = yearlyFunds.get(i).getID();
				j = 0;
			}
		}
		idToIndexesMap.put("yearlyFunds", id2Indexes);

		id = last36MonthFunds.get(0).getID();
		id2Indexes = new HashMap<Integer, Integer[]>();
		for (int i = 0, j = 0; i < last36MonthFunds.size(); i++, j++) {
			if (!last36MonthFunds.get(i).getID().equals(id)) {
				id2Indexes.put(id, new Integer[] { i - j, i - 1 });
				id = last36MonthFunds.get(i).getID();
				j = 0;
			}
		}
		idToIndexesMap.put("last36MonthFunds", id2Indexes);
	}

	/**
	 * 
	 * @return all of the funds ids of this type
	 * @date Apr 16, 2015 By Galil.Zussman
	 */
	private List<Integer> retrieveFundTypeIds() {

		List<Fund> funds = serviceHolder.fundService
				.executeNativeSqlCustomQuery("SELECT DISTINCT ID FROM fund where ReportType=\'"
						+ DETAILED_REPORT_TYPE + "\' ORDER BY ID ASC;");
		List<Integer> ids = new ArrayList<Integer>();
		for (Fund fund : funds)
			ids.add(fund.getID());

		return ids;
	}

	/**
	 * 
	 * @return "SUG" array
	 * @date Apr 16, 2015 By Galil.Zussman
	 */
	private List<String> retrieveFundProductTypes() {

		List<Fund> funds = serviceHolder.fundService
				.executeNativeSqlCustomQuery("SELECT DISTINCT SUG FROM fund where ReportType=\'"
						+ DETAILED_REPORT_TYPE + "\';");
		List<String> types = new ArrayList<String>();
		for (Fund fund : funds)
			types.add(fund.getSUG());
		return types;
	}

	/**
	 * populate corresponding {@link LastMonthResult} for the typed funds, and
	 * map fund ID to {@link LastMonthResult}
	 * 
	 * @return {@link LastMonthResult}
	 */
	private Map<Integer, LastMonthResult> buildLastMonthResults() {
		Map<Integer, LastMonthResult> lastMonthResults = new HashMap<Integer, LastMonthResult>();
		LastMonthResult lastMonthResult = null;
		List<? extends GeneralResult> generalResults = getTypedResults(lastMonthFunds);
		int generalResultCounter = 0;
		for (int i = 0; i < lastMonthFunds.size(); i++) {

			Fund fundItemLastMonth = lastMonthFunds.get(i);
			if(lastMonthResults.get(fundItemLastMonth.getID()) == null){
				GeneralResult generalResultItemLastMonth = generalResults.get(generalResultCounter);			
				if (!fundItemLastMonth.getID().equals(
						generalResultItemLastMonth.ID_OTZAR))
					throw new IdMismatchException("Data corruption: index " + i
							+ " of the fund[" + i + "] and generalResults[" + i
							+ "] have different company Id!\n"
							+ fundItemLastMonth.getID() + "\n"
							+ generalResultItemLastMonth);
	
				lastMonthResult = doCecheOperations(lastMonthResult,
						generalResults, fundItemLastMonth,
						generalResultItemLastMonth);
	
				lastMonthResults.put(fundItemLastMonth.getID(), lastMonthResult);
				generalResultCounter++;
				// System.out.println(funds.get(i).getID() + "\t" +
				// generalResultItemLastMonth.ID_OTZAR);
			}
		}
		return lastMonthResults;
	}

	/**
	 * Template method to wrap the recipe for building the cache
	 * 
	 * @param lastMonthResult
	 * @param generalResults
	 * @param fundItemLastMonth
	 * @param generalResultItemLastMonth
	 * @return
	 */
	LastMonthResult doCecheOperations(LastMonthResult lastMonthResult,
			List<? extends GeneralResult> generalResults,
			Fund fundItemLastMonth, GeneralResult generalResultItemLastMonth) {

		/* start building the LastMonthResult object */
		lastMonthResult = initLastLonthResult(fundItemLastMonth,
				generalResultItemLastMonth);

		/* different cache report may have different logic */
		addTypedOparation(lastMonthResult, generalResults, fundItemLastMonth,
				generalResultItemLastMonth);

		last12MonthsTsua(lastMonthResult, fundItemLastMonth,
				generalResultItemLastMonth);

		last36MonthsTsua(lastMonthResult, fundItemLastMonth,
				generalResultItemLastMonth);
		return lastMonthResult;
	}

	/**
	 * each subclass is responsible to build its own company names list
	 * 
	 * @return
	 * @date Apr 17, 2015 By Galil.Zussman
	 */
	abstract List<String> buildTypedCompanyNames();

	abstract void addTypedOparation(LastMonthResult lastMonthResult,
			List<? extends GeneralResult> generalResults,
			Fund fundItemLastMonth, GeneralResult generalResultItemLastMonth);

	abstract LastMonthResult initLastLonthResult(Fund fundItemLastMonth,
			GeneralResult generalResultItemLastMonth);

	abstract List<? extends GeneralResult> getTypedResults(
			List<Fund> fundsLastMonth);

	/**
	 * get last year ,12 month, TSUA record to pass on
	 * 
	 * @param lastMonthResult
	 * @param fundItemLastMonth
	 * @param generalResultItemLastMonth
	 */
	abstract void last12MonthsTsua(LastMonthResult lastMonthResult,
			Fund fundItemLastMonth, GeneralResult generalResultItemLastMonth);

	/**
	 * get last 3 years ,36 month, TSUA record to pass on
	 * 
	 * @param lastMonthResult
	 * @param fundItemLastMonth
	 * @param generalResultItemLastMonth
	 */
	abstract void last36MonthsTsua(LastMonthResult lastMonthResult,
			Fund fundItemLastMonth, GeneralResult generalResultItemLastMonth);

	/**
	 * get monthly AVG tsua for risk level group per an ID
	 * 
	 * @param generalResults
	 * @param riskLevel
	 */
	@SuppressWarnings("unchecked")
	Double avgTsuaAtMonthlyRiskLevelGroup(Fund fund, String riskLevel) {
		if (dateToFundsMap == null)
			dateToFundsMap = new HashMap<Date, List<Fund>>();
		if (dateToGeneralResultMap == null)
			dateToGeneralResultMap = new HashMap<Date, List<? extends GeneralResult>>();

		Date fundDate = fund.getHODESH_DIVUACH();
		List<Fund> fundsMonthlyList;
		List<? extends GeneralResult> generalResultsMonthlyList;
		if ((fundsMonthlyList = dateToFundsMap.get(fundDate)) == null) {
			// get fundDate monthly funds
			fundsMonthlyList = serviceHolder.fundService.mainFilteredFetch(
					null, fund.getReportType(), null, null, fundDate, fundDate,
					0);
			dateToFundsMap.put(fundDate, fundsMonthlyList);
		}

		if ((generalResultsMonthlyList = (List<GeneralResult>) dateToGeneralResultMap
				.get(fundDate)) == null) {

			generalResultsMonthlyList = getTypedResults(fundsMonthlyList);
			dateToGeneralResultMap.put(fundDate, generalResultsMonthlyList);

		}
		// extract numeric, QUICK AND DIRTY
		// TODO: Should replace riskLevel to ENUM throughout the project !!
		// .fund.Integer maslulHashkaa = Integer.valueOf(riskLevel
		// .replaceAll("\\D+", ""));
		Double retVal = avgRiskLevelFormula(generalResultsMonthlyList,
				riskLevel);

		return retVal;
	}

	Double avgTsuaAtYearlyRiskLevelGroup(
			List<? extends GeneralResult> generalResults, String riskLevel) {
		if (riskLevelToAVG == null)
			riskLevelToAVG = new HashMap<String, Double>();
		if (riskLevelToAVG.get(riskLevel) == null) {
			Double avg = avgRiskLevelFormula(generalResults, riskLevel);
			riskLevelToAVG.put(riskLevel, avg);
			return avg;
		} else
			return riskLevelToAVG.get(riskLevel);

	}

	static Double avgRiskLevelFormula(
			List<? extends GeneralResult> generalResults, String riskLevel) {
		Double sum = 0.0;
		Double avg = 0.0;
		int amount = 0;
		for (GeneralResult generalResult : generalResults) {
			if (generalResult.RISK_LEVEL != null
					&& generalResult.RISK_LEVEL.equals(riskLevel)) {
				sum += generalResult.periodicData.get(0).tsua;
				amount++;
			}
		}
		avg = sum / amount;
		return avg;
	}

	void calculateTsuaMiztaberet(LastMonthResult lastMonthResult) {

		double miztabertLast = 0.0;
		if (lastMonthResult.tsua12MonthArr[0].value != null)
			miztabertLast = lastMonthResult.tsua12MonthArr[0].value / 100;

		for (int j = 1; j < lastMonthResult.tsua12MonthArr.length; j++) {
			if (lastMonthResult.tsua12MonthArr[j].value != null)
				miztabertLast = CalculationExecuter.formulaTsuaMiztberet(
						miztabertLast, lastMonthResult.tsua12MonthArr[j].value);
		}
		lastMonthResult.TSUA_12 = miztabertLast * 100;
	}

	static void calculateStiatTeken(LastMonthResult lastMonthResult) {

		double tsuaMemuzaatLatkufa = 0.0;
		for (int i = 0; i < lastMonthResult.tsua12MonthArr.length; i++) {
			if (lastMonthResult.tsua12MonthArr[i].value != null)
				tsuaMemuzaatLatkufa += lastMonthResult.tsua12MonthArr[i].value;
		}
		tsuaMemuzaatLatkufa = tsuaMemuzaatLatkufa
				/ lastMonthResult.tsua12MonthArr.length;

		double resultSum = 0;
		for (int i = 0; i < lastMonthResult.tsua12MonthArr.length; i++) {
			double tsuaHodshit = lastMonthResult.tsua12MonthArr[i].value == null ? 0
					: lastMonthResult.tsua12MonthArr[i].value;
			double val = Math.pow((tsuaHodshit - tsuaMemuzaatLatkufa), 2);
			resultSum += val;
		}
		double retVal = Math.sqrt(resultSum
				/ lastMonthResult.tsua12MonthArr.length);
		lastMonthResult.STIAT_TEKEN_12 = retVal;
	}

}
