package com.novidea.mof.calculations.cache;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.novidea.mof.calculations.CalculationExecuter;
import com.novidea.mof.calculations.result.GeneralResult;
import com.novidea.mof.calculations.result.LastMonthResult;
import com.novidea.mof.calculations.result.LastMonthResult.TsuaRecord;
import com.novidea.mof.calculations.result.PensionResult;
import com.novidea.mof.entities.LastMonthReportType;
import com.novidea.mof.entities.ReportType;
import com.novidea.mof.entities.model.Fund;

/**
 * 
 * @author Galil.Zussman
 * @date Dec 11, 2014
 */
public class PensionCachedReport extends AbstractCachedReport implements
		Serializable {
	private static final long serialVersionUID = 1L;
	private static volatile PensionCachedReport instance;

	private PensionCachedReport() {
		super(
				ReportType.PENSION_DETAILED_REPORT.getValue(),
				ReportType.PENSION_ASSETS_MAIN_GROUP_DETAILED_REPORT.getValue(),
				ReportType.PENSION_ASSETS_FULL_DETAILED_REPORT.getValue(),
				ReportType.PENSION_ASSETS_YIELDS_AND_BALANCES_REPORT.getValue(),
				LastMonthReportType.PENSION_LAST_MONTH_RESULT.val());
	}

	public static PensionCachedReport getInstance() {
		if (instance == null) {
			synchronized (PensionCachedReport.class) {
				if (instance == null) {
					instance = new PensionCachedReport();
					instance.buildMe();
				}
			}
		}
		return instance;
	}

	@Override
	public void rebuild() {
		instance = null;
		getInstance();

	}

	@Override
	List<String> buildTypedCompanyNames() {
		List<Fund> funds = this.serviceHolder.fundService
				.executeNativeSqlCustomQuery("SELECT  distinct(SHM_HEVRA_MENAHELET) FROM fund where ReportType=\'"
						+ DETAILED_REPORT_TYPE
						+ "\' AND TAARICH_SIUM_PEILUT IS NULL ORDER BY SHM_HEVRA_MENAHELET ASC;");
		List<String> list = new ArrayList<String>();
		for (Fund fund : funds)
			list.add(fund.getSHM_HEVRA_MENAHELET());

		return list;
	}

	@Override
	List<PensionResult> getTypedResults(List<Fund> fundsLastMonth) {
		return CalculationExecuter.getPensionResults(fundsLastMonth, null);
	}

	@Override
	LastMonthResult initLastLonthResult(Fund fundItemLastMonth,
			GeneralResult pensionResultItemLastMonth) {
		return new LastMonthResult(LAST_MONTH_RESULT_TYPE,
				fundItemLastMonth.getID(), fundItemLastMonth.getTSUA_HODSHIT(),
				fundItemLastMonth.getTSUA_MITZTABERET_36_HODASHIM(),
				fundItemLastMonth.getTSUA_MITZTABERET_60_HODASHIM(),
				fundItemLastMonth.getTSUA_MITZT_MI_THILAT_SHANA(),
				fundItemLastMonth.getSTIAT_TEKEN_36_HODASHIM(),
				fundItemLastMonth.getSTIAT_TEKEN_60_HODASHIM(),
				pensionResultItemLastMonth.RISK_LEVEL,
				fundItemLastMonth.getTSUA_SHNATIT_MEMUZAAT_3_SHANIM());
	}

	@Override
	void addTypedOparation(LastMonthResult lastMonthResult,
			List<? extends GeneralResult> generalResults,
			Fund fundItemLastMonth, GeneralResult generalResultItemLastMonth) {
		lastMonthResult.RISK_LEVEL_GRP_AVG = avgTsuaAtYearlyRiskLevelGroup(
				generalResults, generalResultItemLastMonth.RISK_LEVEL);
	}

	@Override
	void last12MonthsTsua(LastMonthResult lastMonthResult,
			Fund fundItemLastMonth, GeneralResult pensionResultItemLastMonth) {

		Integer[] indexes = getIdToIndexesMap().get("yearlyFunds").get(
				fundItemLastMonth.getID());
		List<LastMonthResult.TsuaRecord> tsua12MonthRecordList = null;
		if (indexes != null) {
			tsua12MonthRecordList = new ArrayList<LastMonthResult.TsuaRecord>();
			StringBuilder sb = new StringBuilder();
			sb.append("---------------------------------------------------\nLast 12 months tsua report:");
			for (int j = indexes[0]; j <= indexes[1]; j++) {
				Fund fund = yearlyFunds.get(j);

				sb.append("Fund ID: " + fund.getID() + "\t" + "Date: "
						+ fund.getHODESH_DIVUACH() + "\t" + "Tsua: "
						+ fund.getTSUA_HODSHIT() + "\n");

				Double avgMonthlyRiskGroup = avgTsuaAtMonthlyRiskLevelGroup(
						fund, pensionResultItemLastMonth.RISK_LEVEL);

				tsua12MonthRecordList.add(new LastMonthResult.TsuaRecord(fund
						.getTSUA_HODSHIT(), avgMonthlyRiskGroup, fund
						.getHODESH_DIVUACH()));
			}
			System.out.print(sb);
			System.out.println("size = " + tsua12MonthRecordList.size());
			System.out
					.println("---------------------------------------------------\n");

			lastMonthResult.tsua12MonthArr = new LastMonthResult.TsuaRecord[tsua12MonthRecordList
					.size()];
			tsua12MonthRecordList.toArray(lastMonthResult.tsua12MonthArr);

			lastMonthResult.LTIQ_12 = 0;
			for (TsuaRecord tsuaRecord : tsua12MonthRecordList) {
				if (tsuaRecord.value != null && tsuaRecord.GRP_RISK_AVG != null
						&& (tsuaRecord.value > tsuaRecord.GRP_RISK_AVG))
					lastMonthResult.LTIQ_12++;
			}

			calculateTsuaMiztaberet(lastMonthResult);
			calculateStiatTeken(lastMonthResult);
		}
	}

	@Override
	void last36MonthsTsua(LastMonthResult lastMonthResult,
			Fund fundItemLastMonth, GeneralResult pensionResultItemLastMonth) {
		Integer[] indexes = getIdToIndexesMap().get("last36MonthFunds").get(
				fundItemLastMonth.getID());
		List<LastMonthResult.TsuaRecord> tsua36MonthRecordList = null;
		if (indexes != null) {
			tsua36MonthRecordList = new ArrayList<LastMonthResult.TsuaRecord>();
			StringBuilder sb = new StringBuilder();
			sb.append("---------------------------------------------------\nLast 36 months tsua report:");
			for (int j = indexes[0]; j <= indexes[1]; j++) {
				Fund fund = last36MonthFunds.get(j);

				sb.append("Fund ID: " + fund.getID() + "\t" + "Date: "
						+ fund.getHODESH_DIVUACH() + "\t" + "Tsua: "
						+ fund.getTSUA_HODSHIT() + "\n");

				Double avgMonthlyRiskGroup = avgTsuaAtMonthlyRiskLevelGroup(
						fund, pensionResultItemLastMonth.RISK_LEVEL);

				tsua36MonthRecordList.add(new LastMonthResult.TsuaRecord(fund
						.getTSUA_HODSHIT(), avgMonthlyRiskGroup, fund
						.getHODESH_DIVUACH()));
			}
			System.out.print(sb);
			System.out.println("size = " + tsua36MonthRecordList.size());
			System.out
					.println("---------------------------------------------------\n");

			lastMonthResult.tsua36MonthArr = new LastMonthResult.TsuaRecord[tsua36MonthRecordList
					.size()];
			tsua36MonthRecordList.toArray(lastMonthResult.tsua36MonthArr);
			lastMonthResult.LTIQ_36 = 0;
			for (TsuaRecord tsuaRecord : tsua36MonthRecordList) {
				if (tsuaRecord.value != null && tsuaRecord.GRP_RISK_AVG != null
						&& (tsuaRecord.value > tsuaRecord.GRP_RISK_AVG))
					lastMonthResult.LTIQ_36++;
			}
		}
	}

	public static void main(String[] args) {
		PensionCachedReport.getInstance().getLastReportedMonth();

	}

}
