package com.novidea.mof.calculations;

import java.util.ArrayList;
import java.util.List;

import com.novidea.mof.Log;
import com.novidea.mof.MofProperties;
import com.novidea.mof.calculations.cache.AbstractCachedReport;
import com.novidea.mof.calculations.cache.PensionCachedReport;
import com.novidea.mof.calculations.result.CalculationRecord;
import com.novidea.mof.calculations.result.LastMonthResult;
import com.novidea.mof.calculations.result.PensionResult;
import com.novidea.mof.entities.ReportType;
import com.novidea.mof.entities.model.AssetFullDetailedReport;
import com.novidea.mof.entities.model.AssetMainGroupDetailedReport;
import com.novidea.mof.entities.model.AssetYieldsAndBalancesReport;
import com.novidea.mof.entities.model.Fund;

/**   
 * @author Galil.Zussman
 * @date   May 05, 2015
 */
public class PensionResultsBuilder {
	/*
	 * if TAAGID_SHOLET then 'true', if SHM_HEVRA_MENAHELET then false
	 */
	final static boolean isUSE_TAAGID = false;

	static Double valueOrZero(Double value) {
		return value == null ? 0 : value;
	}

	static Double valueOrZero(String value) {
		return value == null || value.equals("") ? 0 : Double.valueOf(value);
	}

	public static List<PensionResult> getPensionResults(List<Fund> funds,
			Integer investmentRiskLevel) {

		if (funds == null || funds.size() == 0)
			return null;
		List<PensionResult> pensionResults = new ArrayList<PensionResult>();

		try {

			int itemsAmount = 0, tracker = 0;
			int currentID = funds.get(0).getID();
			String taarichHakama = "";
			String tkufatDivuach = "";
			double tzviraNeto = 0;
			double tsuaMemuzaatLatkufa = 0;

			double tsuaLatkufaThisMonth = 0;
			double tsuaMiztaberetLatkufa = 0;
			double tsuaMiztaberetDmeiNihulLatkufa = 0;
			//double shiurDNihulAharonHafkadot = 0;

			List<CalculationRecord> calculationRecordList = new ArrayList<CalculationRecord>();

			double sharpLatkufa = 0;
			double hekefMil = 0;

			Fund monthlyFundItem = funds.get(0);

			PensionResult pensionResult = null;

			pensionResult = new PensionResult(monthlyFundItem.getID(),
					monthlyFundItem.getHODESH_DIVUACH(),
					monthlyFundItem.getSUG(), monthlyFundItem.getSHEM(),
					isUSE_TAAGID ? monthlyFundItem.getSHM_TAAGID_SHOLET()
							: monthlyFundItem.getSHM_HEVRA_MENAHELET());

			// funds have to be ordered by ID for this to work!
			for (Fund fund : funds) {
				monthlyFundItem = funds.get(tracker);
				tsuaLatkufaThisMonth = valueOrZero(monthlyFundItem
						.getTSUA_HODSHIT());

				if (!fund.getID().equals(currentID)) {

					//pensionResult.SHIUR_D_NIHUL_AHARON_HAFKADOT = shiurDNihulAharonHafkadot;

					pensionResult.HEKEF_MIL = hekefMil;
					// SHARP_LATKUFA
					pensionResult.SHARP_LATKUFA = sharpLatkufa;
					pensionResult.TSUA_MIZTABERET_LATKUFA = tsuaMiztaberetLatkufa * 100;
					// SUM
					// TZVIRA_NETO
					pensionResult.TZVIRA_NETO = Double.valueOf(tzviraNeto);
					// AVG
					// TSUA_MEMUZAAT_LATKUFA
					pensionResult.TSUA_MEMUZAAT_LATKUFA = Double
							.valueOf(tsuaMemuzaatLatkufa / itemsAmount);

					// STIAT_TEKEN_LATKUFA
					pensionResult.STIAT_TEKEN_LATKUFA = CalculationExecuter
							.formulaStiatTeken(funds, itemsAmount, tracker,
									pensionResult.TSUA_MEMUZAAT_LATKUFA);
					// VETEK_SHANIM
					pensionResult.VETEK_SHANIM = taarichHakama;
					// TSUA_MIZTABERET_DMEI_NIHUL_LATKUFA
					pensionResult.TSUA_MIZTABERET_DMEI_NIHUL_LATKUFA = tsuaMiztaberetDmeiNihulLatkufa * 100;

					pensionResult.periodicData = calculationRecordList;

					pensionResult.TKUFAT_DIVUACH = tkufatDivuach;

					// conditions to return this result
					if (addPensionLastMonthReportData(pensionResult)) // have last month report - fund EOL
						if (investmentRiskLevel == null
								|| investmentRiskLevel.equals(0))
							pensionResults.add(pensionResult);
						else if (pensionResult.RISK_LEVEL == null
								|| pensionResult.RISK_LEVEL
										.contains(investmentRiskLevel
												.toString())) // will work only if investmentRiskLevel is numbered (i.e "דרגת סיכון 2")
							pensionResults.add(pensionResult);

					/* start a new result and reset all temporal */
					currentID = fund.getID();
					itemsAmount = 0;
					tzviraNeto = 0;
					tsuaMemuzaatLatkufa = 0;
					hekefMil = 0;
					sharpLatkufa = 0;
					tsuaMiztaberetDmeiNihulLatkufa = 0;
					taarichHakama = "";
					//shiurDNihulAharonHafkadot=0;
					tkufatDivuach = "";
					calculationRecordList = new ArrayList<CalculationRecord>();

					tsuaLatkufaThisMonth = valueOrZero(monthlyFundItem
							.getTSUA_HODSHIT());

					pensionResult = new PensionResult(monthlyFundItem.getID(),
							monthlyFundItem.getHODESH_DIVUACH(),
							monthlyFundItem.getSUG(), funds.get(tracker)
									.getSHEM(), isUSE_TAAGID ? funds.get(
									tracker).getSHM_TAAGID_SHOLET() : funds
									.get(tracker).getSHM_HEVRA_MENAHELET());

				}
				//shiurDNihulAharonHafkadot = valueOrZero(fund.getSHIUR_D_NIHUL_HAFKADOT());

				// TZVIRA_NETO
				tzviraNeto += valueOrZero(fund.getTZVIRA_NETO());
				// TSUA_LATKUFA
				tsuaMemuzaatLatkufa += valueOrZero(fund.getTSUA_HODSHIT());
				// SHARP_LATKUFA
				sharpLatkufa = valueOrZero(fund.getSHARP_RIBIT_HASRAT_SIKUN());
				// HEKEF_MIL
				hekefMil = valueOrZero(fund.getYITRAT_NCHASIM_LSOF_TKUFA());

				// VETEK_SHANIM
				taarichHakama = CalculationExecuter.formulaTaarichHakama(fund);

				double shiurDmeiNihul_aharon = valueOrZero(fund
						.getSHIUR_DMEI_NIHUL_AHARON());

				// TSUA_MIZTABERET_DMEI_NIHUL_LATKUFA
				tsuaMiztaberetDmeiNihulLatkufa = CalculationExecuter
						.formulaTsuaMiztaberetDmeiNihulLatkufa(
								tsuaMiztaberetDmeiNihulLatkufa,
								tsuaLatkufaThisMonth / 100,
								shiurDmeiNihul_aharon / 100);

				tkufatDivuach = fund.getTKUFAT_DIVUACH();

				// מצטברת
				if (itemsAmount == 0) {// first in the group

					// TSUA_MIZTABERET_LATKUFA
					tsuaMiztaberetLatkufa = tsuaLatkufaThisMonth / 100;
				} else {
					// TSUA_MIZTABERET_LATKUFA
					tsuaMiztaberetLatkufa = CalculationExecuter
							.formulaTsuaMiztberet(tsuaMiztaberetLatkufa,
									tsuaLatkufaThisMonth);
				}
				CalculationRecord calculationRecord = new CalculationRecord();
				calculationRecord.tsua = tsuaLatkufaThisMonth;
				calculationRecord.tsuaMiztaberetLatkufa = tsuaMiztaberetLatkufa;
				calculationRecord.tsuaMiztaberetDmeiNihul = tsuaMiztaberetDmeiNihulLatkufa;
				calculationRecord.date = monthlyFundItem.getHODESH_DIVUACH();
				calculationRecordList.add(calculationRecord);
				itemsAmount++;
				tracker++;
			}

			//pensionResult.SHIUR_D_NIHUL_AHARON_HAFKADOT = shiurDNihulAharonHafkadot;

			// pensionResult.tsuaaData = tsuaaData;
			pensionResult.HEKEF_MIL = hekefMil;
			// SHARP_LATKUFA
			pensionResult.SHARP_LATKUFA = sharpLatkufa;
			// Miztaberet
			pensionResult.TSUA_MIZTABERET_LATKUFA = tsuaMiztaberetLatkufa * 100;
			// SUM
			// TZVIRA_NETO
			pensionResult.TZVIRA_NETO = Double.valueOf(tzviraNeto);
			// AVG
			// TSUA_MEMUZAAT_LATKUFA
			pensionResult.TSUA_MEMUZAAT_LATKUFA = Double
					.valueOf(tsuaMemuzaatLatkufa / itemsAmount);

			// STIAT_TEKEN_LATKUFA
			pensionResult.STIAT_TEKEN_LATKUFA = CalculationExecuter
					.formulaStiatTeken(funds, itemsAmount, tracker,
							pensionResult.TSUA_MEMUZAAT_LATKUFA);
			// VETEK_SHANIM
			pensionResult.VETEK_SHANIM = taarichHakama;
			// TSUA_MIZTABERET_DMEI_NIHUL_LATKUFA
			pensionResult.TSUA_MIZTABERET_DMEI_NIHUL_LATKUFA = tsuaMiztaberetDmeiNihulLatkufa * 100;
			pensionResult.periodicData = calculationRecordList;

			pensionResult.TKUFAT_DIVUACH = tkufatDivuach;

			// conditions to return this result
			if (addPensionLastMonthReportData(pensionResult)) // don't have last month report - fund EOL
				if (investmentRiskLevel == null
						|| investmentRiskLevel.equals(0))
					pensionResults.add(pensionResult);
				else if (pensionResult.RISK_LEVEL == null
						|| pensionResult.RISK_LEVEL
								.contains(investmentRiskLevel.toString()))
					pensionResults.add(pensionResult);

			return pensionResults;
		} catch (Exception e) {
			Log.error("Got exception in CalculationExecuter.getpensionOrPensionResults, Error: "
					+ e);
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * see {@link pensionResult} for group mapping
	 * @param pensionResult
	 */
	private static boolean addPensionLastMonthReportData(
			PensionResult pensionResult) {

		AbstractCachedReport reportHolder = PensionCachedReport.getInstance();

		pensionResult.GROUP_1 = 0.0;
		pensionResult.GROUP_2 = 0.0;
		pensionResult.GROUP_3 = 0.0;
		pensionResult.GROUP_4 = 0.0;
		pensionResult.GROUP_5 = 0.0;
		pensionResult.GROUP_6 = 0.0;
		pensionResult.GROUP_7 = 0.0;
		pensionResult.GROUP_8 = 0.0;
		pensionResult.GROUP_9 = 0.0;
		pensionResult.GROUP_10 = 0.0;
		pensionResult.GROUP_11 = 0.0;
		pensionResult.GROUP_12 = 0.0;
		pensionResult.GROUP_13 = 0.0;
		pensionResult.GROUP_14 = 0.0;
		pensionResult.GROUP_15 = 0.0;

		double deltaHelper[] = { 0.0, 0.0 };
		Integer[] indexes = reportHolder
				.getIdToIndexesMap()
				.get(ReportType.PENSION_ASSETS_MAIN_GROUP_DETAILED_REPORT
						.getValue()).get(pensionResult.ID_OTZAR);
		if (indexes != null) {
			for (int i = indexes[0]; i <= indexes[1]; i++) {
				AssetMainGroupDetailedReport report = reportHolder
						.getAssetMainGroupDetailedReports().get(i);
				if (report.getID_SUG_NECHES().equals(4701))
					pensionResult.GROUP_1 = valueOrZero(report
							.getACHUZ_SUG_NECHES());

				// first assign, then subtract from the ASSET_FULL_DETAILED_REPORTS items sum 
				if (report.getID_SUG_NECHES().equals(4703))
					pensionResult.GROUP_2 = valueOrZero(report
							.getACHUZ_SUG_NECHES());

				// first assign, then subtract from the ASSET_FULL_DETAILED_REPORTS items sum 
				if (report.getID_SUG_NECHES().equals(4704))
					pensionResult.GROUP_14 = valueOrZero(report
							.getACHUZ_SUG_NECHES());

				if (report.getID_SUG_NECHES().equals(4751)) {
					deltaHelper[0] = valueOrZero(report.getACHUZ_SUG_NECHES());
					pensionResult.GROUP_3 = valueOrZero(report
							.getACHUZ_SUG_NECHES());
				}

				if (report.getID_SUG_NECHES().equals(4705)) {
					deltaHelper[1] = valueOrZero(report.getACHUZ_SUG_NECHES());
					pensionResult.GROUP_13 = valueOrZero(report
							.getACHUZ_SUG_NECHES());
				}

				if (report.getID_SUG_NECHES().equals(4709))
					pensionResult.GROUP_5 = valueOrZero(report
							.getACHUZ_SUG_NECHES());

				if (report.getID_SUG_NECHES().equals(4710))
					pensionResult.GROUP_6 = valueOrZero(report
							.getACHUZ_SUG_NECHES());

				if (report.getID_SUG_NECHES().equals(4706)
						|| report.getID_SUG_NECHES().equals(4708))
					pensionResult.GROUP_7 += valueOrZero(report
							.getACHUZ_SUG_NECHES());

				if (report.getID_SUG_NECHES().equals(4707))
					pensionResult.GROUP_8 = valueOrZero(report
							.getACHUZ_SUG_NECHES());

				if (report.getID_SUG_NECHES().equals(4752))
					pensionResult.GROUP_9 = valueOrZero(report
							.getACHUZ_SUG_NECHES());

				if (report.getID_SUG_NECHES().equals(4761))
					pensionResult.GROUP_10 = valueOrZero(report
							.getACHUZ_SUG_NECHES());

				if (report.getID_SUG_NECHES().equals(4721))
					pensionResult.GROUP_11 = valueOrZero(report
							.getACHUZ_SUG_NECHES());

				if (report.getID_SUG_NECHES().equals(4722))
					pensionResult.GROUP_12 = valueOrZero(report
							.getACHUZ_SUG_NECHES());
				
				if (report.getID_SUG_NECHES().equals(4712))
					pensionResult.GROUP_15 = valueOrZero(report
							.getACHUZ_SUG_NECHES());
			}
		}

		indexes = reportHolder
				.getIdToIndexesMap()
				.get(ReportType.PENSION_ASSETS_YIELDS_AND_BALANCES_REPORT
						.getValue()).get(pensionResult.ID_OTZAR);
		if (indexes != null) {
			for (int i = indexes[0]; i <= indexes[1]; i++) {
				AssetYieldsAndBalancesReport report = reportHolder
						.getAssetYieldsAndBalancesReports().get(i);
				pensionResult.TSUA_NOMINALI_IM_DEMOGRAPHIT = valueOrZero(report
						.getTSUA_NOMINALI_IM_DEMOGRAPHIT());

			}
		}

		indexes = reportHolder.getIdToIndexesMap()
				.get(ReportType.PENSION_DETAILED_REPORT.getValue())
				.get(pensionResult.ID_OTZAR);
		if (indexes != null) {
			for (int i = indexes[0]; i <= indexes[1]; i++) {
				Fund report = reportHolder.getLastMonthFunds().get(i);
				pensionResult.SHIUR_D_NIHUL_HAFKADOT = valueOrZero(report
						.getSHIUR_D_NIHUL_HAFKADOT());
				pensionResult.SHIUR_D_NIHUL_NCHASIM = valueOrZero(report
						.getSHIUR_D_NIHUL_NECHASIM());
				pensionResult.ODEF_GIRAON_ACTUARI_AHARON = valueOrZero(report
						.getODEF_GIRAON_ACTUARI_AHARON());

			}
		}

		Double group2helper = 0.0;
		Double group14helper = 0.0;

		indexes = reportHolder.getIdToIndexesMap()
				.get(ReportType.PENSION_ASSETS_FULL_DETAILED_REPORT.getValue())
				.get(pensionResult.ID_OTZAR);
		if (indexes != null) {
			for (int i = indexes[0]; i <= indexes[1]; i++) {
				AssetFullDetailedReport report = reportHolder
						.getAssetFullDetailedReports().get(i);
				if (report.getID_NATUN().equals(5513)
						|| report.getID_NATUN().equals(5514)
						|| report.getID_NATUN().equals(5515)
						|| report.getID_NATUN().equals(5549)
						|| report.getID_NATUN().equals(5550)
						|| report.getID_NATUN().equals(5551)
						|| report.getID_NATUN().equals(7147)
						|| report.getID_NATUN().equals(7156)
						|| report.getID_NATUN().equals(7174)
						|| report.getID_NATUN().equals(7186)) {

					//int idNatun = report.getID_NATUN();
					double val = valueOrZero(report.getACHUZ());
					//System.out.println("ID: " + report.getID() + "\tidNatun: " + idNatun + "\tval: " + val);
					pensionResult.GROUP_4 += val;

				}

				if (report.getID_NATUN().equals(5513)
						|| report.getID_NATUN().equals(5514)
						|| report.getID_NATUN().equals(5515)
						|| report.getID_NATUN().equals(7147)
						|| report.getID_NATUN().equals(7156))
					group2helper += valueOrZero(report.getACHUZ());

				if (report.getID_NATUN().equals(5549)
						|| report.getID_NATUN().equals(5550)
						|| report.getID_NATUN().equals(5551)
						|| report.getID_NATUN().equals(7174)
						|| report.getID_NATUN().equals(7186))
					group14helper += valueOrZero(report.getACHUZ());

			}
		}

		if (reportHolder.getLastMonthResults() != null) {
			LastMonthResult lastMonthResult = reportHolder
					.getLastMonthResults().get(pensionResult.ID_OTZAR);
			if (lastMonthResult != null) {
				pensionResult.lastMonthResult = lastMonthResult;
				pensionResult.TSUA_MITZT_MI_THILAT_SHANA = lastMonthResult.TSUA_MITZT_MI_THILAT_SHANA;
				pensionResult.TSUA_SHNATIT_MEMUZAAT_3_SHANIM = lastMonthResult.TSUA_SHNATIT_MEMUZAAT_3_SHANIM;
			} else {//Missing ReportHolder.pensionLastMonthResults for fund: (pensionResult.ID_OTZAR)
				String msg = "Missing ReportHolder.pensionLastMonthResults for fund: "
						+ pensionResult.ID_OTZAR;
				Log.warn(msg);
				if (CalculationExecuter.EXCLUDE_EOL_FUNDS)
					return false;
			}
		}

		pensionResult.GROUP_2 -= group2helper;
		pensionResult.GROUP_14 -= group14helper;
		//pensionResult.GROUP_2 = pensionResult.GROUP_2 - pensionResult.GROUP_4;

		pensionResult.DELTA = deltaHelper[0] - deltaHelper[1];
		double sum = pensionResult.GROUP_3 + pensionResult.GROUP_4;
		pensionResult.AGACH_RISK = sum;
		if (sum >= 0 && sum < 1)
			pensionResult.RISK_LEVEL = MofProperties.get("RISK_LEVEL_1");
		if (sum >= 1 && sum < 15)
			pensionResult.RISK_LEVEL = MofProperties.get("RISK_LEVEL_2");
		if (sum >= 15 && sum < 40)
			pensionResult.RISK_LEVEL = MofProperties.get("RISK_LEVEL_3");
		if (sum >= 40 && sum < 70)
			pensionResult.RISK_LEVEL = MofProperties.get("RISK_LEVEL_4");
		if (sum >= 70)
			pensionResult.RISK_LEVEL = MofProperties.get("RISK_LEVEL_5");

		return true;
	}

}
