package com.novidea.mof.ws;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.jws.HandlerChain;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.jws.soap.SOAPBinding.ParameterStyle;
import javax.jws.soap.SOAPBinding.Style;
import javax.jws.soap.SOAPBinding.Use;
import javax.xml.ws.WebServiceException;

import com.novidea.mof.Log;
import com.novidea.mof.MofProperties;
import com.novidea.mof.calculations.result.GemelResult;
import com.novidea.mof.calculations.result.TypeElements;
import com.novidea.mof.entities.model.Fund;
import com.novidea.mof.exception.PrivateKeyException;
import com.novidea.mof.ws.core.MofServiceImpl;

/**
 * {@link MofWebService} class is JAX-WS wrapper that implements
 * {@link MofInterfaceExpose} operations and use {@link MofServiceImpl} to
 * execute those operations
 * 
 * @author Galil.Zussman
 * @date Aug 17, 2014
 */
@WebService(portName = "MofWebServicePort", serviceName = "MofWebService", targetNamespace = "http://ws.mof.novidea.com/")
@SOAPBinding(style = Style.DOCUMENT, use = Use.LITERAL, parameterStyle = ParameterStyle.WRAPPED)
public class MofWebService implements MofInterfaceExpose, Serializable {

	private static final long serialVersionUID = 1L;
	private static final String PRIVATE_KEY = MofProperties.get("PRIVATE_KEY");

	private MofInterfaceExpose mofServiceImplementor = new MofServiceImpl();

	@WebMethod(operationName = "getTypeElements")
	@WebResult(name = "TypeElements")
	@SOAPBinding(style = Style.DOCUMENT, use = Use.LITERAL, parameterStyle = ParameterStyle.WRAPPED)
	public TypeElements getTypeElements(
			@WebParam(name = "reportType") String reportType,
			@WebParam(name = "privateKey") String privateKey) {

		TypeElements typeElements = null;
		try {
			if (!PRIVATE_KEY.equals(privateKey))
				throw new PrivateKeyException("Incorrect PrivateKey !");
			typeElements = mofServiceImplementor.getTypeElements(reportType,
					privateKey);
		} catch (PrivateKeyException e) {
			Log.error("Got PrivateKeyException in mofService.getTypeElements, Error: "
					+ e);
			throw new WebServiceException(
					"PrivateKeyException occured during retrieval of getTypeElements. Error: "
							+ e);
		} catch (Exception e) {
			Log.error("Got exception in mofService.getTypeElements, Error: "
					+ e);
			throw new WebServiceException(
					"Exception occured during retrieval of getTypeElements. Error: "
							+ e);
		}
		return typeElements;
	}

	@WebMethod(operationName = "getFundById")
	@SOAPBinding(style = Style.DOCUMENT, use = Use.LITERAL, parameterStyle = ParameterStyle.WRAPPED)
	public Fund getFundById(@WebParam(name = "id") String id,
			@WebParam(name = "privateKey") String privateKey) {
		Fund fund = null;
		try {
			if (!PRIVATE_KEY.equals(privateKey))
				throw new PrivateKeyException("Incorrect PrivateKey !");

			fund = mofServiceImplementor.getFundById(id, privateKey);
		} catch (PrivateKeyException e) {
			Log.error("Got PrivateKeyException in mofService.getFundById, Error: "
					+ e);
			throw new WebServiceException(
					"PrivateKeyException occured during retrieval of getFundById. Error: "
							+ e);
		} catch (Exception e) {
			Log.error("Got exception in mofService.getFundById, Error: " + e);
			throw new WebServiceException(
					"Exception occured during retrieval of a fund by its id. Error: "
							+ e);
		}
		return fund;
	}

	@WebMethod(operationName = "refreshFunds")
	@SOAPBinding(style = Style.DOCUMENT, use = Use.LITERAL, parameterStyle = ParameterStyle.WRAPPED)
	public void refreshFunds(@WebParam(name = "yearsBack") Integer yearsBack,
			@WebParam(name = "privateKey") String privateKey) {
		try {
			if (!PRIVATE_KEY.equals(privateKey))
				throw new PrivateKeyException("Incorrect PrivateKey !");
			mofServiceImplementor.refreshFunds(yearsBack, privateKey);
		} catch (PrivateKeyException e) {
			Log.error("Got PrivateKeyException in mofService.refreshFunds, Error: "
					+ e);
			throw new WebServiceException(
					"PrivateKeyException occured during retrieval of refreshFunds, Error: "
							+ e);
		} catch (Exception e) {
			Log.error("Got exception in mofService.refreshFunds, Error: " + e);
			throw new WebServiceException(
					"Exception occured during refresh-funds activation, Error: "
							+ e);
		}
	}

	/*@WebMethod(operationName = "refreshFundsWithMonthsBack")
	@SOAPBinding(style = Style.DOCUMENT, use = Use.LITERAL, parameterStyle = ParameterStyle.WRAPPED)
	public void refreshFundsWithMonthsBack(@WebParam(name = "yearsBack") Integer yearsBack, 
			@WebParam(name = "monthsBack") Integer monthsBack,
			@WebParam(name = "privateKey") String privateKey) {
		try {
			if (!PRIVATE_KEY.equals(privateKey))
				throw new PrivateKeyException("Incorrect PrivateKey !");
			mofServiceImplementor.refreshFundsWithMonthsBack(yearsBack, monthsBack, privateKey);
		} catch (PrivateKeyException e) {
			Log.error("Got PrivateKeyException in mofService.refreshFundsWithMonthsBack, Error: "
					+ e);
			throw new WebServiceException(
					"PrivateKeyException occured during retrieval of refreshFunds, Error: "
							+ e);
		} catch (Exception e) {
			Log.error("Got exception in mofService.refreshFundsWithMonthsBack, Error: " + e);
			throw new WebServiceException(
					"Exception occured during refresh-funds activation, Error: "
							+ e);
		}
	}*/

	@WebMethod(operationName = "getFundsByParamJSON")
	@WebResult(name = "FUNDS_JSON")
	@SOAPBinding(style = Style.DOCUMENT, use = Use.LITERAL, parameterStyle = ParameterStyle.WRAPPED)
	public String getFundsByParamJSON(@WebParam(name = "id") List<Integer> ids,
			@WebParam(name = "reportType") String reportType,
			@WebParam(name = "sug") String sug,
			@WebParam(name = "startDate") Date startDate,
			@WebParam(name = "endDate") Date endDate,
			@WebParam(name = "maxRecords") int maxRecords,
			@WebParam(name = "privateKey") String privateKey) {
		try {
			if (!PRIVATE_KEY.equals(privateKey))
				throw new PrivateKeyException("Incorrect PrivateKey !");
			return mofServiceImplementor.getFundsByParamJSON(ids, reportType,
					sug, startDate, endDate, maxRecords, privateKey);
		} catch (PrivateKeyException e) {
			Log.error("Got PrivateKeyException in mofService.getFundsByParamJSON, Error: "
					+ e);
			throw new WebServiceException(
					"PrivateKeyException occured during retrieval of getFundsByParamJSON, Error: "
							+ e);
		} catch (Exception e) {
			Log.error("Got exception in mofService.getFundsByParamJSON, Error: "
					+ e);
			throw new WebServiceException(
					"Exception occured during getFundsByParamJSON execution, Error: "
							+ e);
		}
	}

	@WebMethod(operationName = "getGemelFundsByParamJSON")
	@WebResult(name = "GemelResultsJSON")
	@SOAPBinding(style = Style.DOCUMENT, use = Use.LITERAL, parameterStyle = ParameterStyle.WRAPPED)
	public String getGemelFundsByParamJSON(
			@WebParam(name = "id") List<Integer> ids,
			@WebParam(name = "reportType") String reportType,
			@WebParam(name = "sug") String sug,
			@WebParam(name = "maslulHashkaa") Integer maslulHashkaa,
			@WebParam(name = "startDate") Date startDate,
			@WebParam(name = "endDate") Date endDate,
			@WebParam(name = "maxRecords") int maxRecords,
			@WebParam(name = "privateKey") String privateKey) {
		try {
			if (!PRIVATE_KEY.equals(privateKey))
				throw new PrivateKeyException("Incorrect PrivateKey !");
			return mofServiceImplementor.getGemelFundsByParamJSON(ids,
					reportType, sug, maslulHashkaa, startDate, endDate,
					maxRecords, privateKey);
		} catch (PrivateKeyException e) {
			Log.error("Got PrivateKeyException in mofService.getGemelFundsByParamJSON, Error: "
					+ e);
			throw new WebServiceException(
					"PrivateKeyException occured during retrieval of getGemelFundsByParamJSON, Error: "
							+ e);
		} catch (Exception e) {
			Log.error("Got exception in mofService.getGemelFundsByParamJSON, Error: "
					+ e);
			throw new WebServiceException(
					"Exception occured during getGemelFundsByParamJSON execution, Error: "
							+ e);
		}
	}

	@Override
	@WebMethod(operationName = "getGemelFundsByParamPartsJSON")
	@WebResult(name = "GEMEL_RESULT_PART_JSON")
	@SOAPBinding(style = Style.DOCUMENT, use = Use.LITERAL, parameterStyle = ParameterStyle.WRAPPED)
	public String[] getGemelFundsByParamPartsJSON(
			@WebParam(name = "id") List<Integer> ids,
			@WebParam(name = "reportType") String reportType,
			@WebParam(name = "sug") String sug,
			@WebParam(name = "maslulHashkaa") Integer maslulHashkaa,
			@WebParam(name = "startDate") Date startDate,
			@WebParam(name = "endDate") Date endDate,
			@WebParam(name = "maxRecords") int maxRecords,
			@WebParam(name = "privateKey") String privateKey) {
		try {
			if (!PRIVATE_KEY.equals(privateKey))
				throw new PrivateKeyException("Incorrect PrivateKey !");

			return mofServiceImplementor.getGemelFundsByParamPartsJSON(ids,
					reportType, sug, maslulHashkaa, startDate, endDate,
					maxRecords, privateKey);
		} catch (PrivateKeyException e) {
			Log.error("Got PrivateKeyException in mofService.getGemelFundsByParamPartsJSON, Error: "
					+ e);
			throw new WebServiceException(
					"PrivateKeyException occured during retrieval of getGemelFundsByParamPartsJSON, Error: "
							+ e);
		} catch (Exception e) {
			Log.error("Got exception in mofService.getGemelFundsByParamPartsJSON, Error: "
					+ e);
			throw new WebServiceException(
					"Exception occured during getGemelFundsByParamPartsJSON execution, Error: "
							+ e);
		}
	}

	@Override
	@WebMethod(operationName = "getGemelFundsByParam")
	@WebResult(name = "GemelResult")
	@SOAPBinding(style = Style.DOCUMENT, use = Use.LITERAL, parameterStyle = ParameterStyle.WRAPPED)
	public List<GemelResult> getGemelFundsByParam(
			@WebParam(name = "id") List<Integer> ids,
			@WebParam(name = "reportType") String reportType,
			@WebParam(name = "sug") String sug,
			@WebParam(name = "maslulHashkaa") Integer maslulHashkaa,
			@WebParam(name = "startDate") Date startDate,
			@WebParam(name = "endDate") Date endDate,
			@WebParam(name = "maxRecords") int maxRecords,
			@WebParam(name = "privateKey") String privateKey) {
		try {
			if (!PRIVATE_KEY.equals(privateKey))
				throw new PrivateKeyException("Incorrect PrivateKey !");

			return mofServiceImplementor.getGemelFundsByParam(ids, reportType,
					sug, maslulHashkaa, startDate, endDate, maxRecords,
					privateKey);

		} catch (PrivateKeyException e) {
			Log.error("Got PrivateKeyException in mofService.getGemelFundsByParam, Error: "
					+ e);
			throw new WebServiceException(
					"PrivateKeyException occured during retrieval of getGemelFundsByParam, Error: "
							+ e);
		} catch (Exception e) {
			Log.error("Got exception in mofService.getGemelFundsByParam, Error: "
					+ e);
			throw new WebServiceException(
					"Exception occured during getGemelFundsByParam execution, Error: "
							+ e);
		}
	}

	@WebMethod(operationName = "getInsuranceFundsByParamJSON")
	@WebResult(name = "InsuranceResultsJSON")
	@SOAPBinding(style = Style.DOCUMENT, use = Use.LITERAL, parameterStyle = ParameterStyle.WRAPPED)
	public String getInsuranceFundsByParamJSON(
			@WebParam(name = "id") List<Integer> ids,
			@WebParam(name = "reportType") String reportType,
			@WebParam(name = "sug") String sug,
			@WebParam(name = "maslulHashkaa") Integer maslulHashkaa,
			@WebParam(name = "startDate") Date startDate,
			@WebParam(name = "endDate") Date endDate,
			@WebParam(name = "maxRecords") int maxRecords,
			@WebParam(name = "privateKey") String privateKey) {
		try {
			if (!PRIVATE_KEY.equals(privateKey))
				throw new PrivateKeyException("Incorrect PrivateKey !");

			return mofServiceImplementor.getInsuranceFundsByParamJSON(ids,
					reportType, sug, maslulHashkaa, startDate, endDate,
					maxRecords, privateKey);

		} catch (PrivateKeyException e) {
			Log.error("Got PrivateKeyException in mofService.getInsuranceFundsByParamJSON, Error: "
					+ e);
			throw new WebServiceException(
					"PrivateKeyException occured during retrieval of getInsuranceFundsByParamJSON, Error: "
							+ e);
		} catch (Exception e) {
			Log.error("Got exception in mofService.getInsuranceFundsByParamJSON, Error: "
					+ e);
			throw new WebServiceException(
					"Exception occured during getInsuranceFundsByParamJSON execution, Error: "
							+ e);
		}
	}

	@WebMethod(operationName = "getPensionFundsByParamJSON")
	@WebResult(name = "PensionResultsJSON")
	@SOAPBinding(style = Style.DOCUMENT, use = Use.LITERAL, parameterStyle = ParameterStyle.WRAPPED)
	public String getPensionFundsByParamJSON(
			@WebParam(name = "id") List<Integer> ids,
			@WebParam(name = "reportType") String reportType,
			@WebParam(name = "sug") String sug,
			@WebParam(name = "maslulHashkaa") Integer maslulHashkaa,
			@WebParam(name = "startDate") Date startDate,
			@WebParam(name = "endDate") Date endDate,
			@WebParam(name = "maxRecords") int maxRecords,
			@WebParam(name = "privateKey") String privateKey) {

		try {
			if (!PRIVATE_KEY.equals(privateKey))
				throw new PrivateKeyException("Incorrect PrivateKey !");

			return mofServiceImplementor.getPensionFundsByParamJSON(ids,
					reportType, sug, maslulHashkaa, startDate, endDate,
					maxRecords, privateKey);

		} catch (PrivateKeyException e) {
			Log.error("Got PrivateKeyException in mofService.getPensionFundsByParamJSON, Error: "
					+ e);
			throw new WebServiceException(
					"PrivateKeyException occured during retrieval of getPensionFundsByParamJSON, Error: "
							+ e);
		} catch (Exception e) {
			Log.error("Got exception in mofService.getPensionFundsByParamJSON, Error: "
					+ e);
			throw new WebServiceException(
					"Exception occured during getPensionFundsByParamJSON execution, Error: "
							+ e);
		}
	}

}
