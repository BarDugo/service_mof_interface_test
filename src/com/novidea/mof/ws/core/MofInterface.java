package com.novidea.mof.ws.core;

import java.util.List;

import com.novidea.mof.calculations.result.GemelResult;
import com.novidea.mof.calculations.result.InsuranceResult;
import com.novidea.mof.calculations.result.PensionResult;
import com.novidea.mof.entities.model.Fund;
import com.novidea.mof.ws.MofInterfaceExpose;

/**
 * Full interface for implementation of all core application functonalities.
 * 
 * Deprecated functionalities separated to different interface to keep
 * implementation
 * 
 * @author Galil.Zussman
 * @date Apr 15, 2014
 */
public interface MofInterface extends MofInterfaceExpose {

	/**
	 * WebService exposed method
	 * 
	 * @param nativeQuery
	 *            - mySQL syntax native query
	 * @return String[]
	 */
	public abstract String[] getFundsByNativeQueryPartsJSON(String nativeQuery);

	/**
	 * WebService exposed method
	 * 
	 * @param nativeQuery
	 *            - mySQL syntax native query
	 * @return List<Fund>
	 */
	public abstract List<GemelResult> getGemelFundsByNativeQuery(
			String nativeQuery);

	/**
	 * WebService exposed method
	 * 
	 * @param nativeQuery
	 *            - mySQL syntax native query
	 * @return JSON String
	 */
	public abstract String getFundsByNativeQueryJSON(String nativeQuery);

	/**
	 * WebService exposed method
	 * 
	 * @param nativeQuery
	 *            - mySQL syntax native query
	 * @return List<Fund>
	 */
	public abstract String getGemelFundsByNativeQueryJSON(String nativeQuery);

	/**
	 * WebService exposed method
	 * 
	 * @param nativeQuery
	 *            - mySQL syntax native query
	 * @return List<Fund>
	 */
	public abstract String getInsuranceFundsByNativeQueryJSON(String nativeQuery);

	/**
	 * WebService exposed method
	 * 
	 * @param nativeQuery
	 *            - mySQL syntax native query
	 * @return List<Fund>
	 */
	public abstract List<Fund> getFundsByNativeQuery(String nativeQuery);

	/**
	 * WebService exposed method
	 * 
	 * @param nativeQuery
	 *            - mySQL syntax native query
	 * @return List<Fund>
	 */
	public abstract String getPensionFundsByNativeQueryJSON(String nativeQuery);

	/**
	 * WebService exposed method
	 * 
	 * @param nativeQuery
	 *            - mySQL syntax native query
	 * @return List<Fund>
	 */
	public abstract List<PensionResult> getPensionFundsByNativeQuery(
			String nativeQuery);

	/**
	 * WebService exposed method
	 * 
	 * @param nativeQuery
	 *            - mySQL syntax native query
	 * @return List<Fund>
	 */
	public abstract List<InsuranceResult> getInsuranceFundsByNativeQuery(
			String nativeQuery);

}
