package com.novidea.mof.ws.core;

import java.util.Date;
import java.util.List;

import org.joda.time.DateTime;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.novidea.mof.DataBaseBuilder;
import com.novidea.mof.Log;
import com.novidea.mof.calculations.CalculationExecuter;
import com.novidea.mof.calculations.InsuranceResultsBuilder;
import com.novidea.mof.calculations.cache.GemelCachedReport;
import com.novidea.mof.calculations.cache.InsuranceCachedReport;
import com.novidea.mof.calculations.cache.PensionCachedReport;
import com.novidea.mof.calculations.result.GemelResult;
import com.novidea.mof.calculations.result.GeneralResult;
import com.novidea.mof.calculations.result.InsuranceResult;
import com.novidea.mof.calculations.result.PensionResult;
import com.novidea.mof.calculations.result.TypeElements;
import com.novidea.mof.entities.ReportType;
import com.novidea.mof.entities.model.Fund;
import com.novidea.mof.entities.service.ServiceHolder;
import com.novidea.mof.exception.FailedToGetReportTypeException;
import com.novidea.mof.exception.RefreshFundsException;

/**
 * 
 * @author Galil.Zussman (Redesign)
 * @date May 28, 2014
 */
public class MofServiceImpl implements MofInterface {

	private static final int MAX_STRING_SIZE = 100000;

	private ServiceHolder serviceHolder = ServiceHolder.getInstance();

	@Override
	public TypeElements getTypeElements(String reportType, String privateKey)
			throws FailedToGetReportTypeException {
		if (!reportType.equals(ReportType.GEMEL_DETAILED_REPORT.getValue())
				&& !reportType.equals(ReportType.INSURANCE_DETAILED_REPORT
						.getValue())
				&& !reportType.equals(ReportType.PENSION_DETAILED_REPORT
						.getValue()))
			throw new FailedToGetReportTypeException(
					"unknown report type of: \"" + reportType + "\"");

		TypeElements typeElements = new TypeElements();
		if (reportType.equals(ReportType.INSURANCE_DETAILED_REPORT.getValue())) {
			typeElements.lastReportedMonth = InsuranceCachedReport
					.getInstance().getLastReportedMonth();
			typeElements.companyNames = InsuranceCachedReport.getInstance()
					.getCompanyNames();
			typeElements.allIds = InsuranceCachedReport.getInstance()
					.getAllIds();
			typeElements.productTypes = InsuranceCachedReport.getInstance()
					.getProductTypes();
		}
		if (reportType.equals(ReportType.GEMEL_DETAILED_REPORT.getValue())) {
			typeElements.lastReportedMonth = GemelCachedReport.getInstance()
					.getLastReportedMonth();
			typeElements.companyNames = GemelCachedReport.getInstance()
					.getCompanyNames();
			typeElements.allIds = GemelCachedReport.getInstance().getAllIds();
			typeElements.productTypes = GemelCachedReport.getInstance()
					.getProductTypes();
		}
		if (reportType.equals(ReportType.PENSION_DETAILED_REPORT.getValue())) {
			typeElements.lastReportedMonth = PensionCachedReport.getInstance()
					.getLastReportedMonth();
			typeElements.companyNames = PensionCachedReport.getInstance()
					.getCompanyNames();
			typeElements.allIds = PensionCachedReport.getInstance().getAllIds();
			typeElements.productTypes = PensionCachedReport.getInstance()
					.getProductTypes();
		}
		return typeElements;
	}

	@Override
	public Fund getFundById(String id, String privateKey) {
		return serviceHolder.fundService.findById(Long.valueOf(id));
	}

	@Override
	public List<Fund> getFundsByNativeQuery(String nativeQuery) {
		return serviceHolder.fundService
				.executeNativeSqlCustomQuery(nativeQuery);
	}

	@Override
	public String getFundsByNativeQueryJSON(String nativeQuery) {
		List<Fund> funds = serviceHolder.fundService
				.executeNativeSqlCustomQuery(nativeQuery);
		// ViewHalper.resetNullFieldsFunds(funds);

		String jsonStr = new Gson().toJson(funds);
		return jsonStr;
	}

	@Override
	public String getFundsByParamJSON(List<Integer> ids, String reportType,
			String sug, Date startDate, Date endDate, int maxRecords,
			String privateKey) {
		List<Fund> funds = serviceHolder.fundService.mainFilteredFetch(ids,
				reportType, sug, DateTime.now().toDate(), startDate, endDate,
				maxRecords);
		String jsonStr = new Gson().toJson(funds);
		return jsonStr;
	}

	@Override
	public String getGemelFundsByParamJSON(List<Integer> ids,
			String reportType, String sug, Integer maslulHashkaa,
			Date startDate, Date endDate, int maxRecords, String privateKey) {
		List<Fund> funds = serviceHolder.fundService.mainFilteredFetch(ids,
				reportType, sug, DateTime.now().toDate(), startDate, endDate,
				maxRecords);
		// taking care of the data moved to client side
		// ViewHalper.resetNullFieldsFunds(funds);

		String jsonStr = "";
		if (funds.size() != 0) {// nothing to do otherwise
			List<GemelResult> gemelResults = CalculationExecuter
					.getGemelResults(funds, maslulHashkaa);
			// ronk // jsonStr = new Gson().toJson(gemelResults);
			jsonStr = new GsonBuilder().serializeSpecialFloatingPointValues().create().toJson(gemelResults);
			Log.info("====>>>>130::::" + jsonStr);
		}
		return jsonStr;
	}

	@Override
	public String[] getGemelFundsByParamPartsJSON(List<Integer> ids,
			String reportType, String sug, Integer maslulHashkaa,
			Date startDate, Date endDate, int maxRecords, String privateKey) {
		List<Fund> funds = serviceHolder.fundService.mainFilteredFetch(ids,
				reportType, sug, DateTime.now().toDate(), startDate, endDate,
				maxRecords);

		String jsonStr = "";
		if (funds.size() != 0) {// nothing to do otherwise
			List<GemelResult> gemelResults = CalculationExecuter
					.getGemelResults(funds, maslulHashkaa);
			// ronk // jsonStr = new Gson().toJson(gemelResults);
			jsonStr = new GsonBuilder().serializeSpecialFloatingPointValues().create().toJson(gemelResults);
			Log.info("====>>>>148::::" + jsonStr);
		}
		return SplitStringToParts(jsonStr);
	}

	@Override
	public List<GemelResult> getGemelFundsByParam(List<Integer> ids,
			String reportType, String sug, Integer maslulHashkaa,
			Date startDate, Date endDate, int maxRecords, String privateKey) {
		List<Fund> funds = serviceHolder.fundService.mainFilteredFetch(ids,
				reportType, sug, DateTime.now().toDate(), startDate, endDate,
				maxRecords);
		return CalculationExecuter.getGemelResults(funds, maslulHashkaa);

	}

	@Override
	public String getGemelFundsByNativeQueryJSON(String nativeQuery) {
		List<Fund> funds = serviceHolder.fundService
				.executeNativeSqlCustomQuery(nativeQuery);
		if (funds.size() != 0) {
			String jsonStr = new Gson().toJson(CalculationExecuter
					.getGemelResults(funds, null));

			return jsonStr;
		} else
			return "{}";
	}

	@Override
	public List<GemelResult> getGemelFundsByNativeQuery(String nativeQuery) {
		List<Fund> funds = serviceHolder.fundService
				.executeNativeSqlCustomQuery(nativeQuery);
		return CalculationExecuter.getGemelResults(funds, null);
	}

	@Override
	public String getPensionFundsByParamJSON(List<Integer> ids,
			String reportType, String sug, Integer maslulHashkaa,
			Date startDate, Date endDate, int maxRecords, String privateKey) {
		List<Fund> funds = serviceHolder.fundService.mainFilteredFetch(ids,
				reportType, sug, DateTime.now().toDate(), startDate, endDate,
				maxRecords);
		String jsonStr = "{}";
		if (!funds.isEmpty())
			jsonStr = new Gson().toJson(CalculationExecuter.getPensionResults(
					funds, maslulHashkaa));
		return jsonStr;
	}

	@Override
	public String getPensionFundsByNativeQueryJSON(String nativeQuery) {
		List<Fund> funds = serviceHolder.fundService
				.executeNativeSqlCustomQuery(nativeQuery);
		if (funds.size() != 0) {
			String jsonStr = new Gson().toJson(CalculationExecuter
					.getPensionResults(funds, null));
			return jsonStr;
		} else
			return "{}";
	}

	@Override
	public List<PensionResult> getPensionFundsByNativeQuery(String nativeQuery) {
		List<Fund> funds = serviceHolder.fundService
				.executeNativeSqlCustomQuery(nativeQuery);
		return CalculationExecuter.getPensionResults(funds, null);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<InsuranceResult> getInsuranceFundsByNativeQuery(
			String nativeQuery) {
		List<Fund> funds = serviceHolder.fundService
				.executeNativeSqlCustomQuery(nativeQuery);
		List<? extends GeneralResult> results = InsuranceResultsBuilder
				.getInsuranceResults(funds, null);
		return (List<InsuranceResult>) results;
	}

	@Override
	public String getInsuranceFundsByNativeQueryJSON(String nativeQuery) {
		List<Fund> funds = serviceHolder.fundService
				.executeNativeSqlCustomQuery(nativeQuery);
		// ViewHalper.resetNullFieldsFunds(funds);

		String jsonStr = new Gson().toJson(InsuranceResultsBuilder
				.getInsuranceResults(funds, null));
		return jsonStr;
	}

	@SuppressWarnings("unchecked")
	@Override
	public String getInsuranceFundsByParamJSON(List<Integer> ids,
			String reportType, String sug, Integer maslulHashkaa,
			Date startDate, Date endDate, int maxRecords, String privateKey) {
		List<Fund> funds = serviceHolder.fundService.mainFilteredFetch(ids,
				reportType, sug, DateTime.now().toDate(), startDate, endDate,
				maxRecords);

		String jsonStr = "";
		if (funds.size() != 0) {// nothing to do otherwise
			List<? extends GeneralResult> insuranceResults = InsuranceResultsBuilder
					.getInsuranceResults(funds, maslulHashkaa);
			jsonStr = new Gson()
					.toJson((List<InsuranceResult>) insuranceResults);
		}
		return jsonStr;
	}

	@Override
	public String[] getFundsByNativeQueryPartsJSON(String nativeQuery) {
		List<Fund> funds = serviceHolder.fundService
				.executeNativeSqlCustomQuery(nativeQuery);
		return SplitStringToParts(new Gson().toJson(funds));
	}

	private String[] SplitStringToParts(final String sourceStr) {
		String[] resultParts = new String[sourceStr.length() / MAX_STRING_SIZE
				+ 1];
		System.out.println("Start splitting " + sourceStr.length() + " to "
				+ resultParts.length + " parts of size " + MAX_STRING_SIZE);
		for (int i = 0, offset = 0; i < resultParts.length; i++, offset += MAX_STRING_SIZE) {
			int len = Math.min(sourceStr.length() - offset, MAX_STRING_SIZE);
			System.out.println("JSON Part[" + i + "]: " + offset + "\tTo: "
					+ (offset + len - 1));
			resultParts[i] = sourceStr.substring(offset, offset + len);
		}
		return resultParts;
	}

	@Override
	public void refreshFunds(Integer yearsBack, String privateKey)
			throws RefreshFundsException {
		try {
			DataBaseBuilder.execute(yearsBack, null);
		} catch (Exception e) {
			throw new RefreshFundsException(e);
		}
	}
	
	public void refreshFundsWithMonthsBack(Integer yearsBack, Integer monthsBack, String privateKey)
			throws RefreshFundsException {
		try {
			DataBaseBuilder.execute(yearsBack,monthsBack);
		} catch (Exception e) {
			throw new RefreshFundsException(e);
		}
	}

	public List<Fund> getFundsByNameAndTypeLocal(String namePart, String type) {
		return serviceHolder.fundService.getFundsByNameAndType(namePart, type);
	}

}
