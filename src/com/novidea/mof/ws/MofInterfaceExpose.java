package com.novidea.mof.ws;

import java.util.Date;
import java.util.List;

import com.novidea.mof.calculations.result.GemelResult;
import com.novidea.mof.calculations.result.LastMonthResult;
import com.novidea.mof.calculations.result.TypeElements;
import com.novidea.mof.entities.model.Fund;
import com.novidea.mof.exception.FailedToGetReportTypeException;
import com.novidea.mof.exception.RefreshFundsException;

/**
 * {@link MofInterfaceExpose} is an interface that define all of the web
 * services that this application expose to the outside world
 * 
 * @author Galil.Zussman
 * @date Nov 3, 2014
 */
public interface MofInterfaceExpose {

	/**
	 * WebService exposed method
	 * 
	 * @param reportType
	 * @return
	 * @throws FailedToGetReportTypeException 
	 */
	public abstract TypeElements getTypeElements(String reportType,
			String privateKey) throws FailedToGetReportTypeException;

	/**
	 * WebService exposed method
	 * 
	 * @param id
	 * @return
	 */
	public abstract Fund getFundById(String id, String privateKey);

	/**
	 * WebService exposed method
	 * 
	 * @param reportType
	 * @param sug
	 * @param startDate
	 * @param endDate
	 * @param maxRecords
	 * @return
	 */
	public abstract String getFundsByParamJSON(List<Integer> ids,
			String reportType, String sug, Date startDate, Date endDate,
			int maxRecords, String privateKey);

	/**
	 * getGemelFundsByParamJSON - WebService exposed method
	 * 
	 * @param reportType
	 * @param sug
	 * @param startDate
	 * @param endDate
	 * @param maxRecords
	 * @return
	 */
	public abstract String getGemelFundsByParamJSON(List<Integer> ids,
			String reportType, String sug, Integer maslulHashkaa,
			Date startDate, Date endDate, int maxRecords, String privateKey);

	/**
	 * getGemelFundsByParamJSON - WebService exposed method
	 * 
	 * @param reportType
	 * @param sug
	 * @param startDate
	 * @param endDate
	 * @param maxRecords
	 * @return
	 */
	public abstract String[] getGemelFundsByParamPartsJSON(List<Integer> ids,
			String reportType, String sug, Integer maslulHashkaa,
			Date startDate, Date endDate, int maxRecords, String privateKey);

	/**
	 * getGemelFundsByParam - for building the {@link LastMonthResult}
	 * 
	 * @param reportType
	 * @param sug
	 * @param maslulHashkaa
	 * @param startDate
	 * @param endDate
	 * @param maxRecords
	 * @return
	 */
	public abstract List<GemelResult> getGemelFundsByParam(List<Integer> ids,
			String reportType, String sug, Integer maslulHashkaa,
			Date startDate, Date endDate, int maxRecords, String privateKey);

	/**
	 * getPensionFundsByParamJSON - WebService exposed method
	 * 
	 * @param reportType
	 * @param sug
	 * @param startDate
	 * @param endDate
	 * @param maxRecords
	 * @return
	 */
	public abstract String getPensionFundsByParamJSON(List<Integer> ids,
			String reportType, String sug, Integer maslulHashkaa,
			Date startDate, Date endDate, int maxRecords, String privateKey);

	/**
	 * WebService exposed method
	 * 
	 * @param ids
	 * @param reportType
	 * @param sug
	 * @param maslulHashkaa
	 * @param startDate
	 * @param endDate
	 * @param maxRecords
	 * @return
	 */
	public abstract String getInsuranceFundsByParamJSON(List<Integer> ids,
			String reportType, String sug, Integer maslulHashkaa,
			Date startDate, Date endDate, int maxRecords, String privateKey);

	/**
	 * WebService exposed method - loads all funds into db
	 * 
	 * @throws RefreshFundsException
	 */
	public abstract void refreshFunds(Integer yearsBack, String privateKey)
			throws RefreshFundsException;

	/**
	 * WebService exposed method - loads all funds into db
	 * 
	 * @throws RefreshFundsException
	 */
	//public abstract void refreshFundsWithMonthsBack(Integer yearsBack, Integer monthsBack, String privateKey)
	//		throws RefreshFundsException;

}