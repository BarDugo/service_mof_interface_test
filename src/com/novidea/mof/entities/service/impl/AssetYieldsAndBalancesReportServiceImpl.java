package com.novidea.mof.entities.service.impl;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import com.novidea.mof.entities.dao.sql.AssetYieldsAndBalancesReportDao;
import com.novidea.mof.entities.model.AssetYieldsAndBalancesReport;
import com.novidea.mof.entities.service.AssetYieldsAndBalancesReportService;

/**
 * 
 * @author Galil.Zussman
 * @date May 8, 2015
 */
public class AssetYieldsAndBalancesReportServiceImpl implements
		AssetYieldsAndBalancesReportService, Serializable {

	private static final long serialVersionUID = 1L;

	private AssetYieldsAndBalancesReportDao assetYieldsAndBalancesReportDao = AssetYieldsAndBalancesReportDao
			.getInstance();

	@Override
	public void saveOrUpdateAll(
			List<AssetYieldsAndBalancesReport> assetYieldsAndBalancesReports) {
		assetYieldsAndBalancesReportDao
				.saveOrUpdateAll(assetYieldsAndBalancesReports);

	}

	@Override
	public List<AssetYieldsAndBalancesReport> findByHODESH_DIVUACH(Date date,
			String assetYieldsAndBalancesReportTYPE) {
		return assetYieldsAndBalancesReportDao.findByHODESH_DIVUACH(date,
				assetYieldsAndBalancesReportTYPE);
	}
}
