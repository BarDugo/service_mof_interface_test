package com.novidea.mof.entities.service.impl;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import com.novidea.mof.entities.dao.sql.AssetFullDetailedReportDao;
import com.novidea.mof.entities.model.AssetFullDetailedReport;
import com.novidea.mof.entities.service.AssetFullDetailedReportService;

/**
 * 
 * @author Galil.Zussman
 * @date Sep 4, 2014
 */
public class AssetFullDetailedReportServiceImpl implements Serializable,
		AssetFullDetailedReportService {

	private static final long serialVersionUID = 1L;

	private AssetFullDetailedReportDao assetFullDetailedReportDao = AssetFullDetailedReportDao
			.getInstance();

	public AssetFullDetailedReportDao getAssetFullDetailedReportDao() {
		return assetFullDetailedReportDao;
	}

	public void setAssetFullDetailedReportDao(
			AssetFullDetailedReportDao assetFullDetailedReportDao) {
		this.assetFullDetailedReportDao = assetFullDetailedReportDao;
	}

	@Override
	public void saveOrUpdateAll(List<AssetFullDetailedReport> reportEntities) {
		assetFullDetailedReportDao.saveOrUpdateAll(reportEntities);
	}

	@Override
	public AssetFullDetailedReport findById(long id) {
		return assetFullDetailedReportDao.findById(id);
	}

	@Override
	public List<AssetFullDetailedReport> findByHODESH_DIVUACH(Date date,String ASSET_FULL_DETAILED_REPORT_HOLDER_TYPE) {

		return assetFullDetailedReportDao.findByHODESH_DIVUACH(date,ASSET_FULL_DETAILED_REPORT_HOLDER_TYPE);
	}

	@Override
	public List<AssetFullDetailedReport> executeNativeSqlCustomQuery(
			String theQueryString) {
		return assetFullDetailedReportDao.executeNativeSqlCustomQuery(theQueryString);
	}

}
