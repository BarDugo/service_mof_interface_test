package com.novidea.mof.entities.service.impl;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import com.novidea.mof.entities.dao.sql.AssetMainGroupDetailedReportDao;
import com.novidea.mof.entities.model.AssetMainGroupDetailedReport;
import com.novidea.mof.entities.service.AssetMainGroupDetailedReportService;

/**
 * 
 * @author Galil.Zussman
 * @date Sep 4, 2014
 */
public class AssetMainGroupDetailedReportServiceImpl implements
		AssetMainGroupDetailedReportService, Serializable {

	private static final long serialVersionUID = 1L;

	private AssetMainGroupDetailedReportDao assetMainGroupDetailedReportDao = AssetMainGroupDetailedReportDao
			.getInstance();

	public AssetMainGroupDetailedReportDao getAssetMainGroupDetailedReportDao() {
		return assetMainGroupDetailedReportDao;
	}

	public void setAssetMainGroupDetailedReportDao(
			AssetMainGroupDetailedReportDao assetMainGroupDetailedReportDao) {
		this.assetMainGroupDetailedReportDao = assetMainGroupDetailedReportDao;
	}

	@Override
	public void saveOrUpdateAll(
			List<AssetMainGroupDetailedReport> assetMainGroupDetailedReports) {
		assetMainGroupDetailedReportDao
				.saveOrUpdateAll(assetMainGroupDetailedReports);

	}

	@Override
	public List<AssetMainGroupDetailedReport> findByHODESH_DIVUACH(Date date,
			String assetMainGroupDetailedReportTYPE) {
		return assetMainGroupDetailedReportDao.findByHODESH_DIVUACH(date,
				assetMainGroupDetailedReportTYPE);
	}
}
