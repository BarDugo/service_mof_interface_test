package com.novidea.mof.entities.service.impl;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import com.novidea.mof.calculations.CalculationExecuter;
import com.novidea.mof.entities.dao.sql.FundDao;
import com.novidea.mof.entities.model.Fund;
import com.novidea.mof.entities.service.FundService;

/**
 * 
 * @author Galil.Zussman
 * @date Sep 4, 2014
 */
public class FundServiceImpl implements FundService, Serializable {

	private static final long serialVersionUID = 1L;

	private FundDao fundDao = FundDao.getInstance();

	public void setFundDao(FundDao fundDao) {
		this.fundDao = fundDao;
	}

	public void saveOrUpdateAll(List<Fund> fundEntities) {
		fundDao.saveOrUpdateAll(fundEntities);
	}

	@Override
	public List<Fund> mainFilteredFetch(List<Integer> ids, String reportType,
			String sug, Date endOfActivityDate, Date startDate, Date endDate,
			int maxRecords) {
		return fundDao.mainFilteredFetch(ids, reportType, sug,
				endOfActivityDate, startDate, endDate, maxRecords,
				CalculationExecuter.EXCLUDE_EOL_FUNDS);
	}

	@Override
	public Fund findById(Long id) {
		return this.fundDao.findById(id);
	}

	@Override
	public List<Fund> getFundsByNameAndType(String namePart, String type) {
		return this.fundDao.getFundsByNameAndType(namePart, type);
	}

	@Override
	public List<Fund> executeNativeSqlCustomQuery(String theQueryString) {
		return this.fundDao.executeNativeSqlCustomQuery(theQueryString);
	}

	@Override
	public List<Fund> fetchTsuaaYearlyResults(String reportType,
			Date startDate, int minusYears, int maxRecords) {
		return this.fundDao.fetchTsuaaYearlyResults(reportType, startDate,
				minusYears, maxRecords);
	}

}
