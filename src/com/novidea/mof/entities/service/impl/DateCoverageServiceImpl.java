package com.novidea.mof.entities.service.impl;

import java.io.Serializable;
import java.util.List;

import com.novidea.mof.entities.ReportType;
import com.novidea.mof.entities.dao.sql.DateCoverageDao;
import com.novidea.mof.entities.model.DateCoverage;
import com.novidea.mof.entities.service.DateCoverageService;

/**
 *  
 * @author Galil.Zussman
 * @date Sep 4, 2014
 */
public class DateCoverageServiceImpl implements DateCoverageService,
		Serializable {

	private static final long serialVersionUID = 1L;

	private DateCoverageDao dateCoverageDao = DateCoverageDao.getInstance();

	public DateCoverageDao getDateCoverageDao() {
		return dateCoverageDao;
	}

	public void setDateCoverageDao(DateCoverageDao dateCoverageDao) {
		this.dateCoverageDao = dateCoverageDao;
	}

	@Override
	public List<DateCoverage> findMonthOfYearAndType(int year, int monthOfYear,
			ReportType reportType) {
		return dateCoverageDao.findMonthOfYearAndType(year, monthOfYear,
				reportType);
	}

	@Override
	public void saveOrUpdateAll(List<DateCoverage> dateCoverages) {
		dateCoverageDao.saveOrUpdateAll(dateCoverages);

	}

}
