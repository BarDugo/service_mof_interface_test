package com.novidea.mof.entities.service;

import java.util.Date;
import java.util.List;

import com.novidea.mof.entities.model.AssetMainGroupDetailedReport;

/**
 * 
 * @author Galil.Zussman
 * @date Sep 4, 2014
 */
public interface AssetMainGroupDetailedReportService {

	void saveOrUpdateAll(
			List<AssetMainGroupDetailedReport> assetMainGroupDetailedReports);

	public List<AssetMainGroupDetailedReport> findByHODESH_DIVUACH(Date date,
			String assetMainGroupDetailedReportTYPE);

}
