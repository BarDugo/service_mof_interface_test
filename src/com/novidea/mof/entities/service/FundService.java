package com.novidea.mof.entities.service;

import java.util.Date;
import java.util.List;

import com.novidea.mof.entities.model.Fund;

/**
 * 
 * @author Galil.Zussman
 * @date Sep 4, 2014
 */
public interface FundService {

	/**
	 * 
	 * @param reportType
	 * @param sug
	 * @param startDate
	 * @param endDate
	 * @param maxRecords
	 * @return
	 */
	public List<Fund> mainFilteredFetch(List<Integer> ids, String reportType,
			String sug, Date endOfActivityDate, Date startDate, Date endDate,
			int maxRecords);

	public void saveOrUpdateAll(List<Fund> fundEntities);

	public Fund findById(Long id);

	public List<Fund> getFundsByNameAndType(String namePart, String type);

	public List<Fund> executeNativeSqlCustomQuery(String theQueryString);

	public List<Fund> fetchTsuaaYearlyResults(String reportType,
			Date startDate, int minusYears, int maxRecords);

}