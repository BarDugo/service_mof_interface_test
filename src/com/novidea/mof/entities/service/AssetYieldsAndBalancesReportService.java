package com.novidea.mof.entities.service;

import java.util.Date;
import java.util.List;

import com.novidea.mof.entities.model.AssetYieldsAndBalancesReport;

/**
 * 
 * @author Galil.Zussman
 * @date Sep 4, 2014
 */
public interface AssetYieldsAndBalancesReportService {

	public void saveOrUpdateAll(List<AssetYieldsAndBalancesReport> reportEntities);

	public List<AssetYieldsAndBalancesReport> findByHODESH_DIVUACH(Date date, String ASSET_FULL_DETAILED_REPORT_HOLDER_TYPE);
	

}