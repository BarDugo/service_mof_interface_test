package com.novidea.mof.entities.service;

import java.util.Date;
import java.util.List;

import com.novidea.mof.entities.model.AssetFullDetailedReport;

/**
 * 
 * @author Galil.Zussman
 * @date Sep 4, 2014
 */
public interface AssetFullDetailedReportService {

	public void saveOrUpdateAll(List<AssetFullDetailedReport> reportEntities);

	public List<AssetFullDetailedReport> findByHODESH_DIVUACH(Date date, String ASSET_FULL_DETAILED_REPORT_HOLDER_TYPE);
	
	public List<AssetFullDetailedReport>  executeNativeSqlCustomQuery(String theQueryString);

	public AssetFullDetailedReport findById(long id);

}