package com.novidea.mof.entities.service;

import java.io.Serializable;

import com.novidea.mof.entities.service.impl.AssetFullDetailedReportServiceImpl;
import com.novidea.mof.entities.service.impl.AssetMainGroupDetailedReportServiceImpl;
import com.novidea.mof.entities.service.impl.AssetYieldsAndBalancesReportServiceImpl;
import com.novidea.mof.entities.service.impl.DateCoverageServiceImpl;
import com.novidea.mof.entities.service.impl.FundServiceImpl;

/**
 * 
 * @author Galil.Zussman
 * @date Sep 4, 2014
 */
public class ServiceHolder implements Serializable {

	private static final long serialVersionUID = 1L;
	private static volatile ServiceHolder serviceHolder = null;

	public FundService fundService;
	public AssetFullDetailedReportService assetFullDetailedReportService;
	public AssetMainGroupDetailedReportService assetMainGroupDetailedReportService;
	public AssetYieldsAndBalancesReportService assetYieldsAndBalancesReportService;
	public DateCoverageService dateCoverageService;

	private ServiceHolder() {
		/*	// Spring Impl
			ApplicationContext ctx = ContextHelper.getApplicationContext();
			this.fundService = (FundService) ctx.getBean("fundServiceImpl");
			this.assetFullDetailedReportService = (AssetFullDetailedReportService) ctx
					.getBean("assetFullDetailedReportServiceImpl");
			this.assetMainGroupDetailedReportService = (AssetMainGroupDetailedReportService) ctx
					.getBean("assetMainGroupDetailedReportServiceImpl");
			this.dateCoverageService = (DateCoverageService) ctx
					.getBean("dateCoverageServiceImpl");
		*/
		// NONE Spring Impl
		this.fundService = new FundServiceImpl();
		this.assetFullDetailedReportService = new AssetFullDetailedReportServiceImpl();
		this.assetMainGroupDetailedReportService = new AssetMainGroupDetailedReportServiceImpl();
		this.assetYieldsAndBalancesReportService = new AssetYieldsAndBalancesReportServiceImpl();
		this.dateCoverageService = new DateCoverageServiceImpl();
	}

	public static ServiceHolder getInstance() {
		if (serviceHolder == null) {
			synchronized (ServiceHolder.class) {
				if (serviceHolder == null) {
					serviceHolder = new ServiceHolder();
				}
			}
		}
		return serviceHolder;
	}

}
