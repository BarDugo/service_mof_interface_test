package com.novidea.mof.entities.service;

import java.util.List;

import com.novidea.mof.entities.ReportType;
import com.novidea.mof.entities.model.DateCoverage;

/**
 * 
 * @author Galil.Zussman
 * @date Sep 4, 2014
 */
public interface DateCoverageService {

	List<DateCoverage> findMonthOfYearAndType(int year, int monthOfYear,
			ReportType reportType);

	void saveOrUpdateAll(List<DateCoverage> dateCoverages);

}
 