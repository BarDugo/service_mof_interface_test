package com.novidea.mof.entities;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**   
 * @author Galil.Zussman
 * @date   Dec 14, 2014
 */
public class ReportTypeCodeMapper {

	public static final Map<String, Set<Integer>> typeToCodesInUseMapper = new HashMap<String, Set<Integer>>();

	static Set<Integer> FULL_DETAILED_REPORT_ID_CODES;
	static Set<Integer> MAIN_GROUP_REPORT_ID_CODES;
	
	static {
		//////////////////////////// Gemel Codes////////////////////////////
		FULL_DETAILED_REPORT_ID_CODES = new HashSet<Integer>();
		MAIN_GROUP_REPORT_ID_CODES = new HashSet<Integer>();

		FULL_DETAILED_REPORT_ID_CODES.add(5513);
		FULL_DETAILED_REPORT_ID_CODES.add(5514);
		FULL_DETAILED_REPORT_ID_CODES.add(5515);
		FULL_DETAILED_REPORT_ID_CODES.add(5549);
		FULL_DETAILED_REPORT_ID_CODES.add(5550);
		FULL_DETAILED_REPORT_ID_CODES.add(5551);
		FULL_DETAILED_REPORT_ID_CODES.add(7156);
		FULL_DETAILED_REPORT_ID_CODES.add(7174);
		FULL_DETAILED_REPORT_ID_CODES.add(7186);
		typeToCodesInUseMapper.put(
				ReportType.GEMEL_ASSETS_FULL_DETAILED_REPORT.getValue(),
				new HashSet<Integer>(FULL_DETAILED_REPORT_ID_CODES));

		// חלוקת נכסים לקבוצות ראשיות
		MAIN_GROUP_REPORT_ID_CODES.add(4701); // אג"ח ממשלתיות סחירות 
		MAIN_GROUP_REPORT_ID_CODES.add(4703); // אג"ח קונצרני סחיר ותעודות סל אג"חיות
		MAIN_GROUP_REPORT_ID_CODES.add(4704); // אג"ח קונצרניות לא סחירות 
		MAIN_GROUP_REPORT_ID_CODES.add(4705); // חשיפה למניות - מניות, אופציות ותעודות סל מנייתיות
		MAIN_GROUP_REPORT_ID_CODES.add(4706); // פקדונות 
		MAIN_GROUP_REPORT_ID_CODES.add(4707); // הלוואות
		MAIN_GROUP_REPORT_ID_CODES.add(4708); // מזומנים ושווי מזומנים 
		MAIN_GROUP_REPORT_ID_CODES.add(4709); // קרנות נאמנות
		MAIN_GROUP_REPORT_ID_CODES.add(4710); // נכסים אחרים
		// חלוקת נכסים לפי סחירים/לא סחירים
		MAIN_GROUP_REPORT_ID_CODES.add(4721); // נכסים סחירים ונזילים
		MAIN_GROUP_REPORT_ID_CODES.add(4722); // נכסים לא סחירים
		// חלוקת נכסים לפי בארץ/חוץ לארץ       
		MAIN_GROUP_REPORT_ID_CODES.add(4731); // 
		MAIN_GROUP_REPORT_ID_CODES.add(4732); // 
		// חשיפות                              
		MAIN_GROUP_REPORT_ID_CODES.add(4751); // חשיפה למניות
		MAIN_GROUP_REPORT_ID_CODES.add(4752); // חשיפה לחו"ל
		MAIN_GROUP_REPORT_ID_CODES.add(4761); // חשיפה למט"ח
		typeToCodesInUseMapper.put(
				ReportType.GEMEL_ASSETS_MAIN_GROUP_DETAILED_REPORT.getValue(),
				new HashSet<Integer>(MAIN_GROUP_REPORT_ID_CODES));
		
		//////////////////////////// Pension Codes////////////////////////////
		FULL_DETAILED_REPORT_ID_CODES = new HashSet<Integer>();
		MAIN_GROUP_REPORT_ID_CODES = new HashSet<Integer>();
		
		FULL_DETAILED_REPORT_ID_CODES.add(5513);
		FULL_DETAILED_REPORT_ID_CODES.add(5514);
		FULL_DETAILED_REPORT_ID_CODES.add(5515);
		FULL_DETAILED_REPORT_ID_CODES.add(5549);
		FULL_DETAILED_REPORT_ID_CODES.add(5550);
		FULL_DETAILED_REPORT_ID_CODES.add(5551);
		FULL_DETAILED_REPORT_ID_CODES.add(7156);
		FULL_DETAILED_REPORT_ID_CODES.add(7174);
		FULL_DETAILED_REPORT_ID_CODES.add(7186);
		typeToCodesInUseMapper.put(
				ReportType.PENSION_ASSETS_FULL_DETAILED_REPORT.getValue(),
				new HashSet<Integer>(FULL_DETAILED_REPORT_ID_CODES));
		
		// חלוקת נכסים לקבוצות ראשיות
		MAIN_GROUP_REPORT_ID_CODES.add(4701); // אג"ח ממשלתיות סחירות 
		MAIN_GROUP_REPORT_ID_CODES.add(4703); // אג"ח קונצרני סחיר ותעודות סל אג"חיות
		MAIN_GROUP_REPORT_ID_CODES.add(4704); // אג"ח קונצרניות לא סחירות 
		MAIN_GROUP_REPORT_ID_CODES.add(4705); // חשיפה למניות - מניות, אופציות ותעודות סל מנייתיות
		MAIN_GROUP_REPORT_ID_CODES.add(4706); // פקדונות 
		MAIN_GROUP_REPORT_ID_CODES.add(4707); // הלוואות
		MAIN_GROUP_REPORT_ID_CODES.add(4708); // מזומנים ושווי מזומנים 
		MAIN_GROUP_REPORT_ID_CODES.add(4709); // קרנות נאמנות
		MAIN_GROUP_REPORT_ID_CODES.add(4710); // נכסים אחרים
		MAIN_GROUP_REPORT_ID_CODES.add(4712); // אג"ח מיועדות
		// חלוקת נכסים לפי סחירים/לא סחירים
		MAIN_GROUP_REPORT_ID_CODES.add(4721); // נכסים סחירים ונזילים
		MAIN_GROUP_REPORT_ID_CODES.add(4722); // נכסים לא סחירים
		// חלוקת נכסים לפי בארץ/חוץ לארץ       
		MAIN_GROUP_REPORT_ID_CODES.add(4731); // 
		MAIN_GROUP_REPORT_ID_CODES.add(4732); // 
		// חשיפות                              
		MAIN_GROUP_REPORT_ID_CODES.add(4751); // חשיפה למניות
		MAIN_GROUP_REPORT_ID_CODES.add(4752); // חשיפה לחו"ל
		MAIN_GROUP_REPORT_ID_CODES.add(4761); // חשיפה למט"ח
		typeToCodesInUseMapper.put(
				ReportType.PENSION_ASSETS_MAIN_GROUP_DETAILED_REPORT.getValue(),
				new HashSet<Integer>(MAIN_GROUP_REPORT_ID_CODES));

		//////////////////////////// Insurance Codes////////////////////////////
		FULL_DETAILED_REPORT_ID_CODES = new HashSet<Integer>();
		MAIN_GROUP_REPORT_ID_CODES = new HashSet<Integer>();

		FULL_DETAILED_REPORT_ID_CODES.add(7110);
		FULL_DETAILED_REPORT_ID_CODES.add(7111);
		FULL_DETAILED_REPORT_ID_CODES.add(7112);
		FULL_DETAILED_REPORT_ID_CODES.add(7131);
		FULL_DETAILED_REPORT_ID_CODES.add(7132);
		FULL_DETAILED_REPORT_ID_CODES.add(7133);
		FULL_DETAILED_REPORT_ID_CODES.add(7139);
		FULL_DETAILED_REPORT_ID_CODES.add(7148);

		typeToCodesInUseMapper.put(
				ReportType.INSURANCE_ASSETS_FULL_DETAILED_REPORT.getValue(),
				new HashSet<Integer>(FULL_DETAILED_REPORT_ID_CODES));

		// חלוקת נכסים לקבוצות ראשיות
		MAIN_GROUP_REPORT_ID_CODES.add(4701); // אג"ח ממשלתיות סחירות 
		MAIN_GROUP_REPORT_ID_CODES.add(4703); // קונצרני סחיר
		MAIN_GROUP_REPORT_ID_CODES.add(4704); // קונצרני לא סחיר 
		MAIN_GROUP_REPORT_ID_CODES.add(4705); // מניות
		MAIN_GROUP_REPORT_ID_CODES.add(4706); // פקדונות 
		MAIN_GROUP_REPORT_ID_CODES.add(4707); // הלוואות
		MAIN_GROUP_REPORT_ID_CODES.add(4708); // מזומנים 
		MAIN_GROUP_REPORT_ID_CODES.add(4709); // קרנות נאמנות
		MAIN_GROUP_REPORT_ID_CODES.add(4710); // נכסים אחרים
		// חלוקת נכסים לפי סחירים/לא סחירים
		MAIN_GROUP_REPORT_ID_CODES.add(4721); // נכסים סחירים 
		MAIN_GROUP_REPORT_ID_CODES.add(4722); // נכסים לא סחירים
		// חלוקת נכסים לפי בארץ/חוץ לארץ       
		MAIN_GROUP_REPORT_ID_CODES.add(4731); // נכסים בארץ
		MAIN_GROUP_REPORT_ID_CODES.add(4732); // נכסים בחול ובמט"ח
		// חשיפות                              
		MAIN_GROUP_REPORT_ID_CODES.add(4751); // חשיפה למניות
		MAIN_GROUP_REPORT_ID_CODES.add(4752); // חשיפה לחו"ל
		MAIN_GROUP_REPORT_ID_CODES.add(4753); // חשיפה למט"ח
		typeToCodesInUseMapper.put(
				ReportType.INSURANCE_ASSETS_MAIN_GROUP_DETAILED_REPORT
						.getValue(), new HashSet<Integer>(
						MAIN_GROUP_REPORT_ID_CODES));
		
		
		FULL_DETAILED_REPORT_ID_CODES = null;
		MAIN_GROUP_REPORT_ID_CODES = null;
	}

}
