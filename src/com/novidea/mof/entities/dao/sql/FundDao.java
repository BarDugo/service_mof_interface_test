package com.novidea.mof.entities.dao.sql;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Transaction;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import com.novidea.mof.Log;
import com.novidea.mof.entities.model.Fund;
import com.novidea.mof.hibernate.AbstractHibernateDAO;

/**
 * A data access object (DAO) providing persistence and search support for Fund
 * entities. Transaction control of the CRUD operations can directly support
 * Spring container-managed transactions or they can be augmented to handle
 * user-managed Spring transactions. Each of these methods provides additional
 * information for how to configure it for the desired type of transaction
 * control.
 * 
 * @see com.novidea.mof.entities.model.Fund
 * @author Galil.Zussman
 * 
 */
public class FundDao extends AbstractHibernateDAO<Fund> implements Serializable {

	private static final long serialVersionUID = 1L;
	private static FundDao fundDao = null;

	private FundDao() {
		super(Fund.class);
	}

	public static FundDao getInstance() {
		if (fundDao == null) {
			synchronized (FundDao.class) {
				if (fundDao == null) {
					fundDao = new FundDao();
				}
			}
		}
		return fundDao;

	}

	/* (non-Javadoc)
	 * @see com.novidea.mof.entities.dao.sql.FundService#mainFilteredFetch(java.lang.String, java.lang.String, java.util.Date, java.util.Date, int)
	 */
	@SuppressWarnings("unchecked")
	public List<Fund> mainFilteredFetch(List<Integer> ids, String reportType,
			String sug, Date endOfActivityDate, Date startDate, Date endDate,
			int maxRecords, boolean excludeEolFunds) {
		Object[] params = new String[] { reportType, sug, startDate.toString(),
				endDate.toString() };
		String msg = String
				.format("with params: reportType = '%s', sug = '%s',  startDate = %s, endDate = %s",
						params);
		Log.info("Start to mainFilteredFetch for getting the funds, " + msg);
		try {
			Transaction tx = getSession().beginTransaction();
			// .add(Restrictions.eq("ID", 101)) // FOR DEBUG
			Criteria criteria = getSession().createCriteria(Fund.class);
			criteria.add(
					Restrictions.between("HODESH_DIVUACH", startDate, endDate))
					.add(Restrictions.eq("reportType", reportType))
					.addOrder(Order.asc("ID"))
					.addOrder(Order.asc("HODESH_DIVUACH"));
			if (excludeEolFunds)
				criteria.add(Restrictions.isNull("TAARICH_SIUM_PEILUT"));
			if (sug != null && !sug.equals(""))
				criteria.add(Restrictions.eq("SUG", sug));
			if (ids != null && !ids.isEmpty())
				criteria.add(Restrictions.in("ID", ids));
			if (maxRecords > 0)
				criteria.setMaxResults(maxRecords);
			List<Fund> list = (List<Fund>) criteria.list();
			tx.commit();
			return list;
		} catch (RuntimeException re) {
			log.error("mainFilteredFetch failed", re);
			throw re;
		}
	}

	public List<Fund> fetchTsuaaYearlyResults(String reportType,
			Date startDate, int minusYears, int maxRecords) {
		Log.info("Start fetchTsuaaYearlyResults for getting the funds");
		try {
			DateTime date = new DateTime(startDate);
			DateTimeFormatter dtfOut = DateTimeFormat.forPattern("yyyy-MM-dd");
			String startDateStr = dtfOut.print(date.minusYears(minusYears)
					.plusMonths(1));
			String endDateStr = dtfOut.print(date);

			String query = "SELECT ID,SHEM,HODESH_DIVUACH,TSUA_HODSHIT,ReportType FROM fund where ReportType='"
					+ reportType
					+ "' and HODESH_DIVUACH between '"
					+ startDateStr
					+ "' and '"
					+ endDateStr
					+ "' ORDER BY ID ASC , HODESH_DIVUACH ASC ";
			if (maxRecords > 0)
				query += "limit " + maxRecords;
			System.out.println(query);
			List<Fund> list = this.executeNativeSqlCustomQuery(query);
			return list;
		} catch (RuntimeException re) {
			log.error("fetchTsuaaYearlyResults failed", re);
			throw re;
		}
	}

	public List<Fund> getFundsByNameAndType(String namePart, String type) {
		Log.info("Start retrieveByNameAndType method.");
		Log.debug("namepart=" + (namePart != null ? namePart : "null")
				+ ", type=" + (type != null ? type : "null") + ".");

		String query = "SELECT ID, SHEM FROM fund WHERE SHEM LIKE '%"
				+ namePart + "%' AND (SUG LIKE '%" + type + "%'"
				+ (type == null || type.equals("") ? " OR SUG IS NULL" : "")
				+ ");";

		List<Fund> funds = super.executeNativeSqlCustomQuery(query);
		if (funds.isEmpty())
			return null;
		return funds;
	}

}