package com.novidea.mof.entities.dao.sql;

import java.util.List;

import com.novidea.mof.entities.ReportType;
import com.novidea.mof.entities.model.Fund;

/**
 * @author Galil.Zussman
 * @date Aug 5, 2014
 */
public class FundDaoHelper {

	private static final String SUG_REPLACEMENT = "INSURANCE";

	private static final String ADJUSTED_RECORD_TYPE_PENSION = "קרן פנסיה";
	private static final String ADJUSTED_RECORD_TYPE_STUDY = "קרן השתלמות";
	private static final String ADJUSTED_RECORD_TYPE_GEMEL = "קופת גמל";
	private static final String ADJUSTED_RECORD_TYPE_INSURANCE = "פוליסת ביטוח מנהלים";
	private static final String MOF_FUND_GENERAL = "קרנות כלליות";
	private static final String MOF_FUND_NEW = "קרנות חדשות";
	private static final String MOF_FUND_STUDY = "קרנות השתלמות";
	private static final String MOF_FUND_COMPENSATE_PERSONAL = "תגמולים ואישית לפיצויים";
	private static final String MOF_FUND_COMPENSATE_CENTRAL = "מרכזית לפיצויים";
	private static final String MOF_FUND_INSURANCE = "ביטוח מנהלים";

	public FundDaoHelper() {
	}

	/**
	 * converts the received type (SUG) to the Agentwise known types
	 * 
	 * @param fundSUG
	 * @return converted string or the original if it doesn't fit any
	 */
	public static String getAdjustedType(String fundSUG) {
		if (fundSUG.equalsIgnoreCase(MOF_FUND_GENERAL)
				|| fundSUG.equalsIgnoreCase(MOF_FUND_NEW))
			return ADJUSTED_RECORD_TYPE_PENSION;
		else if (fundSUG.equalsIgnoreCase(MOF_FUND_STUDY))
			return ADJUSTED_RECORD_TYPE_STUDY;
		else if (fundSUG.equalsIgnoreCase(MOF_FUND_COMPENSATE_PERSONAL)
				|| fundSUG.equalsIgnoreCase(MOF_FUND_COMPENSATE_CENTRAL))
			return ADJUSTED_RECORD_TYPE_GEMEL;
		else if (fundSUG.equalsIgnoreCase(MOF_FUND_INSURANCE))
			return ADJUSTED_RECORD_TYPE_INSURANCE;
		else
			return fundSUG;
	}

	/**
	 *  change to @see(com.novidea.mof.entities.dao.sql.FundDaoHelper.SUG_REPLACEMENT) when reportType == ReportType.INSURANCE
	 * @param reportType - the fund type of the current batch 
	 * @param funds	- the batch
	 */
	public static void changeElementsByReportTypeFilter(ReportType reportType,
			List<?> funds) {
		if (reportType == ReportType.INSURANCE_DETAILED_REPORT
				&& !funds.isEmpty()) {
			for (Object obj : funds) {
				Fund fund = (Fund) obj;
				if (fund.getSUG() == null || fund.getSUG().equals(""))
					fund.setSUG(SUG_REPLACEMENT);
			}
		}
	}
}
