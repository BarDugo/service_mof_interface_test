package com.novidea.mof.entities.dao.sql;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;

import com.novidea.mof.entities.ReportType;
import com.novidea.mof.entities.model.DateCoverage;
import com.novidea.mof.hibernate.AbstractHibernateDAO;

/**
 * @author Galil.Zussman
 * @date May 23, 2014
 */
public class DateCoverageDao extends AbstractHibernateDAO<DateCoverage>
		implements java.io.Serializable {

	private static final long serialVersionUID = 1L;
	private static DateCoverageDao dateCoverageDao = null;

	private DateCoverageDao() {
		super(DateCoverage.class);
	}

	public static DateCoverageDao getInstance() {
		if (dateCoverageDao == null) {
			synchronized (DateCoverageDao.class) {
				if (dateCoverageDao == null) {
					dateCoverageDao = new DateCoverageDao();
				}
			}
		}
		return dateCoverageDao;

	}

	@SuppressWarnings("unchecked")
	public List<DateCoverage> findMonthOfYearAndType(int year, int month,
			ReportType reportType) {
		log.debug("findMonthOfYear start");
		try {
			Transaction tx = getSession().beginTransaction();

			Criteria criteria = getSession().createCriteria(DateCoverage.class)
					.add(Restrictions.eq("year", year))
					.add(Restrictions.eq("month", month));
			if (reportType != null)
				criteria.add(Restrictions.eq("type", reportType.getValue()));
			List<DateCoverage> list = (List<DateCoverage>) criteria.list();
			tx.commit();
			return list;
		} catch (RuntimeException re) {
			log.error("findYearMonth failed", re);
			throw re;
		}
	}
}
