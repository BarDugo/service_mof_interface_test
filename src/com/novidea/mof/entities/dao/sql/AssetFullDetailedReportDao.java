package com.novidea.mof.entities.dao.sql;

import java.util.Date;
import java.util.List;
import java.util.Set;

import org.hibernate.Criteria;
import org.hibernate.Transaction;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import com.novidea.mof.Log;
import com.novidea.mof.entities.ReportTypeCodeMapper;
import com.novidea.mof.entities.model.AssetFullDetailedReport;
import com.novidea.mof.exception.FailedToGetReportTypeException;
import com.novidea.mof.hibernate.AbstractHibernateDAO;

/**
 * 
 * @author Galil.Zussman
 * @date Aug 31, 2014
 */
public class AssetFullDetailedReportDao extends
		AbstractHibernateDAO<AssetFullDetailedReport> implements
		java.io.Serializable {

	private static final long serialVersionUID = 1L;

	private static AssetFullDetailedReportDao assetFullDetailedReportDao = null;

	private AssetFullDetailedReportDao() {
		super(AssetFullDetailedReport.class);
	}

	public static AssetFullDetailedReportDao getInstance() {
		if (assetFullDetailedReportDao == null) {
			synchronized (AssetFullDetailedReportDao.class) {
				if (assetFullDetailedReportDao == null) {
					assetFullDetailedReportDao = new AssetFullDetailedReportDao();
				}
			}
		}
		return assetFullDetailedReportDao;

	}

	@SuppressWarnings("unchecked")
	public List<AssetFullDetailedReport> findByHODESH_DIVUACH(Date date,
			String assetFullDetailedReportTYPE) {
		Log.debug("AssetFullDetailedReportDao - Start to findByHODESH_DIVUACH for getting the AssetFullDetailedReport for type "
				+ assetFullDetailedReportTYPE);
		try {

			//ID_NATUN to use
			Set<Integer> idsInUse = ReportTypeCodeMapper.typeToCodesInUseMapper
					.get(assetFullDetailedReportTYPE);
			if (idsInUse == null)
				throw new FailedToGetReportTypeException(
						"can't get reportType use-codes for type: "
								+ assetFullDetailedReportTYPE);

			Transaction tx = getSession().beginTransaction();
			Criteria criteria = getSession()
					.createCriteria(AssetFullDetailedReport.class)
					.add(Restrictions.eq("reportType",
							assetFullDetailedReportTYPE))
					.add(Restrictions.eq("HODESH_DIVUACH", date))
					.add(Restrictions.in("ID_NATUN", idsInUse))
					.addOrder(Order.asc("ID"))
					.addOrder(Order.asc("HODESH_DIVUACH"));
			List<AssetFullDetailedReport> list = (List<AssetFullDetailedReport>) criteria
					.list();
			tx.commit();
			return list;
		} catch (RuntimeException re) {
			log.error("findByHODESH_DIVUACH failed", re);
			throw re;
		} catch (FailedToGetReportTypeException e) {
			log.error("findByHODESH_DIVUACH failed", e);
		}
		return null;
	}

}