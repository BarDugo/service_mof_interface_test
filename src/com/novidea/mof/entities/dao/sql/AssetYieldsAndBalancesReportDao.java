package com.novidea.mof.entities.dao.sql;

import java.util.Date;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Transaction;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import com.novidea.mof.Log;
import com.novidea.mof.entities.model.AssetYieldsAndBalancesReport;
import com.novidea.mof.hibernate.AbstractHibernateDAO;

/**
 * 
 * @author Galil.Zussman
 * @date May 07, 2015
 */
public class AssetYieldsAndBalancesReportDao extends
		AbstractHibernateDAO<AssetYieldsAndBalancesReport> implements
		java.io.Serializable {

	private static final long serialVersionUID = 1L;
	private static AssetYieldsAndBalancesReportDao assetYieldsAndBalancesReportDao = null;

	private AssetYieldsAndBalancesReportDao() {
		super(AssetYieldsAndBalancesReport.class);
	}

	public static AssetYieldsAndBalancesReportDao getInstance() {
		if (assetYieldsAndBalancesReportDao == null) {
			synchronized (AssetYieldsAndBalancesReportDao.class) {
				if (assetYieldsAndBalancesReportDao == null) {
					assetYieldsAndBalancesReportDao = new AssetYieldsAndBalancesReportDao();
				}
			}
		}
		return assetYieldsAndBalancesReportDao;

	}

	@SuppressWarnings("unchecked")
	public List<AssetYieldsAndBalancesReport> findByHODESH_DIVUACH(Date date,
			String assetYieldsAndBalancesReportTYPE) {
		Log.info("AssetYieldsAndBalancesReportDao - Start to findByHODESH_DIVUACH for getting the AssetYieldsAndBalancesReport for type "
				+ assetYieldsAndBalancesReportTYPE);
		try {

			//ID_SUG_NECHES to use
			//Set<Integer> idsInUse = ReportTypeCodeMapper.typeToCodesInUseMapper
			//		.get(assetYieldsAndBalancesReportTYPE);
			//if (idsInUse == null)
			//	throw new FailedToGetReportTypeException(
			//			"can't get reportType use-codes for type: "
			//					+ assetYieldsAndBalancesReportTYPE);

			Transaction tx = getSession().beginTransaction();
			Criteria criteria = getSession()
					.createCriteria(AssetYieldsAndBalancesReport.class)
					.add(Restrictions.eq("reportType",
							assetYieldsAndBalancesReportTYPE))
					.add(Restrictions.eq("HODESH_DIVUACH", date))
					//.add(Restrictions.in("ID_SUG_NECHES", idsInUse))
					.addOrder(Order.asc("ID"));
			List<AssetYieldsAndBalancesReport> list = (List<AssetYieldsAndBalancesReport>) criteria
					.list();
			tx.commit();
			return list;
		} catch (RuntimeException re) {
			log.error("findByHODESH_DIVUACH failed", re);
			throw re;
		}
	}

}