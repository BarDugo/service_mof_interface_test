package com.novidea.mof.entities.dao.sql;

import java.util.Date;
import java.util.List;
import java.util.Set;

import org.hibernate.Criteria;
import org.hibernate.Transaction;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import com.novidea.mof.Log;
import com.novidea.mof.entities.ReportTypeCodeMapper;
import com.novidea.mof.entities.model.AssetMainGroupDetailedReport;
import com.novidea.mof.exception.FailedToGetReportTypeException;
import com.novidea.mof.hibernate.AbstractHibernateDAO;

/**
 * 
 * @author Galil.Zussman
 * @date Sep 01, 2014
 */
//@Repository // import org.springframework.stereotype.Repository;
public class AssetMainGroupDetailedReportDao extends
		AbstractHibernateDAO<AssetMainGroupDetailedReport> implements
		java.io.Serializable {

	private static final long serialVersionUID = 1L;
	private static AssetMainGroupDetailedReportDao assetMainGroupDetailedReportDao = null;

	private AssetMainGroupDetailedReportDao() {
		super(AssetMainGroupDetailedReport.class);
	}

	public static AssetMainGroupDetailedReportDao getInstance() {
		if (assetMainGroupDetailedReportDao == null) {
			synchronized (AssetMainGroupDetailedReportDao.class) {
				if (assetMainGroupDetailedReportDao == null) {
					assetMainGroupDetailedReportDao = new AssetMainGroupDetailedReportDao();
				}
			}
		}
		return assetMainGroupDetailedReportDao;

	}

	@SuppressWarnings("unchecked")
	public List<AssetMainGroupDetailedReport> findByHODESH_DIVUACH(Date date,
			String assetMainGroupDetailedReportTYPE) {
		Log.info("AssetMainGroupDetailedReportDao - Start to findByHODESH_DIVUACH for getting the AssetMainGroupDetailedReport for type "
				+ assetMainGroupDetailedReportTYPE);
		try {

			//ID_SUG_NECHES to use
			Set<Integer> idsInUse = ReportTypeCodeMapper.typeToCodesInUseMapper
					.get(assetMainGroupDetailedReportTYPE);
			if (idsInUse == null)
				throw new FailedToGetReportTypeException(
						"can't get reportType use-codes for type: "
								+ assetMainGroupDetailedReportTYPE);

			Transaction tx = getSession().beginTransaction();
			Criteria criteria = getSession()
					.createCriteria(AssetMainGroupDetailedReport.class)
					.add(Restrictions.eq("reportType",
							assetMainGroupDetailedReportTYPE))
					.add(Restrictions.eq("HODESH_DIVUACH", date))
					.add(Restrictions.in("ID_SUG_NECHES", idsInUse))
					.addOrder(Order.asc("ID"))
					.addOrder(Order.asc("HODESH_DIVUACH"));
			List<AssetMainGroupDetailedReport> list = (List<AssetMainGroupDetailedReport>) criteria
					.list();
			tx.commit();
			return list;
		} catch (RuntimeException re) {
			log.error("findByHODESH_DIVUACH failed", re);
			throw re;
		} catch (FailedToGetReportTypeException e) {
			log.error("findByHODESH_DIVUACH failed", e);
		}
		return null;
	}

}