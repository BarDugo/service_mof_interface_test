package com.novidea.mof.entities.dao.http;

import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.X509Certificate;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import org.apache.commons.io.IOUtils;
import org.joda.time.DateTime;

import com.novidea.mof.Log;
import com.novidea.mof.exception.FailedToGetReportTypeException;

public abstract class AbstractFundHttpDao {

	private static final String PARAM_MTKF = "miTkfDivuach";
	private static final String PARAM_ADTKF = "adTkfDivuach";
	private static final int MAX_RETRY = 5;

	// default: One month sampling
	// can publicly change the values
	public int fromYear = DateTime.now().getYear();
	public int fromMonth = DateTime.now().minusMonths(2).getMonthOfYear();
	public int toYear = DateTime.now().getYear();
	public int toMonth = DateTime.now().minusMonths(2).getMonthOfYear();

	private String url;
	private byte retryCounter = 0;
	private String unchangingParams = "";

	public AbstractFundHttpDao(String url, String unchangingParams) {
		this.url = url;
		this.unchangingParams = unchangingParams;
	}

	// output sample: miTkfDivuach=201303&adTkfDivuach=201403&polisot=0000&sug=1
	public String getQueryString(int fromYear, int fromMonth, int toYear,
			int toMonth) {

		StringBuilder sb = new StringBuilder();
		sb.append(PARAM_MTKF);
		sb.append("=");
		sb.append(fromYear);
		sb.append(formatNumberToTwoDigit(fromMonth));
		sb.append("&");
		sb.append(PARAM_ADTKF);
		sb.append("=");
		sb.append(toYear);
		sb.append(formatNumberToTwoDigit(toMonth));
		sb.append("&");
		sb.append(unchangingParams);
		Log.debug(sb.toString());
		return sb.toString();
	}

	private String formatNumberToTwoDigit(int num) {
		if (("" + num).length() < 2) {
			return "0" + num;
		} else
			return "" + num;
	}

	public String HttpRequest(StringBuilder sbLogger) throws IOException,
			FailedToGetReportTypeException {
        TrustManager[] trustAllCerts = new TrustManager[] { new X509TrustManager() {
            public java.security.cert.X509Certificate[] getAcceptedIssuers() { return null; }
            public void checkClientTrusted(X509Certificate[] certs, String authType) { }
            public void checkServerTrusted(X509Certificate[] certs, String authType) { }

        } };

        SSLContext sc;
		try {
			sc = SSLContext.getInstance("SSL");
			try {
				sc.init(null, trustAllCerts, new java.security.SecureRandom());
			} catch (KeyManagementException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
		} catch (NoSuchAlgorithmException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		Log.info("Start HttpRequest for " + this.getClass().getName());
		String result = null;
		InputStream inst = null;
		try {
			if (sbLogger == null) //shoulden't be the case
				sbLogger = new StringBuilder();
			String urlString = url;
			String qstring = getQueryString(fromYear, fromMonth, toYear,
					toMonth);
			if (qstring != null && qstring.length() > 0) {
				urlString += "?" + qstring;
			}
			URL urlConn = new URL(urlString);

			//Logging msg
			String msg = "Retry Num: " + (retryCounter + 1);
			String msg2 = "URL: " + urlConn.toExternalForm();
			sbLogger.append(msg + "\n" + msg2 + "\n");

			Log.info(msg);
			Log.info(msg2);
			HttpURLConnection conn = null;
			try {
				retryCounter++;

				//sbLogger.append("Retry Counter = " + retryCounter + "\n");
				Log.debug("Retry Counter = " + retryCounter);
				conn = (HttpURLConnection)urlConn.openConnection();
				HttpURLConnection.setFollowRedirects(false);
				Log.debug("Connection opened");
				conn.setConnectTimeout(180000);
		        conn.setReadTimeout(180000);
		        conn.setRequestMethod("GET");
		        conn.setRequestProperty("User-Agent", "Mozilla/5.0 (Windows; U; Windows NT 6.0; en-US; rv:1.9.1.2) Gecko/20090729 Firefox/3.5.2 (.NET CLR 3.5.30729)");
		        conn.connect();
				inst = conn.getInputStream();
			} catch (IOException e) {
				sbLogger.append(e + "\n");
				Log.error(e.getMessage());
				// retry MAX_RETRY times
				while (retryCounter < MAX_RETRY && retryCounter > 0) {
					this.HttpRequest(sbLogger);
				}
				if (retryCounter == MAX_RETRY) {
					String errMsg = "tryed to fetch data " + retryCounter
							+ " times from url : " + urlConn.toExternalForm()
							+ "  but couldn't succeed";
					sbLogger.append(errMsg + "\n");
					Log.error(errMsg);
					return null;
				} else if (retryCounter == 0)
					throw new FailedToGetReportTypeException();
			}

			StringWriter writer = new StringWriter();
			try {
				IOUtils.copy(inst, writer, "UTF-8");
				Log.debug("Reponse read into string buffer.");
			} catch (IOException e) {
				// handle specific exception that happens a lot with http
				// connections.
				// in this case the writer get data copy in to but
				// exception accrue
				if (e.getMessage().contains("Premature EOF")) {
					Log.warn("PrematureEOFException, nothing important, moving on..");
				} else {
					Log.error(e.getMessage(), e);
					throw e;
				}
			}
			result = writer.toString();

		} catch (MalformedURLException e) {
			Log.error("", e);
			throw e;
		} catch (IOException e) {
			Log.error("", e);
			throw e;
		} finally {
			retryCounter = 0;
			if (inst != null)
				inst.close();
		}
		Log.info("Exit HttpRequest for " + this.getClass().getName());
		Log.debug(result);
		return result;
	}

	public AbstractFundHttpDao() {
	}

	protected final String getUrl() {
		return url;
	}

	protected final void setUrl(String url) {
		this.url = url;
	}
}
