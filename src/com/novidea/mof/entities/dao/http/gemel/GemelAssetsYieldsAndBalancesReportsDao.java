package com.novidea.mof.entities.dao.http.gemel;

import com.novidea.mof.MofProperties;
import com.novidea.mof.entities.dao.http.AbstractFundHttpDao;

/**
 * 
 * @author Galil.Zussman
 * @date May 08, 2015
 */
public class GemelAssetsYieldsAndBalancesReportsDao extends AbstractFundHttpDao {
	private static final String url = MofProperties.get("GEMEL_SERVICE_URL");
	private static final String UNCHANGING_PARAMS = "kupot=0000&Dochot=1&sug=3";

	public GemelAssetsYieldsAndBalancesReportsDao() {
		super(url, UNCHANGING_PARAMS);
	}
}
