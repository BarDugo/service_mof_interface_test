package com.novidea.mof.entities.dao.http.gemel;

import com.novidea.mof.entities.dao.http.AbstractFundHttpDao;
import com.novidea.mof.MofProperties;



public class GemelFundHttpDao extends AbstractFundHttpDao {
	private static final String url = MofProperties.get("GEMEL_SERVICE_URL");
	private static final String UNCHANGING_PARAMS = "kupot=0000&Dochot=1&sug=1";

	public GemelFundHttpDao() {
		super(url, UNCHANGING_PARAMS);
	}
}
