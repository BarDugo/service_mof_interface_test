package com.novidea.mof.entities.dao.http.gemel;

import com.novidea.mof.MofProperties;
import com.novidea.mof.entities.dao.http.AbstractFundHttpDao;

/**
 * 
 * @author Galil.Zussman
 * @date Aug 28, 2014
 */
public class GemelAssetsFullDetailedDao extends AbstractFundHttpDao {
	private static final String url = MofProperties.get("GEMEL_SERVICE_URL");
	private static final String UNCHANGING_PARAMS = "kupot=0000&Dochot=1&sug=4";

	public GemelAssetsFullDetailedDao() {
		super(url, UNCHANGING_PARAMS);
	}
}
