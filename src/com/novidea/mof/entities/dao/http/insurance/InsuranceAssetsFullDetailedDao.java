package com.novidea.mof.entities.dao.http.insurance;

import com.novidea.mof.MofProperties;
import com.novidea.mof.entities.dao.http.AbstractFundHttpDao;

/**
 * 
 * @author Galil.Zussman
 * @date Aug 28, 2014
 */
public class InsuranceAssetsFullDetailedDao extends AbstractFundHttpDao {
	private static final String url = MofProperties.get("INSURANCE_SERVICE_URL");
	private static final String UNCHANGING_PARAMS = "polisot=0000&sug=3";

	public InsuranceAssetsFullDetailedDao() {
		super(url, UNCHANGING_PARAMS);
	}
}
