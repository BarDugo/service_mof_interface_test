package com.novidea.mof.entities.dao.http.insurance;

import com.novidea.mof.MofProperties;
import com.novidea.mof.entities.dao.http.AbstractFundHttpDao;

/**
 * 
 * @author Galil.Zussman
 * @date Dec 08, 2014
 */
public class InsuranceAssetsMainGroupDetailedDao  extends AbstractFundHttpDao {
	private static final String url = MofProperties.get("INSURANCE_SERVICE_URL");
	private static final String UNCHANGING_PARAMS = "polisot=0000&sug=4";

	public InsuranceAssetsMainGroupDetailedDao() {
		super(url, UNCHANGING_PARAMS);
	}
}
