package com.novidea.mof.entities.dao.http.insurance;

import com.novidea.mof.MofProperties;
import com.novidea.mof.entities.dao.http.AbstractFundHttpDao;

/**
 * 
 * @author Galil.Zussman
 * @date May 08, 2015
 */
public class InsuranceAssetYieldsAndBalancesDao extends AbstractFundHttpDao {
	private static final String url = MofProperties.get("INSURANCE_SERVICE_URL");
	private static final String UNCHANGING_PARAMS = "polisot=0000&sug=2";

	public InsuranceAssetYieldsAndBalancesDao() {
		super(url, UNCHANGING_PARAMS);
	}
}
