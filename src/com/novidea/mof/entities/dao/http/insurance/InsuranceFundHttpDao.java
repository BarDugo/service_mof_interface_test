package com.novidea.mof.entities.dao.http.insurance;

import com.novidea.mof.MofProperties;
import com.novidea.mof.entities.dao.http.AbstractFundHttpDao;



public class InsuranceFundHttpDao extends AbstractFundHttpDao {
	private static final String url = MofProperties.get("INSURANCE_SERVICE_URL");
	private static final String UNCHANGING_PARAMS = "polisot=0000&sug=1";

	public InsuranceFundHttpDao() {
		super(url,UNCHANGING_PARAMS);
	}
}
