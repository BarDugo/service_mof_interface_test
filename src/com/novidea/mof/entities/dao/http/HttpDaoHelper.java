package com.novidea.mof.entities.dao.http;

import java.io.IOException;

import org.joda.time.DateTime;

import com.novidea.mof.entities.dao.http.gemel.GemelAssetsFullDetailedDao;
import com.novidea.mof.entities.dao.http.gemel.GemelAssetsMainGroupDetailedDao;
import com.novidea.mof.entities.dao.http.gemel.GemelAssetsYieldsAndBalancesReportsDao;
import com.novidea.mof.entities.dao.http.gemel.GemelFundHttpDao;
import com.novidea.mof.entities.dao.http.insurance.InsuranceAssetYieldsAndBalancesDao;
import com.novidea.mof.entities.dao.http.insurance.InsuranceAssetsFullDetailedDao;
import com.novidea.mof.entities.dao.http.insurance.InsuranceAssetsMainGroupDetailedDao;
import com.novidea.mof.entities.dao.http.insurance.InsuranceFundHttpDao;
import com.novidea.mof.entities.dao.http.pension.PensionAssetYieldsAndBalancesDao;
import com.novidea.mof.entities.dao.http.pension.PensionAssetsFullDetailedDao;
import com.novidea.mof.entities.dao.http.pension.PensionAssetsMainGroupDetailedDao;
import com.novidea.mof.entities.dao.http.pension.PensionFundHttpDao;
import com.novidea.mof.exception.FailedToGetReportTypeException;

/**
 * 
 * @author Galil.Zussman
 * @date Sep 2, 2014
 */
public class HttpDaoHelper {

	public static String loadPensionAssetsMainGroupDetailedDao(
			DateTime startDate, DateTime endDate, StringBuilder sbLogger)
			throws IOException, FailedToGetReportTypeException {
		PensionAssetsMainGroupDetailedDao pensionAssetsMainGroupDetailedDao = new PensionAssetsMainGroupDetailedDao();
		if (startDate == null || endDate == null)
			return pensionAssetsMainGroupDetailedDao.HttpRequest(sbLogger);
		else {
			pensionAssetsMainGroupDetailedDao.fromMonth = startDate
					.getMonthOfYear();
			pensionAssetsMainGroupDetailedDao.fromYear = startDate.getYear();
			pensionAssetsMainGroupDetailedDao.toMonth = endDate
					.getMonthOfYear();
			pensionAssetsMainGroupDetailedDao.toYear = endDate.getYear();
			return pensionAssetsMainGroupDetailedDao.HttpRequest(sbLogger);
		}
	}

	public static String loadPensionAssetsFullDetailedDao(DateTime startDate,
			DateTime endDate, StringBuilder sbLogger) throws IOException,
			FailedToGetReportTypeException {
		PensionAssetsFullDetailedDao pensionAssetsFullDetailedDao = new PensionAssetsFullDetailedDao();
		if (startDate == null || endDate == null)
			return pensionAssetsFullDetailedDao.HttpRequest(sbLogger);
		else {
			pensionAssetsFullDetailedDao.fromMonth = startDate.getMonthOfYear();
			pensionAssetsFullDetailedDao.fromYear = startDate.getYear();
			pensionAssetsFullDetailedDao.toMonth = endDate.getMonthOfYear();
			pensionAssetsFullDetailedDao.toYear = endDate.getYear();
			return pensionAssetsFullDetailedDao.HttpRequest(sbLogger);
		}
	}

	public static String loadGemelAssetsMainGroupDetailedDao(
			DateTime startDate, DateTime endDate, StringBuilder sbLogger)
			throws IOException, FailedToGetReportTypeException {
		GemelAssetsMainGroupDetailedDao gemelAssetsMainGroupDetailedDao = new GemelAssetsMainGroupDetailedDao();
		if (startDate == null || endDate == null)
			return gemelAssetsMainGroupDetailedDao.HttpRequest(sbLogger);
		else {
			gemelAssetsMainGroupDetailedDao.fromMonth = startDate
					.getMonthOfYear();
			gemelAssetsMainGroupDetailedDao.fromYear = startDate.getYear();
			gemelAssetsMainGroupDetailedDao.toMonth = endDate.getMonthOfYear();
			gemelAssetsMainGroupDetailedDao.toYear = endDate.getYear();
			return gemelAssetsMainGroupDetailedDao.HttpRequest(sbLogger);
		}
	}

	public static String loadGemelAssetsFullDetailedDao(DateTime startDate,
			DateTime endDate, StringBuilder sbLogger) throws IOException,
			FailedToGetReportTypeException {
		GemelAssetsFullDetailedDao gemelAssetsFullDetailsDao = new GemelAssetsFullDetailedDao();
		if (startDate == null || endDate == null)
			return gemelAssetsFullDetailsDao.HttpRequest(sbLogger);
		else {
			gemelAssetsFullDetailsDao.fromMonth = startDate.getMonthOfYear();
			gemelAssetsFullDetailsDao.fromYear = startDate.getYear();
			gemelAssetsFullDetailsDao.toMonth = endDate.getMonthOfYear();
			gemelAssetsFullDetailsDao.toYear = endDate.getYear();
			return gemelAssetsFullDetailsDao.HttpRequest(sbLogger);
		}
	}

	public static String loadPesionFundsDao(DateTime startDate,
			DateTime endDate, StringBuilder sbLogger) throws IOException,
			FailedToGetReportTypeException {
		PensionFundHttpDao pensionFundHttpDao = new PensionFundHttpDao();
		if (startDate == null || endDate == null)
			return pensionFundHttpDao.HttpRequest(sbLogger);
		else {
			pensionFundHttpDao.fromMonth = startDate.getMonthOfYear();
			pensionFundHttpDao.fromYear = startDate.getYear();
			pensionFundHttpDao.toMonth = endDate.getMonthOfYear();
			pensionFundHttpDao.toYear = endDate.getYear();
			return pensionFundHttpDao.HttpRequest(sbLogger);
		}
	}

	public static String loadGemelFundsDao(DateTime startDate,
			DateTime endDate, StringBuilder sbLogger) throws IOException,
			FailedToGetReportTypeException {
		GemelFundHttpDao gemelFundHttpDao = new GemelFundHttpDao();
		if (startDate == null || endDate == null)
			return gemelFundHttpDao.HttpRequest(sbLogger);
		else {
			gemelFundHttpDao.fromMonth = startDate.getMonthOfYear();
			gemelFundHttpDao.fromYear = startDate.getYear();
			gemelFundHttpDao.toMonth = endDate.getMonthOfYear();
			gemelFundHttpDao.toYear = endDate.getYear();
			return gemelFundHttpDao.HttpRequest(sbLogger);
		}
	}

	public static String loadInsuranceFundsDao(DateTime startDate,
			DateTime endDate, StringBuilder sbLogger)
			throws FailedToGetReportTypeException, IOException {
		InsuranceFundHttpDao insuranceFundHttpDao = new InsuranceFundHttpDao();
		if (startDate == null || endDate == null)
			return insuranceFundHttpDao.HttpRequest(sbLogger);
		else {
			insuranceFundHttpDao.fromMonth = startDate.getMonthOfYear();
			insuranceFundHttpDao.fromYear = startDate.getYear();
			insuranceFundHttpDao.toMonth = endDate.getMonthOfYear();
			insuranceFundHttpDao.toYear = endDate.getYear();
			return insuranceFundHttpDao.HttpRequest(sbLogger);
		}
	}

	public static String loadInsuranceAssetsFullDetailedDao(DateTime startDate,
			DateTime endDate, StringBuilder sbLogger)
			throws FailedToGetReportTypeException, IOException {
		InsuranceAssetsFullDetailedDao insuranceAssetsFullDetailedDao = new InsuranceAssetsFullDetailedDao();
		if (startDate == null || endDate == null)
			return insuranceAssetsFullDetailedDao.HttpRequest(sbLogger);
		else {
			insuranceAssetsFullDetailedDao.fromMonth = startDate
					.getMonthOfYear();
			insuranceAssetsFullDetailedDao.fromYear = startDate.getYear();
			insuranceAssetsFullDetailedDao.toMonth = endDate.getMonthOfYear();
			insuranceAssetsFullDetailedDao.toYear = endDate.getYear();
			return insuranceAssetsFullDetailedDao.HttpRequest(sbLogger);
		}
	}

	public static String loadInsuranceAssetsMainGroupDetailedDao(
			DateTime startDate, DateTime endDate, StringBuilder sbLogger)
			throws FailedToGetReportTypeException, IOException {
		InsuranceAssetsMainGroupDetailedDao insuranceAssetsMainGroupDetailedDao = new InsuranceAssetsMainGroupDetailedDao();
		if (startDate == null || endDate == null)
			return insuranceAssetsMainGroupDetailedDao.HttpRequest(sbLogger);
		else {
			insuranceAssetsMainGroupDetailedDao.fromMonth = startDate
					.getMonthOfYear();
			insuranceAssetsMainGroupDetailedDao.fromYear = startDate.getYear();
			insuranceAssetsMainGroupDetailedDao.toMonth = endDate
					.getMonthOfYear();
			insuranceAssetsMainGroupDetailedDao.toYear = endDate.getYear();
			return insuranceAssetsMainGroupDetailedDao.HttpRequest(sbLogger);
		}
	}

	public static String loadGemelAssetsYieldsAndBalancesReportsDao(
			DateTime startDate, DateTime endDate, StringBuilder sbLogger)
			throws IOException, FailedToGetReportTypeException {
		GemelAssetsYieldsAndBalancesReportsDao gemelAssetsYieldsAndBalancesReportsDao = new GemelAssetsYieldsAndBalancesReportsDao();
		if (startDate == null || endDate == null)
			return gemelAssetsYieldsAndBalancesReportsDao.HttpRequest(sbLogger);
		else {
			gemelAssetsYieldsAndBalancesReportsDao.fromMonth = startDate
					.getMonthOfYear();
			gemelAssetsYieldsAndBalancesReportsDao.fromYear = startDate
					.getYear();
			gemelAssetsYieldsAndBalancesReportsDao.toMonth = endDate
					.getMonthOfYear();
			gemelAssetsYieldsAndBalancesReportsDao.toYear = endDate.getYear();
			return gemelAssetsYieldsAndBalancesReportsDao.HttpRequest(sbLogger);
		}
	}

	public static String loadPensionAssetYieldsAndBalancesDao(
			DateTime startDate, DateTime endDate, StringBuilder sbLogger)
			throws IOException, FailedToGetReportTypeException {
		PensionAssetYieldsAndBalancesDao pensionAssetYieldsAndBalancesHttpDao = new PensionAssetYieldsAndBalancesDao();
		if (startDate == null || endDate == null)
			return pensionAssetYieldsAndBalancesHttpDao.HttpRequest(sbLogger);
		else {
			pensionAssetYieldsAndBalancesHttpDao.fromMonth = startDate
					.getMonthOfYear();
			pensionAssetYieldsAndBalancesHttpDao.fromYear = startDate.getYear();
			pensionAssetYieldsAndBalancesHttpDao.toMonth = endDate
					.getMonthOfYear();
			pensionAssetYieldsAndBalancesHttpDao.toYear = endDate.getYear();
			return pensionAssetYieldsAndBalancesHttpDao.HttpRequest(sbLogger);
		}
	}

	public static String loadInsuranceAssetYieldsAndBalancesDao(
			DateTime startDate, DateTime endDate, StringBuilder sbLogger)
			throws IOException, FailedToGetReportTypeException {
		InsuranceAssetYieldsAndBalancesDao insuranceAssetYieldsAndBalancesDao = new InsuranceAssetYieldsAndBalancesDao();
		if (startDate == null || endDate == null)
			return insuranceAssetYieldsAndBalancesDao.HttpRequest(sbLogger);
		else {
			insuranceAssetYieldsAndBalancesDao.fromMonth = startDate
					.getMonthOfYear();
			insuranceAssetYieldsAndBalancesDao.fromYear = startDate.getYear();
			insuranceAssetYieldsAndBalancesDao.toMonth = endDate
					.getMonthOfYear();
			insuranceAssetYieldsAndBalancesDao.toYear = endDate.getYear();
			return insuranceAssetYieldsAndBalancesDao.HttpRequest(sbLogger);
		}
	}

}
