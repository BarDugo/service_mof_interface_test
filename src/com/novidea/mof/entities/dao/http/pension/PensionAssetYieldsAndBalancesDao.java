package com.novidea.mof.entities.dao.http.pension;

import com.novidea.mof.MofProperties;
import com.novidea.mof.entities.dao.http.AbstractFundHttpDao;

/**
 * 
 * @author Galil.Zussman
 * @date May 08, 2015
 */
public class PensionAssetYieldsAndBalancesDao extends AbstractFundHttpDao {
	private static final String url = MofProperties.get("PENSION_SERVICE_URL");
	private static final String UNCHANGING_PARAMS = "kupot=0000&Dochot=1001&sug=3";

	public PensionAssetYieldsAndBalancesDao() {
		super(url, UNCHANGING_PARAMS);
	}
}
