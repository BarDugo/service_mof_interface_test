package com.novidea.mof.entities.dao.http.pension;

import com.novidea.mof.MofProperties;
import com.novidea.mof.entities.dao.http.AbstractFundHttpDao;

/**
 * 
 * @author Galil.Zussman
 * @date Aug 28, 2014
 */
public class PensionAssetsFullDetailedDao extends AbstractFundHttpDao {
	private static final String url = MofProperties.get("PENSION_SERVICE_URL");
	private static final String UNCHANGING_PARAMS = "kupot=0000&Dochot=1001&sug=4";

	public PensionAssetsFullDetailedDao() {
		super(url, UNCHANGING_PARAMS);
	}
}
