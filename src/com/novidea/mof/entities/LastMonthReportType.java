package com.novidea.mof.entities;

public enum LastMonthReportType {

	GEMEL_LAST_MONTH_RESULT("GemelLastMonthResult"), 
	PENSION_LAST_MONTH_RESULT("PensionLastMonthResult"),
	INSURANCE_LAST_MONTH_RESULT("InsuranceLastMonthResult");

	private final String value;

	LastMonthReportType(String value) {
		this.value = value;
	}

	public String val() {
		return value;
	}
}
