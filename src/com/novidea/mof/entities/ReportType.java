package com.novidea.mof.entities;
/**
 *  All of the supported report types that have data source at mof.gov.il 
 * 
 * @author Galil.Zussman
 *
 */
public enum ReportType {
	
	//DetailedReport Type - the detailed report is the master type when calling from WS
	INSURANCE_DETAILED_REPORT("InsuranceDetailedReport"), 
	GEMEL_DETAILED_REPORT("GemelDetailedReport"), 
	PENSION_DETAILED_REPORT("PensionDetailedReport"),

	//AssetsFullDetailedReport Type
	GEMEL_ASSETS_FULL_DETAILED_REPORT("GemelAssetsFullDetailedReport"), 
	PENSION_ASSETS_FULL_DETAILED_REPORT("PensionAssetsFullDetailedReport"), 
 	INSURANCE_ASSETS_FULL_DETAILED_REPORT("InsuranceAssetsFullDetailedReport"),

 	//AssetsMainGroupDetailedReport Type
	GEMEL_ASSETS_MAIN_GROUP_DETAILED_REPORT("GemelAssetsMainGroupDetailedReport"), 
	PENSION_ASSETS_MAIN_GROUP_DETAILED_REPORT("PensionAssetsMainGroupDetailedReport"),
	INSURANCE_ASSETS_MAIN_GROUP_DETAILED_REPORT("InsuranceAssetsMainGroupDetailedReport"),
	
	//AssetYieldsAndBalancesReport Type
	PENSION_ASSETS_YIELDS_AND_BALANCES_REPORT("PensionAssetsYieldsAndBalancesReport"), 
	GEMEL_ASSETS_YIELDS_AND_BALANCES_REPORT("GemelAssetsYieldsAndBalancesReport"),
	INSURANCE_ASSETS_YIELDS_AND_BALANCES_REPORT("InsuranceAssetsYieldsAndBalancesReport");
	

	private final String value;

	ReportType(String value) {
		this.value = value;
	}

	public String getValue() {
		return value;
	}
}
