package com.novidea.mof.entities.model;

import java.util.HashMap;
import java.util.Map;

public class DataApiNamesMap {
	private static final Map<String, Map<String, String>> apiNameByAttributeNameMap = new HashMap<String, Map<String, String>>();
	static {
		Map<String, String> auxmap = new HashMap<String, String>();
		auxmap.put("AD_TKUFAT_DIVUACH", "AD_TKUFAT_DIVUACH");
		auxmap.put("ALPHA_SHNATIT", "ALPHA_SHNATI");
		auxmap.put("MI_TKUFAT_DIVUACH", "MI_TKUFAT_DIVUACH");
		auxmap.put("SHARP_RIBIT_HASRAT_SIKUN", "SHARP_RIBIT_HASRAT_SIKUN");
		auxmap.put("STIAT_TEKEN_36_HODASHIM", "STIAT_TEKEN_36_HODASHIM");
		auxmap.put("STIAT_TEKEN_60_HODASHIM", "STIAT_TEKEN_60_HODASHIM");
		auxmap.put("TAARICH_HAFAKAT_HADOCH", "TAARICH_HAFAKAT_HADOCH");
		auxmap.put("TSUA_MEMUZAAT_36_HODASHIM", "TSUA_MEMUZAAT_36_HODASHIM");
		auxmap.put("TSUA_MEMUZAAT_60_HODASHIM", "TSUA_MEMUZAAT_60_HODASHIM");
		auxmap.put("TSUA_MEMUZAAT_LETKUFA", "TSUA_MEMUZAAT_LETKUFA");
		auxmap.put("TSUA_MITZTABERET_36_HODASHIM", "TSUA_MITZTABERET_36_HODASHIM");
		auxmap.put("TSUA_MITZTABERET_60_HODASHIM", "TSUA_MITZTABERET_60_HODASHIM");
		auxmap.put("TSUA_MITZTABERET_LETKUFA", "TSUA_MITZ_LE_TKUFA");
		auxmap.put("TSUA_SHNATIT_MEMUZAAT_3_SHANIM", "TSUA_SHNATIT_MEMUZAAT_3_SHANIM");
		auxmap.put("TSUA_SHNATIT_MEMUZAAT_5_SHANIM", "TSUA_SHNATIT_MEMUZAAT_5_SHANIM");
		auxmap.put("YAHAS_NEZILUT", "YAHAS_NEZILUT");
		auxmap.put("ID", "ID");
		auxmap.put("SHM_KUPA", "SHEM");
		auxmap.put("SHARP_ANAF", "SHARP_ANAF");
		auxmap.put("HAFKADOT_LLO_HAAVAROT", "HAFKADOT_LLO_HAAVAROT");
		auxmap.put("MATZAV_DIVUACH", "MATZAV_DIVUACH");
		auxmap.put("MSHICHOT_LLO_HAAVAROT", "MSHICHOT_LLO_HAAVAROT");
		auxmap.put("SUG_TAAGID_SHOLET", "SUG_TAAGID_SHOLET");
		auxmap.put("SHM_HEVRA_MENAHELET", "SHM_HEVRA_MENAHELET");
		auxmap.put("SHM_TAAGID_SHOLET", "SHM_TAAGID_SHOLET");
		auxmap.put("SHIUR_DMEI_NIHUL_AHARON", "SHIUR_DMEI_NIHUL_AHARON");
		auxmap.put("TZVIRA_NETO", "TZVIRA_NETO");
		auxmap.put("YITRAT_NCHASIM_LSOF_TKUFA", "YITRAT_NCHASIM_LSOF_TKUFA");
		auxmap.put("SUG_KUPA", "SUG");
		auxmap.put("HAAVAROT_BEIN_HAKUPOT", "HAAVAROT");
		auxmap.put("SHARP_KOL_HAKUPOT", "SHARP_ALL");
		auxmap.put("HITMAHUT_MISHNIT", "HITMAHUT_MISHNIT");
		auxmap.put("HITMAHUT_RASHIT", "HITMAHUT_RASHIT");
		auxmap.put("MISPAR_KUPA_AV", "MISPAR_KUPA_AV");
		auxmap.put("UCHLUSIYAT_YAAD", "UCHLUSIYAT_YAAD");
		auxmap.put("TAARICH_HAKAMA", "TAARICH_HAKAMA");
		auxmap.put("TSUA_NOMINALIT_BRUTO_HODSHIT", "TSUA_HODSHIT");
		auxmap.put("NUM_HEVRA", "NUM_HEVRA");
		auxmap.put("TKUFAT_DIVUACH", "TKUFAT_DIVUACH");
		auxmap.put("TAARICH_SIUM_PEILUT", "TAARICH_SIUM_PEILUT");
		auxmap.put("TSUA_MITZT_MI_THILAT_SHANA", "TSUA_MITZT_MI_THILAT_SHANA");
		auxmap.put("SHIUR_D_NIHUL_AHARON_HAFKADOT", "SHIUR_D_NIHUL_HAFKADOT");
		apiNameByAttributeNameMap.put("GemelDetailedReport",
				new HashMap<String, String>(auxmap));

		auxmap.put("AD_TKUFAT_DIVUACH", "AD_TKUFAT_DIVUACH");
		auxmap.put("ALPHA_SHNATI", "ALPHA_SHNATI");
		auxmap.put("MI_TKUFAT_DIVUACH", "MI_TKUFAT_DIVUACH");
		auxmap.put("SHARP_RIBIT_HASRAT_SIKUN", "SHARP_RIBIT_HASRAT_SIKUN");
		auxmap.put("STIAT_TEKEN_36_HODASHIM", "STIAT_TEKEN_36_HODASHIM");
		auxmap.put("STIAT_TEKEN_60_HODASHIM", "STIAT_TEKEN_60_HODASHIM");
		auxmap.put("TAARICH_HAFAKAT_HADOCH", "TAARICH_HAFAKAT_HADOCH");
		auxmap.put("TSUA_MEMUZAAT_36_HODASHIM", "TSUA_MEMUZAAT_36_HODASHIM");
		auxmap.put("TSUA_MEMUZAAT_60_HODASHIM", "TSUA_MEMUZAAT_60_HODASHIM");
		auxmap.put("TSUA_MEMUZAAT_LETKUFA", "TSUA_MEMUZAAT_LETKUFA");
		auxmap.put("TSUA_MITZTABERET_36_HODASHIM", "TSUA_MITZTABERET_36_HODASHIM");
		auxmap.put("TSUA_MITZTABERET_60_HODASHIM", "TSUA_MITZTABERET_60_HODASHIM");
		auxmap.put("TSUA_MITZTABERET_LETKUFA", "TSUA_MITZ_LE_TKUFA");
		auxmap.put("TSUA_SHNATIT_MEMUZAAT_3_SHANIM", "TSUA_SHNATIT_MEMUZAAT_3_SHANIM");
		auxmap.put("TSUA_SHNATIT_MEMUZAAT_5_SHANIM", "TSUA_SHNATIT_MEMUZAAT_5_SHANIM");
		auxmap.put("YAHAS_NEZILUT", "YAHAS_NEZILUT");
		auxmap.put("ID", "ID");
		auxmap.put("SHM_KRN", "SHEM");
		auxmap.put("SHARP_TSUA_HETZYONIT_ANAF", "SHARP_ANAF");
		auxmap.put("HAFKADOT_LLO_HAAVAROT", "HAFKADOT_LLO_HAAVAROT");
		auxmap.put("MATZAV_DIVUACH", "MATZAV_DIVUACH");
		auxmap.put("MSHICHOT_LLO_HAAVAROT", "MSHICHOT_LLO_HAAVAROT");
		auxmap.put("SUG_TAAGID_SHOLET", "SUG_TAAGID_SHOLET");
		auxmap.put("SHM_HEVRA_MENAHELET", "SHM_HEVRA_MENAHELET");
		auxmap.put("SHM_TAAGID_SHOLET", "SHM_TAAGID_SHOLET");
		auxmap.put("SHIUR_DMEI_NIHUL_AHARON", "SHIUR_DMEI_NIHUL_AHARON");
		auxmap.put("TZVIRA_NETO", "TZVIRA_NETO");
		auxmap.put("YITRAT_NCHASIM_LSOF_TKUFA", "YITRAT_NCHASIM_LSOF_TKUFA");
		auxmap.put("SUG_KRN", "SUG");
		auxmap.put("HAAVAROT_BEIN_HAKRANOT", "HAAVAROT");
		auxmap.put("SHARP_TSUA_HETZYONIT_ALL", "SHARP_ALL");
		auxmap.put("BETA_AGACH_KONTZERNIOT_TZMUDOT","BETA_AGACH_KONTZERNIOT_TZMUDOT");
		auxmap.put("BETA_AGACH_MEMSHALTIOT_TZMUDOT","BETA_AGACH_MEMSHALTIOT_TZMUDOT");
		auxmap.put("BETA_AGACH_MEM_LO_TZMUDOT", "BETA_AGACH_MEM_LO_TZMUDOT");
		auxmap.put("BETA_HUTZ_LAARETZ", "BETA_HUTZ_LAARETZ");
		auxmap.put("BETA_TA100", "BETA_TA100");
		auxmap.put("R_SQUARED", "R_SQUARED");
		auxmap.put("ODEF_GIRAON_ACTUARI_AHARON", "ODEF_GIRAON_ACTUARI_AHARON");
		auxmap.put("SHIUR_D_NIHUL_AHARON_HAFKADOT", "SHIUR_D_NIHUL_HAFKADOT");
		auxmap.put("SHIUR_D_NIHUL_AHARON_NCHASIM", "SHIUR_D_NIHUL_NECHASIM");
		auxmap.put("TSUA_MEMUZAT_HASHKAOT_HOFSHIOT", "TSUA_MEMUZAT_HASHKAOT_HOFSHIOT");
		auxmap.put("TSUA_NOMINALIT_BRUTO_HODSHIT", "TSUA_HODSHIT");
		auxmap.put("NUM_HEVRA", "NUM_HEVRA");
		auxmap.put("TKUFAT_DIVUACH", "TKUFAT_DIVUACH");
		auxmap.put("TAARICH_SIUM_PEILUT", "TAARICH_SIUM_PEILUT");
		auxmap.put("TSUA_MITZT_MI_THILAT_SHANA", "TSUA_MITZT_MI_THILAT_SHANA");
		auxmap.put("TSUA_NOM_HASHKAOT_HOFSHIOT", "TSUA_NOM_HASHKAOT_HOFSHIOT");
		auxmap.put("YR_ODEF_GIRAON_ACTUARI_AHARON",	"YR_ODEF_GIRAON_ACTUARI_AHARON");
		auxmap.put("ODEF_GIRAON_ACTUARI_LETKUFA", "ODEF_GIRAON_ACTUARI_LETKUFA");
		apiNameByAttributeNameMap.put("PensionDetailedReport",
				new HashMap<String, String>(auxmap));

		auxmap.put("AD_TKUFAT_DIVUACH", "AD_TKUFAT_DIVUACH");
		auxmap.put("ALPHA_SHNATI", "ALPHA_SHNATI");
		auxmap.put("MI_TKUFAT_DIVUACH", "MI_TKUFAT_DIVUACH");
		auxmap.put("SHARP_RIBIT_HASRAT_SIKUN", "SHARP_RIBIT_HASRAT_SIKUN");
		auxmap.put("STIAT_TEKEN_36_HODASHIM", "STIAT_TEKEN_36_HODASHIM");
		auxmap.put("STIAT_TEKEN_60_HODASHIM", "STIAT_TEKEN_60_HODASHIM");
		auxmap.put("TAARICH_HAFAKAT_HADOCH", "TAARICH_HAFAKAT_HADOCH");
		auxmap.put("TSUA_MEMUZAAT_36_HODASHIM", "TSUA_MEMUZAAT_36_HODASHIM");
		auxmap.put("TSUA_MEMUZAAT_60_HODASHIM", "TSUA_MEMUZAAT_60_HODASHIM");
		auxmap.put("TSUA_MEMUZAAT_LETKUFA", "TSUA_MEMUZAAT_LETKUFA");
		auxmap.put("TSUA_MITZTABERET_36_HODASHIM", "TSUA_MITZTABERET_36_HODASHIM");
		auxmap.put("TSUA_MITZTABERET_60_HODASHIM", "TSUA_MITZTABERET_60_HODASHIM");
		auxmap.put("TSUA_MITZ_LE_TKUFA", "TSUA_MITZ_LE_TKUFA");
		auxmap.put("TSUA_SHNATIT_MEMUZAAT_3_SHANIM", "TSUA_SHNATIT_MEMUZAAT_3_SHANIM");
		auxmap.put("TSUA_SHNATIT_MEMUZAAT_5_SHANIM", "TSUA_SHNATIT_MEMUZAAT_5_SHANIM");
		auxmap.put("YAHAS_NEZILUT", "YAHAS_NEZILUT");
		auxmap.put("ID_GUF", "ID");
		auxmap.put("SHEM_GUF", "SHEM");
		auxmap.put("SHARP_TSUA_HEZYONIT_ANAF", "SHARP_ANAF");
		auxmap.put("BETA_AGACH_KONTZERNIOT_TZMUDOT", "BETA_AGACH_KONTZERNIOT_TZMUDOT");
		auxmap.put("BETA_AGACH_MEMSHALTIOT_TZMUDOT", "BETA_AGACH_MEMSHALTIOT_TZMUDOT");
		auxmap.put("BETA_AGACH_MEM_LO_TZMUDOT", "BETA_AGACH_MEM_LO_TZMUDOT");
		auxmap.put("BETA_HUTZ_LAARETZ", "BETA_HUTZ_LAARETZ");
		auxmap.put("BETA_TA100", "BETA_TA100");
		auxmap.put("SHIUR_D_NIHUL_HAFKADOT", "SHIUR_D_NIHUL_HAFKADOT");
		auxmap.put("SHIUR_D_NIHUL_NECHASIM", "SHIUR_D_NIHUL_NECHASIM");
		auxmap.put("ID_HEVRA", "ID_HEVRA");
		auxmap.put("R_SQUARED", "R_SQUARED");
		auxmap.put("TKUFAT_HAKAMA", "TKUFAT_HAKAMA");
		auxmap.put("YIT_NCHASIM_BFOAL", "YIT_NCHASIM_BFOAL");
		auxmap.put("TSUA_HODSHIT", "TSUA_HODSHIT");
		auxmap.put("NUM_HEVRA", "NUM_HEVRA");
		auxmap.put("TKUFAT_DIVUACH", "TKUFAT_DIVUACH");
		auxmap.put("TAARICH_SIUM_PEILUT", "TAARICH_SIUM_PEILUT");
		auxmap.put("TSUA_MITZ_MI_THILAT_HASHANA", "TSUA_MITZT_MI_THILAT_SHANA");
		apiNameByAttributeNameMap.put("InsuranceDetailedReport",
				new HashMap<String, String>(auxmap));

		////////////////////////// AssetsFullDetailedReport
		auxmap.put("ID_KUPA", "ID");
		auxmap.put("TKF_DIVUACH", "TKF_DIVUACH");
		auxmap.put("ID_NATUN", "ID_NATUN");
		auxmap.put("SHM_NATUN", "SHM_NATUN");
		auxmap.put("ERECH_NATUN", "ERECH_NATUN");
		auxmap.put("ACHUZ", "ACHUZ");
		apiNameByAttributeNameMap.put("GemelAssetsFullDetailedReport",
				new HashMap<String, String>(auxmap));

		auxmap.put("ID_KRN", "ID");
		auxmap.put("SHM_KRN", "SHEM");
		auxmap.put("TKF_DIVUACH", "TKF_DIVUACH");
		auxmap.put("ID_NATUN", "ID_NATUN");
		auxmap.put("SHM_NATUN", "SHM_NATUN");
		auxmap.put("ERECH_NATUN", "ERECH_NATUN");
		auxmap.put("ACHUZ", "ACHUZ");
		apiNameByAttributeNameMap.put("PensionAssetsFullDetailedReport",
				new HashMap<String, String>(auxmap));

		auxmap.put("ID_GUF", "ID");
		auxmap.put("SHEM_GUF", "SHEM");
		auxmap.put("ID_HEVRA", "ID_HEVRA");
		auxmap.put("SHEM_HEVRA", "SHEM_HEVRA");
		auxmap.put("TKF_DIVUACH", "TKF_DIVUACH");
		auxmap.put("ID_SUG_NECHES", "ID_NATUN");
		auxmap.put("SHM_NECHES", "SHM_NATUN");
		auxmap.put("ERECH_NECHES", "ERECH_NATUN");
		apiNameByAttributeNameMap.put("InsuranceAssetsFullDetailedReport",
				new HashMap<String, String>(auxmap));

		////////////////////////// AssetMainGroupDetailedReport
		auxmap.put("ID_KUPA", "ID");
		auxmap.put("TKF_DIVUACH", "TKF_DIVUACH");
		auxmap.put("KVUTZAT_NECHASIM", "KVUTZAT_NECHASIM");
		auxmap.put("ID_SUG_NECHES_KVUTZA", "ID_SUG_NECHES_KVUTZA");
		auxmap.put("ID_SUG_NECHES", "ID_SUG_NECHES");
		auxmap.put("SHM_SUG_NECHES", "SHM_SUG_NECHES");
		auxmap.put("SCHUM_SUG_NECHES", "SCHUM_SUG_NECHES");
		auxmap.put("ACHUZ_SUG_NECHES", "ACHUZ_SUG_NECHES");
		apiNameByAttributeNameMap.put("GemelAssetsMainGroupDetailedReport",
				new HashMap<String, String>(auxmap));

		auxmap.put("ID_MASLUL_RISHUY", "ID");
		auxmap.put("SHM_KRN", "SHEM");
		auxmap.put("TKF_DIVUACH", "TKF_DIVUACH");
		auxmap.put("KVUTZAT_NECHASIM", "KVUTZAT_NECHASIM");
		auxmap.put("ID_SUG_NECHES_KVUTZA", "ID_SUG_NECHES_KVUTZA");
		auxmap.put("ID_SUG_NECHES", "ID_SUG_NECHES");
		auxmap.put("SHM_SUG_NECHES", "SHM_SUG_NECHES");
		auxmap.put("SCHUM_SUG_NECHES", "SCHUM_SUG_NECHES");
		auxmap.put("ACHUZ_SUG_NECHES", "ACHUZ_SUG_NECHES");
		apiNameByAttributeNameMap.put("PensionAssetsMainGroupDetailedReport",
				new HashMap<String, String>(auxmap));
		
		auxmap.put("ID_GUF", "ID");
		auxmap.put("SHEM_GUF", "SHEM");
		auxmap.put("ID_HEVRA", "ID_HEVRA");
		auxmap.put("SHEM_HEVRA", "SHEM_HEVRA");
		auxmap.put("TKF_DIVUACH", "TKF_DIVUACH");
		auxmap.put("SHM_SUG_NECHES_KVUTZA", "KVUTZAT_NECHASIM");
		auxmap.put("ID_SUG_NECHES", "ID_SUG_NECHES");
		auxmap.put("SHM_SUG_NECHES", "SHM_SUG_NECHES");
		auxmap.put("SCHUM_SUG_NECHES", "SCHUM_SUG_NECHES");
		auxmap.put("ACHUZ_SUG_NECHES", "ACHUZ_SUG_NECHES");
		apiNameByAttributeNameMap.put("InsuranceAssetsMainGroupDetailedReport",
				new HashMap<String, String>(auxmap));
		
		////////////////////////// AssetsYieldsAndBalancesReport
		auxmap.put("ID_KRN", "ID");
		auxmap.put("SHM_KRN", "SHEM");
		auxmap.put("TKF_DIVUACH", "TKF_DIVUACH");
		auxmap.put("TSUA_NOMINALI_BFOAL", "TSUA_NOMINALI_BFOAL");
		auxmap.put("TSUA_DEMOGRAPHIT", "TSUA_DEMOGRAPHIT");
		auxmap.put("TSUA_NOMINALI_IM_DEMOGRAPHIT", "TSUA_NOMINALI_IM_DEMOGRAPHIT");
		auxmap.put("YIT_NCHASIM_BFOAL", "YIT_NCHASIM_BFOAL");
		auxmap.put("TSUA_NOM_HASHKAOT_HOFSHIOT", "TSUA_NOM_HASHKAOT_HOFSHIOT");
		auxmap.put("ODEF_GIRAON_ACTUARI_RIVONI", "ODEF_GIRAON_ACTUARI_RIVONI");
		apiNameByAttributeNameMap.put("PensionAssetsYieldsAndBalancesReport",
				new HashMap<String, String>(auxmap));
	
		auxmap.put("ID_KUPA", "ID");
		auxmap.put("TKF_DIVUACH", "TKF_DIVUACH");
		auxmap.put("TSUA_NOMINALI_BFOAL", "TSUA_NOMINALI_BFOAL");
		auxmap.put("YIT_NCHASIM_BFOAL", "YIT_NCHASIM_BFOAL");
		apiNameByAttributeNameMap.put("GemelAssetsYieldsAndBalancesReport",
				new HashMap<String, String>(auxmap));
		
		auxmap.put("ID_GUF", "ID");
		auxmap.put("SHEM_GUF", "SHEM");
		auxmap.put("TKF_DIVUACH", "TKF_DIVUACH");
		auxmap.put("TSUA_HODSHIT", "TSUA_NOMINALI_BFOAL");
		auxmap.put("YIT_NCHASIM_BFOAL", "YIT_NCHASIM_BFOAL");
		apiNameByAttributeNameMap.put("InsuranceAssetsYieldsAndBalancesReport",
				new HashMap<String, String>(auxmap));
		
	}

	public static Map<String, Map<String, String>> getApiNameByAttributeNameMap() {
		return apiNameByAttributeNameMap;
	}

	public static String getApiNameByAttribute(String reportType, String attr) {
		return apiNameByAttributeNameMap.get(reportType).get(attr);
	}
}
