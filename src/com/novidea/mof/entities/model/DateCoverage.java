package com.novidea.mof.entities.model;

import static javax.persistence.GenerationType.IDENTITY;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * DateCoverage entity.
 * 
 * @author Galil.Zussman
 * @date 23.05.2014
 * 
 */
@Entity
@Table(name = "date_coverage")
/*, catalog = "mof"*/
public class DateCoverage implements Serializable {

	private static final long serialVersionUID = 1L;

	private Integer id;
	private Integer year;
	private Integer month;
	private String type;
	private Date modifiedDate;

	public DateCoverage() {
	}

	public DateCoverage(Integer year, Integer month, String type,
			Date modifiedDate) {
		super();
		this.year = year;
		this.month = month;
		this.type = type;
		this.modifiedDate = modifiedDate;
	}

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "ID", unique = true, nullable = false)
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name = "YEAR", nullable = false)
	public Integer getYear() {
		return year;
	}

	public void setYear(Integer year) {
		this.year = year;
	}

	@Column(name = "MONTH", nullable = false)
	public Integer getMonth() {
		return month;
	}

	public void setMonth(Integer month) {
		this.month = month;
	}

	@Column(name = "TYPE", nullable = false)
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	@Column(name = "ModifiedDate")
	@Temporal(TemporalType.TIMESTAMP)
	public Date getModifiedDate() {
		return modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	@Override
	public String toString() {
		Class<DateCoverage> generalClass = DateCoverage.class;
		Field[] fields = generalClass.getDeclaredFields();
		String fieldName;
		Object fieldValue = null;
		StringBuffer sb = new StringBuffer();
		for (int i = 1/* skip serialVersionUID */; i < fields.length; i++) {
			fieldName = fields[i].getName();
			try {
				fieldValue = "" + fields[i].get(this);
			} catch (IllegalArgumentException e) {
				fieldValue = "N\\A";
			} catch (IllegalAccessException e) {
				fieldValue = "N\\A";
			}
			sb.append(fieldName + "=" + fieldValue + ", ");
		}
		return sb.toString();
	}
}
