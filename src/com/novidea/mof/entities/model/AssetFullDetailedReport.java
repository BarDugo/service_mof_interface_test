package com.novidea.mof.entities.model;

import java.lang.reflect.Field;
import java.util.Date; 

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "asset_full_detailed_report")/*, catalog = "mof"*/
public class AssetFullDetailedReport implements java.io.Serializable {

	private static final long serialVersionUID = 1L;
	// Fields

	private Long Key;
	private Integer ID;
	private String ReportType;
	private Date CreatedDate;
	private Date UpdatedDate;
	private Date HODESH_DIVUACH;
	private Integer ID_HEVRA;
	private String SHEM;
	private String SHM_SUG_NECHES_KVUTZA;
	private String SHEM_HEVRA;
	private String TKF_DIVUACH;
	private Integer ID_NATUN;
	private String SHM_NATUN;
	private Double ERECH_NATUN;
	private Double ACHUZ;

	private Fund fund;

	// Constructors

	/** default constructor */
	public AssetFullDetailedReport() {
	}

	/** minimal constructor */
	public AssetFullDetailedReport(Integer ID, String reportType,
			Date createdDate, Date updatedDate, Date HODESH_DIVUACH) {
		this.ID = ID;
		this.ReportType = reportType;
		this.CreatedDate = createdDate;
		this.UpdatedDate = updatedDate;
		this.HODESH_DIVUACH = HODESH_DIVUACH;
	}

	// Property accessors
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "Key", unique = true, nullable = false)
	public Long getKey() {
		return Key;
	}

	public void setKey(Long key) {
		Key = key;
	}

	@Column(name = "ID", nullable = false)
	public Integer getID() {
		return ID;
	}

	@Column(name = "ID", nullable = false)
	public void setID(Integer ID) {
		this.ID = ID;
	}

	@Column(name = "ReportType", nullable = false, length = 60)
	public String getReportType() {
		return ReportType;
	}

	public void setReportType(String reportType) {
		ReportType = reportType;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "CreatedDate", nullable = false, length = 19)
	public Date getCreatedDate() {
		return this.CreatedDate;
	}

	public void setCreatedDate(Date CreatedDate) {
		this.CreatedDate = CreatedDate;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "UpdatedDate", nullable = false, length = 19)
	public Date getUpdatedDate() {
		return UpdatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		UpdatedDate = updatedDate;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "HODESH_DIVUACH", length = 10)
	public Date getHODESH_DIVUACH() {
		return HODESH_DIVUACH;
	}

	public void setHODESH_DIVUACH(Date hODESH_DIVUACH) {
		HODESH_DIVUACH = hODESH_DIVUACH;
	}

	@Column(name = "ID_HEVRA")
	public Integer getID_HEVRA() {
		return ID_HEVRA;
	}

	public void setID_HEVRA(Integer iD_HEVRA) {
		ID_HEVRA = iD_HEVRA;
	}

	@Column(name = "SHEM")
	public String getSHEM() {
		return SHEM;
	}

	public void setSHEM(String sHEM) {
		SHEM = sHEM;
	}

	@Column(name = "SHM_SUG_NECHES_KVUTZA")
	public String getSHM_SUG_NECHES_KVUTZA() {
		return SHM_SUG_NECHES_KVUTZA;
	}

	public void setSHM_SUG_NECHES_KVUTZA(String sHM_SUG_NECHES_KVUTZA) {
		SHM_SUG_NECHES_KVUTZA = sHM_SUG_NECHES_KVUTZA;
	}

	@Column(name = "SHEM_HEVRA")
	public String getSHEM_HEVRA() {
		return SHEM_HEVRA;
	}

	public void setSHEM_HEVRA(String sHEM_HEVRA) {
		SHEM_HEVRA = sHEM_HEVRA;
	}

	@Column(name = "TKF_DIVUACH")
	public String getTKF_DIVUACH() {
		return TKF_DIVUACH;
	}

	public void setTKF_DIVUACH(String tKF_DIVUACH) {
		TKF_DIVUACH = tKF_DIVUACH;
	}

	@Column(name = "ID_NATUN")
	public Integer getID_NATUN() {
		return ID_NATUN;
	}

	public void setID_NATUN(Integer iD_NATUN) {
		ID_NATUN = iD_NATUN;
	}

	@Column(name = "SHM_NATUN")
	public String getSHM_NATUN() {
		return SHM_NATUN;
	}

	public void setSHM_NATUN(String sHM_NATUN) {
		SHM_NATUN = sHM_NATUN;
	}

	@Column(name = "ERECH_NATUN", precision = 16, scale = 6)
	public Double getERECH_NATUN() {
		return ERECH_NATUN;
	}

	public void setERECH_NATUN(Double eRECH_NATUN) {
		ERECH_NATUN = eRECH_NATUN;
	}

	@Column(name = "ACHUZ", precision = 16, scale = 6)
	public Double getACHUZ() {
		return ACHUZ;
	}

	public void setACHUZ(Double aCHUZ) {
		ACHUZ = aCHUZ;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "FUND_KEY", nullable = true)
	public Fund getFund() {
		return fund;
	}

	public void setFund(Fund fund) {
		this.fund = fund;
	}

	@Override
	public String toString() {
		Class<AssetFullDetailedReport> fundClass = AssetFullDetailedReport.class;
		Field[] fundFields = fundClass.getDeclaredFields();
		String fieldName;
		Object fieldValue = null;
		StringBuffer sb = new StringBuffer();
		for (int i = 1 /* skip serialVersionUID */; i < fundFields.length; i++) {
			fieldName = fundFields[i].getName();
			if (fieldName.equalsIgnoreCase("updateddate")
					|| fieldName.equalsIgnoreCase("fund"))
				continue;
			try {
				fieldValue = "" + fundFields[i].get(this);
			} catch (IllegalArgumentException e) {
				fieldValue = "N\\A";
			} catch (IllegalAccessException e) {
				fieldValue = "N\\A";
			}
			sb.append(fieldName + "=" + fieldValue + ", ");
		}
		return sb.toString();
	}
}