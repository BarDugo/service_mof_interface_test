package com.novidea.mof.entities.model;

import static javax.persistence.GenerationType.IDENTITY;

import java.lang.reflect.Field;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "asset_main_group_detailed_report")
/*, catalog = "mof"*/
public class AssetMainGroupDetailedReport implements java.io.Serializable {

	private static final long serialVersionUID = 1L;
	// Fields

	private Long Key;
	private Integer ID;
	private String ReportType;
	private Date CreatedDate;
	private Date UpdatedDate;
	private Date HODESH_DIVUACH;
	private String SHEM;
	private Integer ID_HEVRA;
	private String TKF_DIVUACH;
	private String KVUTZAT_NECHASIM;
	private Integer ID_SUG_NECHES_KVUTZA;
	private Integer ID_SUG_NECHES;
	private String SHM_SUG_NECHES;
	private String SHEM_HEVRA;
	private Double SCHUM_SUG_NECHES;
	private Double ACHUZ_SUG_NECHES;

	private Fund fund;

	// Constructors

	/** default constructor */
	public AssetMainGroupDetailedReport() {
	}

	/** minimal constructor */
	public AssetMainGroupDetailedReport(Integer ID, String reportType,
			Date createdDate, Date updatedDate, Date HODESH_DIVUACH) {
		this.ID = ID;
		this.ReportType = reportType;
		this.CreatedDate = createdDate;
		this.UpdatedDate = updatedDate;
		this.HODESH_DIVUACH = HODESH_DIVUACH;
	}

	// Property accessors
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "Key", unique = true, nullable = false)
	public Long getKey() {
		return Key;
	}

	public void setKey(Long key) {
		Key = key;
	}

	@Column(name = "ID", nullable = false)
	public Integer getID() {
		return ID;
	}

	public void setID(Integer ID) {
		this.ID = ID;
	}

	@Column(name = "ReportType", nullable = false, length = 20)
	public String getReportType() {
		return ReportType;
	}

	public void setReportType(String reportType) {
		ReportType = reportType;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "CreatedDate", nullable = false, length = 19)
	public Date getCreatedDate() {
		return this.CreatedDate;
	}

	public void setCreatedDate(Date CreatedDate) {
		this.CreatedDate = CreatedDate;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "UpdatedDate", nullable = false, length = 19)
	public Date getUpdatedDate() {
		return UpdatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		UpdatedDate = updatedDate;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "HODESH_DIVUACH", length = 10)
	public Date getHODESH_DIVUACH() {
		return HODESH_DIVUACH;
	}

	public void setHODESH_DIVUACH(Date hODESH_DIVUACH) {
		HODESH_DIVUACH = hODESH_DIVUACH;
	}

	@Column(name = "SHEM")
	public String getSHEM() {
		return SHEM;
	}

	public void setSHEM(String sHEM) {
		SHEM = sHEM;
	}

	@Column(name = "ID_HEVRA")
	public Integer getID_HEVRA() {
		return ID_HEVRA;
	}

	public void setID_HEVRA(Integer iD_HEVRA) {
		ID_HEVRA = iD_HEVRA;
	}

	@Column(name = "TKF_DIVUACH")
	public String getTKF_DIVUACH() {
		return TKF_DIVUACH;
	}

	public void setTKF_DIVUACH(String tKF_DIVUACH) {
		TKF_DIVUACH = tKF_DIVUACH;
	}

	@Column(name = "KVUTZAT_NECHASIM")
	public String getKVUTZAT_NECHASIM() {
		return KVUTZAT_NECHASIM;
	}

	public void setKVUTZAT_NECHASIM(String kVUTZAT_NECHASIM) {
		KVUTZAT_NECHASIM = kVUTZAT_NECHASIM;
	}

	@Column(name = "ID_SUG_NECHES_KVUTZA")
	public Integer getID_SUG_NECHES_KVUTZA() {
		return ID_SUG_NECHES_KVUTZA;
	}

	public void setID_SUG_NECHES_KVUTZA(Integer iD_SUG_NECHES_KVUTZA) {
		ID_SUG_NECHES_KVUTZA = iD_SUG_NECHES_KVUTZA;
	}

	@Column(name = "ID_SUG_NECHES")
	public Integer getID_SUG_NECHES() {
		return ID_SUG_NECHES;
	}

	public void setID_SUG_NECHES(Integer iD_SUG_NECHES) {
		ID_SUG_NECHES = iD_SUG_NECHES;
	}

	@Column(name = "SHM_SUG_NECHES")
	public String getSHM_SUG_NECHES() {
		return SHM_SUG_NECHES;
	}

	public void setSHM_SUG_NECHES(String sHM_SUG_NECHES) {
		SHM_SUG_NECHES = sHM_SUG_NECHES;
	}

	@Column(name = "SHEM_HEVRA")
	public String getSHEM_HEVRA() {
		return SHEM_HEVRA;
	}

	public void setSHEM_HEVRA(String sHEM_HEVRA) {
		SHEM_HEVRA = sHEM_HEVRA;
	}

	@Column(name = "SCHUM_SUG_NECHES")
	public Double getSCHUM_SUG_NECHES() {
		return SCHUM_SUG_NECHES;
	}

	public void setSCHUM_SUG_NECHES(Double sCHUM_SUG_NECHES) {
		SCHUM_SUG_NECHES = sCHUM_SUG_NECHES;
	}

	@Column(name = "ACHUZ_SUG_NECHES")
	public Double getACHUZ_SUG_NECHES() {
		return ACHUZ_SUG_NECHES;
	}

	public void setACHUZ_SUG_NECHES(Double aCHUZ_SUG_NECHES) {
		ACHUZ_SUG_NECHES = aCHUZ_SUG_NECHES;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "FUND_KEY", nullable = true)
	public Fund getFund() {
		return fund;
	}

	public void setFund(Fund fund) {
		this.fund = fund;
	}

	@Override
	public String toString() {
		Class<AssetMainGroupDetailedReport> fundClass = AssetMainGroupDetailedReport.class;
		Field[] fundFields = fundClass.getDeclaredFields();
		String fieldName;
		Object fieldValue = null;
		StringBuffer sb = new StringBuffer();
		for (int i = 1 /* skip serialVersionUID */; i < fundFields.length; i++) {
			fieldName = fundFields[i].getName();
			if (fieldName.equalsIgnoreCase("updateddate")
					|| fieldName.equalsIgnoreCase("fund"))
				continue;
			try {
				fieldValue = "" + fundFields[i].get(this);
			} catch (IllegalArgumentException e) {
				fieldValue = "N\\A";
			} catch (IllegalAccessException e) {
				fieldValue = "N\\A";
			}
			sb.append(fieldName + "=" + fieldValue + ", ");
		}
		return sb.toString();
	}
}