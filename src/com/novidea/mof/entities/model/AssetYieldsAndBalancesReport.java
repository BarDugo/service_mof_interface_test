package com.novidea.mof.entities.model;

import static javax.persistence.GenerationType.IDENTITY;

import java.lang.reflect.Field;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "asset_yields_and_balances_report")
public class AssetYieldsAndBalancesReport implements java.io.Serializable {

	private static final long serialVersionUID = 1L;

	// Fields
	private Long Key;
	private Integer ID;
	private String ReportType;
	private Date CreatedDate;
	private Date UpdatedDate;
	private Date HODESH_DIVUACH;
	private String SHEM;
	private String TKF_DIVUACH;
	private Double TSUA_NOMINALI_BFOAL;
	private Double TSUA_DEMOGRAPHIT;
	private Double TSUA_NOMINALI_IM_DEMOGRAPHIT;
	private Double YIT_NCHASIM_BFOAL;
	private Double TSUA_NOM_HASHKAOT_HOFSHIOT;
	private Double ODEF_GIRAON_ACTUARI_RIVONI;

	private Fund fund;

	// Constructors

	/** default constructor */
	public AssetYieldsAndBalancesReport() {
	}

	/** minimal constructor */
	public AssetYieldsAndBalancesReport(Integer ID, String reportType,
			Date createdDate, Date updatedDate, Date HODESH_DIVUACH) {
		this.ID = ID;
		this.ReportType = reportType;
		this.CreatedDate = createdDate;
		this.UpdatedDate = updatedDate;
		this.HODESH_DIVUACH = HODESH_DIVUACH;
	}

	// Property accessors
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "Key", unique = true, nullable = false)
	public Long getKey() {
		return Key;
	}

	public void setKey(Long key) {
		Key = key;
	}

	@Column(name = "ID", nullable = false)
	public Integer getID() {
		return ID;
	}

	public void setID(Integer ID) {
		this.ID = ID;
	}

	@Column(name = "ReportType", nullable = false, length = 20)
	public String getReportType() {
		return ReportType;
	}

	public void setReportType(String reportType) {
		ReportType = reportType;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "CreatedDate", nullable = false, length = 19)
	public Date getCreatedDate() {
		return this.CreatedDate;
	}

	public void setCreatedDate(Date CreatedDate) {
		this.CreatedDate = CreatedDate;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "UpdatedDate", nullable = false, length = 19)
	public Date getUpdatedDate() {
		return UpdatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		UpdatedDate = updatedDate;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "HODESH_DIVUACH", length = 10)
	public Date getHODESH_DIVUACH() {
		return HODESH_DIVUACH;
	}

	public void setHODESH_DIVUACH(Date hODESH_DIVUACH) {
		HODESH_DIVUACH = hODESH_DIVUACH;
	}

	@Column(name = "SHEM")
	public String getSHEM() {
		return SHEM;
	}

	public void setSHEM(String sHEM) {
		SHEM = sHEM;
	}

	@Column(name = "TKF_DIVUACH")
	public String getTKF_DIVUACH() {
		return TKF_DIVUACH;
	}

	public void setTKF_DIVUACH(String tKF_DIVUACH) {
		TKF_DIVUACH = tKF_DIVUACH;
	}

	@Column(name = "TSUA_NOMINALI_BFOAL")
	public Double getTSUA_NOMINALI_BFOAL() {
		return TSUA_NOMINALI_BFOAL;
	}

	public void setTSUA_NOMINALI_BFOAL(Double tSUA_NOMINALI_BFOAL) {
		TSUA_NOMINALI_BFOAL = tSUA_NOMINALI_BFOAL;
	}

	@Column(name = "TSUA_DEMOGRAPHIT")
	public Double getTSUA_DEMOGRAPHIT() {
		return TSUA_DEMOGRAPHIT;
	}

	public void setTSUA_DEMOGRAPHIT(Double tSUA_DEMOGRAPHIT) {
		TSUA_DEMOGRAPHIT = tSUA_DEMOGRAPHIT;
	}

	@Column(name = "TSUA_NOMINALI_IM_DEMOGRAPHIT")
	public Double getTSUA_NOMINALI_IM_DEMOGRAPHIT() {
		return TSUA_NOMINALI_IM_DEMOGRAPHIT;
	}

	public void setTSUA_NOMINALI_IM_DEMOGRAPHIT(
			Double tSUA_NOMINALI_IM_DEMOGRAPHIT) {
		TSUA_NOMINALI_IM_DEMOGRAPHIT = tSUA_NOMINALI_IM_DEMOGRAPHIT;
	}

	@Column(name = "YIT_NCHASIM_BFOAL")
	public Double getYIT_NCHASIM_BFOAL() {
		return YIT_NCHASIM_BFOAL;
	}

	public void setYIT_NCHASIM_BFOAL(Double yIT_NCHASIM_BFOAL) {
		YIT_NCHASIM_BFOAL = yIT_NCHASIM_BFOAL;
	}

	@Column(name = "TSUA_NOM_HASHKAOT_HOFSHIOT")
	public Double getTSUA_NOM_HASHKAOT_HOFSHIOT() {
		return TSUA_NOM_HASHKAOT_HOFSHIOT;
	}

	public void setTSUA_NOM_HASHKAOT_HOFSHIOT(Double tSUA_NOM_HASHKAOT_HOFSHIOT) {
		TSUA_NOM_HASHKAOT_HOFSHIOT = tSUA_NOM_HASHKAOT_HOFSHIOT;
	}

	@Column(name = "ODEF_GIRAON_ACTUARI_RIVONI")
	public Double getODEF_GIRAON_ACTUARI_RIVONI() {
		return ODEF_GIRAON_ACTUARI_RIVONI;
	}

	public void setODEF_GIRAON_ACTUARI_RIVONI(Double oDEF_GIRAON_ACTUARI_RIVONI) {
		ODEF_GIRAON_ACTUARI_RIVONI = oDEF_GIRAON_ACTUARI_RIVONI;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "FUND_KEY", nullable = true)
	public Fund getFund() {
		return fund;
	}

	public void setFund(Fund fund) {
		this.fund = fund;
	}

	@Override
	public String toString() {
		Class<AssetYieldsAndBalancesReport> fundClass = AssetYieldsAndBalancesReport.class;
		Field[] fundFields = fundClass.getDeclaredFields();
		String fieldName;
		Object fieldValue = null;
		StringBuffer sb = new StringBuffer();
		for (int i = 1 /* skip serialVersionUID */; i < fundFields.length; i++) {
			fieldName = fundFields[i].getName();
			if (fieldName.equalsIgnoreCase("updateddate")
					|| fieldName.equalsIgnoreCase("fund"))
				continue;
			try {
				fieldValue = "" + fundFields[i].get(this);
			} catch (IllegalArgumentException e) {
				fieldValue = "N\\A";
			} catch (IllegalAccessException e) {
				fieldValue = "N\\A";
			}
			sb.append(fieldName + "=" + fieldValue + ", ");
		}
		return sb.toString();
	}
}