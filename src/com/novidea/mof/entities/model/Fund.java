package com.novidea.mof.entities.model;

import static javax.persistence.GenerationType.IDENTITY;

import java.lang.reflect.Field;
import java.sql.Timestamp;
import java.util.Date;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * Fund entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "fund")
/*, catalog = "mof"*/
public class Fund implements java.io.Serializable {

	private static final long serialVersionUID = 1L;
	// Fields

	private Long Key;
	private Integer ID;
	private String ReportType;
	private Date CreatedDate;
	private Date UpdatedDate;
	private Date HODESH_DIVUACH;
	private String SHEM;
	private Double TSUA_HODSHIT;
	private String AD_TKUFAT_DIVUACH;
	private Double ALPHA_SHNATI;
	private String MI_TKUFAT_DIVUACH;
	private Double SHARP_RIBIT_HASRAT_SIKUN;
	private Double STIAT_TEKEN_36_HODASHIM;
	private Double STIAT_TEKEN_60_HODASHIM;
	private Date TAARICH_HAFAKAT_HADOCH;
	private Double TSUA_MEMUZAAT_36_HODASHIM;
	private Double TSUA_MEMUZAAT_60_HODASHIM;
	private Double TSUA_MEMUZAAT_LETKUFA;
	private Double TSUA_MITZTABERET_36_HODASHIM;
	private Double TSUA_MITZTABERET_60_HODASHIM;
	private Double TSUA_SHNATIT_MEMUZAAT_3_SHANIM;
	private Double TSUA_SHNATIT_MEMUZAAT_5_SHANIM;
	private Double YAHAS_NEZILUT;
	private Double TSUA_MITZ_LE_TKUFA;
	private Double SHARP_ANAF;
	private Double HAFKADOT_LLO_HAAVAROT;
	private String MATZAV_DIVUACH;
	private Double MSHICHOT_LLO_HAAVAROT;
	private String SUG_TAAGID_SHOLET;
	private String SHM_HEVRA_MENAHELET;
	private String SHM_TAAGID_SHOLET;
	private Double SHIUR_DMEI_NIHUL_AHARON;
	private Double TZVIRA_NETO;
	private Double YITRAT_NCHASIM_LSOF_TKUFA;
	private String SUG;
	private Double HAAVAROT;
	private Double SHARP_ALL;
	private Double BETA_AGACH_KONTZERNIOT_TZMUDOT;
	private Double BETA_AGACH_MEMSHALTIOT_TZMUDOT;
	private Double BETA_AGACH_MEM_LO_TZMUDOT;
	private Double BETA_HUTZ_LAARETZ;
	private Double BETA_TA100;
	private Double SHIUR_D_NIHUL_HAFKADOT;
	private Double SHIUR_D_NIHUL_NECHASIM;
	private String HITMAHUT_MISHNIT;
	private String HITMAHUT_RASHIT;
	private String MISPAR_KUPA_AV;
	private String UCHLUSIYAT_YAAD;
	private Date TAARICH_HAKAMA;
	private Double TSUA_MEMUZAT_HASHKAOT_HOFSHIOT;
	private Double ODEF_GIRAON_ACTUARI_AHARON;
	private String ID_HEVRA;
	private Double R_SQUARED;
	private String TKUFAT_HAKAMA;
	private Double YIT_NCHASIM_BFOAL;
	private String NUM_HEVRA;
	private Double TSUA_MITZT_MI_THILAT_SHANA;
	private String TKUFAT_DIVUACH;
	private Date TAARICH_SIUM_PEILUT;
	private Double TSUA_NOM_HASHKAOT_HOFSHIOT;
	private String YR_ODEF_GIRAON_ACTUARI_AHARON;
	private String ODEF_GIRAON_ACTUARI_LETKUFA;

	private Set<AssetFullDetailedReport> assetFullDetailedReports;
	private Set<AssetMainGroupDetailedReport> assetMainGroupDetailedReports;

	// Constructors

	/** default constructor */
	public Fund() {
	}

	/** minimal constructor */
	public Fund(Integer ID, String ReportType, Timestamp CreatedDate,
			Timestamp UpdatedDate) {
		this.ID = ID;
		this.ReportType = ReportType;
		this.UpdatedDate = UpdatedDate;
		this.CreatedDate = CreatedDate;
	}

	/** full constructor */
	public Fund(Integer ID, String ReportType, Timestamp UpdatedDate,
			Date HODESH_DIVUACH, String SHEM, Double TSUA_HODSHIT,
			String AD_TKUFAT_DIVUACH, Double ALPHA_SHNATI,
			String MI_TKUFAT_DIVUACH, Double SHARP_RIBIT_HASRAT_SIKUN,
			Double STIAT_TEKEN_36_HODASHIM, Double STIAT_TEKEN_60_HODASHIM,
			Date TAARICH_HAFAKAT_HADOCH, Double TSUA_MEMUZAAT_36_HODASHIM,
			Double TSUA_MEMUZAAT_60_HODASHIM, Double TSUA_MEMUZAAT_LETKUFA,
			Double TSUA_MITZTABERET_36_HODASHIM,
			Double TSUA_MITZTABERET_60_HODASHIM,
			Double TSUA_SHNATIT_MEMUZAAT_3_SHANIM,
			Double TSUA_SHNATIT_MEMUZAAT_5_SHANIM, Double YAHAS_NEZILUT,
			Double TSUA_MITZ_LE_TKUFA, Double SHARP_ANAF,
			Double HAFKADOT_LLO_HAAVAROT, String MATZAV_DIVUACH,
			Double MSHICHOT_LLO_HAAVAROT, String SUG_TAAGID_SHOLET,
			String SHM_HEVRA_MENAHELET, String SHM_TAAGID_SHOLET,
			Double SHIUR_DMEI_NIHUL_AHARON, Double TZVIRA_NETO,
			Double YITRAT_NCHASIM_LSOF_TKUFA, String SUG, Double HAAVAROT,
			Double SHARP_ALL, Double BETA_AGACH_KONTZERNIOT_TZMUDOT,
			Double BETA_AGACH_MEMSHALTIOT_TZMUDOT,
			Double BETA_AGACH_MEM_LO_TZMUDOT, Double BETA_HUTZ_LAARETZ,
			Double BETA_TA100, Double SHIUR_D_NIHUL_HAFKADOT,
			Double SHIUR_D_NIHUL_NECHASIM, String HITMAHUT_MISHNIT,
			String HITMAHUT_RASHIT, String MISPAR_KUPA_AV,
			String UCHLUSIYAT_YAAD, Date TAARICH_HAKAMA,
			Double TSUA_MEMUZAT_HASHKAOT_HOFSHIOT,
			Double ODEF_GIRAON_ACTUARI_AHARON, String ID_HEVRA,
			Double R_SQUARED, String TKUFAT_HAKAMA, Double YIT_NCHASIM_BFOAL,
			String NUM_HEVRA, Double TSUA_MITZT_MI_THILAT_SHANA,
			String TKUFAT_DIVUACH, Date TAARICH_SIUM_PEILUT,
			Double TSUA_NOM_HASHKAOT_HOFSHIOT,
			String YR_ODEF_GIRAON_ACTUARI_AHARON) {

		this.ID = ID;
		this.ReportType = ReportType;
		this.UpdatedDate = UpdatedDate;
		this.HODESH_DIVUACH = HODESH_DIVUACH;
		this.SHEM = SHEM;
		this.TSUA_HODSHIT = TSUA_HODSHIT;
		this.AD_TKUFAT_DIVUACH = AD_TKUFAT_DIVUACH;
		this.ALPHA_SHNATI = ALPHA_SHNATI;
		this.MI_TKUFAT_DIVUACH = MI_TKUFAT_DIVUACH;
		this.SHARP_RIBIT_HASRAT_SIKUN = SHARP_RIBIT_HASRAT_SIKUN;
		this.STIAT_TEKEN_36_HODASHIM = STIAT_TEKEN_36_HODASHIM;
		this.STIAT_TEKEN_60_HODASHIM = STIAT_TEKEN_60_HODASHIM;
		this.TAARICH_HAFAKAT_HADOCH = TAARICH_HAFAKAT_HADOCH;
		this.TSUA_MEMUZAAT_36_HODASHIM = TSUA_MEMUZAAT_36_HODASHIM;
		this.TSUA_MEMUZAAT_60_HODASHIM = TSUA_MEMUZAAT_60_HODASHIM;
		this.TSUA_MEMUZAAT_LETKUFA = TSUA_MEMUZAAT_LETKUFA;
		this.TSUA_MITZTABERET_36_HODASHIM = TSUA_MITZTABERET_36_HODASHIM;
		this.TSUA_MITZTABERET_60_HODASHIM = TSUA_MITZTABERET_60_HODASHIM;
		this.TSUA_SHNATIT_MEMUZAAT_3_SHANIM = TSUA_SHNATIT_MEMUZAAT_3_SHANIM;
		this.TSUA_SHNATIT_MEMUZAAT_5_SHANIM = TSUA_SHNATIT_MEMUZAAT_5_SHANIM;
		this.YAHAS_NEZILUT = YAHAS_NEZILUT;
		this.TSUA_MITZ_LE_TKUFA = TSUA_MITZ_LE_TKUFA;
		this.SHARP_ANAF = SHARP_ANAF;
		this.HAFKADOT_LLO_HAAVAROT = HAFKADOT_LLO_HAAVAROT;
		this.MATZAV_DIVUACH = MATZAV_DIVUACH;
		this.MSHICHOT_LLO_HAAVAROT = MSHICHOT_LLO_HAAVAROT;
		this.SUG_TAAGID_SHOLET = SUG_TAAGID_SHOLET;
		this.SHM_HEVRA_MENAHELET = SHM_HEVRA_MENAHELET;
		this.SHM_TAAGID_SHOLET = SHM_TAAGID_SHOLET;
		this.SHIUR_DMEI_NIHUL_AHARON = SHIUR_DMEI_NIHUL_AHARON;
		this.TZVIRA_NETO = TZVIRA_NETO;
		this.YITRAT_NCHASIM_LSOF_TKUFA = YITRAT_NCHASIM_LSOF_TKUFA;
		this.SUG = SUG;
		this.HAAVAROT = HAAVAROT;
		this.SHARP_ALL = SHARP_ALL;
		this.BETA_AGACH_KONTZERNIOT_TZMUDOT = BETA_AGACH_KONTZERNIOT_TZMUDOT;
		this.BETA_AGACH_MEMSHALTIOT_TZMUDOT = BETA_AGACH_MEMSHALTIOT_TZMUDOT;
		this.BETA_AGACH_MEM_LO_TZMUDOT = BETA_AGACH_MEM_LO_TZMUDOT;
		this.BETA_HUTZ_LAARETZ = BETA_HUTZ_LAARETZ;
		this.BETA_TA100 = BETA_TA100;
		this.SHIUR_D_NIHUL_HAFKADOT = SHIUR_D_NIHUL_HAFKADOT;
		this.SHIUR_D_NIHUL_NECHASIM = SHIUR_D_NIHUL_NECHASIM;
		this.HITMAHUT_MISHNIT = HITMAHUT_MISHNIT;
		this.HITMAHUT_RASHIT = HITMAHUT_RASHIT;
		this.MISPAR_KUPA_AV = MISPAR_KUPA_AV;
		this.UCHLUSIYAT_YAAD = UCHLUSIYAT_YAAD;
		this.TAARICH_HAKAMA = TAARICH_HAKAMA;
		this.TSUA_MEMUZAT_HASHKAOT_HOFSHIOT = TSUA_MEMUZAT_HASHKAOT_HOFSHIOT;
		this.ODEF_GIRAON_ACTUARI_AHARON = ODEF_GIRAON_ACTUARI_AHARON;
		this.ID_HEVRA = ID_HEVRA;
		this.R_SQUARED = R_SQUARED;
		this.TKUFAT_HAKAMA = TKUFAT_HAKAMA;
		this.YIT_NCHASIM_BFOAL = YIT_NCHASIM_BFOAL;
		this.NUM_HEVRA = NUM_HEVRA;
		this.TSUA_MITZT_MI_THILAT_SHANA = TSUA_MITZT_MI_THILAT_SHANA;
		this.TKUFAT_DIVUACH = TKUFAT_DIVUACH;
		this.TAARICH_SIUM_PEILUT = TAARICH_SIUM_PEILUT;
		this.TSUA_NOM_HASHKAOT_HOFSHIOT = TSUA_NOM_HASHKAOT_HOFSHIOT;
		this.YR_ODEF_GIRAON_ACTUARI_AHARON = YR_ODEF_GIRAON_ACTUARI_AHARON;
	}

	// Property accessors
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "Key", unique = true, nullable = false)
	public Long getKey() {
		return this.Key;
	}

	public void setKey(Long Key) {
		this.Key = Key;
	}

	@Column(name = "ID", nullable = false)
	public Integer getID() {
		return this.ID;
	}

	public void setID(Integer ID) {
		this.ID = ID;
	}

	@Column(name = "ReportType", nullable = false, length = 60)
	public String getReportType() {
		return this.ReportType;
	}

	public void setReportType(String ReportType) {
		this.ReportType = ReportType;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "CreatedDate", nullable = false, length = 19)
	public Date getCreatedDate() {
		return this.CreatedDate;
	}

	public void setCreatedDate(Date CreatedDate) {
		this.CreatedDate = CreatedDate;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "UpdatedDate", nullable = false, length = 19)
	public Date getUpdatedDate() {
		return this.UpdatedDate;
	}

	public void setUpdatedDate(Date UpdatedDate) {
		this.UpdatedDate = UpdatedDate;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "HODESH_DIVUACH", length = 10)
	public Date getHODESH_DIVUACH() {
		return this.HODESH_DIVUACH;
	}

	public void setHODESH_DIVUACH(Date HODESH_DIVUACH) {
		this.HODESH_DIVUACH = HODESH_DIVUACH;
	}

	@Column(name = "SHEM", length = 250)
	public String getSHEM() {
		return this.SHEM;
	}

	public void setSHEM(String SHEM) {
		this.SHEM = SHEM;
	}

	@Column(name = "TSUA_HODSHIT", precision = 16, scale = 6)
	public Double getTSUA_HODSHIT() {
		return this.TSUA_HODSHIT;
	}

	public void setTSUA_HODSHIT(Double TSUA_HODSHIT) {
		this.TSUA_HODSHIT = TSUA_HODSHIT;
	}

	@Column(name = "AD_TKUFAT_DIVUACH", length = 250)
	public String getAD_TKUFAT_DIVUACH() {
		return this.AD_TKUFAT_DIVUACH;
	}

	public void setAD_TKUFAT_DIVUACH(String AD_TKUFAT_DIVUACH) {
		this.AD_TKUFAT_DIVUACH = AD_TKUFAT_DIVUACH;
	}

	@Column(name = "ALPHA_SHNATI", precision = 16, scale = 6)
	public Double getALPHA_SHNATI() {
		return this.ALPHA_SHNATI;
	}

	public void setALPHA_SHNATI(Double ALPHA_SHNATI) {
		this.ALPHA_SHNATI = ALPHA_SHNATI;
	}

	@Column(name = "MI_TKUFAT_DIVUACH", length = 250)
	public String getMI_TKUFAT_DIVUACH() {
		return this.MI_TKUFAT_DIVUACH;
	}

	public void setMI_TKUFAT_DIVUACH(String MI_TKUFAT_DIVUACH) {
		this.MI_TKUFAT_DIVUACH = MI_TKUFAT_DIVUACH;
	}

	@Column(name = "SHARP_RIBIT_HASRAT_SIKUN", precision = 16, scale = 6)
	public Double getSHARP_RIBIT_HASRAT_SIKUN() {
		return this.SHARP_RIBIT_HASRAT_SIKUN;
	}

	public void setSHARP_RIBIT_HASRAT_SIKUN(Double SHARP_RIBIT_HASRAT_SIKUN) {
		this.SHARP_RIBIT_HASRAT_SIKUN = SHARP_RIBIT_HASRAT_SIKUN;
	}

	@Column(name = "STIAT_TEKEN_36_HODASHIM", precision = 16, scale = 6)
	public Double getSTIAT_TEKEN_36_HODASHIM() {
		return this.STIAT_TEKEN_36_HODASHIM;
	}

	public void setSTIAT_TEKEN_36_HODASHIM(Double STIAT_TEKEN_36_HODASHIM) {
		this.STIAT_TEKEN_36_HODASHIM = STIAT_TEKEN_36_HODASHIM;
	}

	@Column(name = "STIAT_TEKEN_60_HODASHIM", precision = 16, scale = 6)
	public Double getSTIAT_TEKEN_60_HODASHIM() {
		return this.STIAT_TEKEN_60_HODASHIM;
	}

	public void setSTIAT_TEKEN_60_HODASHIM(Double STIAT_TEKEN_60_HODASHIM) {
		this.STIAT_TEKEN_60_HODASHIM = STIAT_TEKEN_60_HODASHIM;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "TAARICH_HAFAKAT_HADOCH", length = 10)
	public Date getTAARICH_HAFAKAT_HADOCH() {
		return this.TAARICH_HAFAKAT_HADOCH;
	}

	public void setTAARICH_HAFAKAT_HADOCH(Date TAARICH_HAFAKAT_HADOCH) {
		this.TAARICH_HAFAKAT_HADOCH = TAARICH_HAFAKAT_HADOCH;
	}

	@Column(name = "TSUA_MEMUZAAT_36_HODASHIM", precision = 16, scale = 6)
	public Double getTSUA_MEMUZAAT_36_HODASHIM() {
		return this.TSUA_MEMUZAAT_36_HODASHIM;
	}

	public void setTSUA_MEMUZAAT_36_HODASHIM(Double TSUA_MEMUZAAT_36_HODASHIM) {
		this.TSUA_MEMUZAAT_36_HODASHIM = TSUA_MEMUZAAT_36_HODASHIM;
	}

	@Column(name = "TSUA_MEMUZAAT_60_HODASHIM", precision = 16, scale = 6)
	public Double getTSUA_MEMUZAAT_60_HODASHIM() {
		return this.TSUA_MEMUZAAT_60_HODASHIM;
	}

	public void setTSUA_MEMUZAAT_60_HODASHIM(Double TSUA_MEMUZAAT_60_HODASHIM) {
		this.TSUA_MEMUZAAT_60_HODASHIM = TSUA_MEMUZAAT_60_HODASHIM;
	}

	@Column(name = "TSUA_MEMUZAAT_LETKUFA", precision = 16, scale = 6)
	public Double getTSUA_MEMUZAAT_LETKUFA() {
		return this.TSUA_MEMUZAAT_LETKUFA;
	}

	public void setTSUA_MEMUZAAT_LETKUFA(Double TSUA_MEMUZAAT_LETKUFA) {
		this.TSUA_MEMUZAAT_LETKUFA = TSUA_MEMUZAAT_LETKUFA;
	}

	@Column(name = "TSUA_MITZTABERET_36_HODASHIM", precision = 16, scale = 6)
	public Double getTSUA_MITZTABERET_36_HODASHIM() {
		return this.TSUA_MITZTABERET_36_HODASHIM;
	}

	public void setTSUA_MITZTABERET_36_HODASHIM(
			Double TSUA_MITZTABERET_36_HODASHIM) {
		this.TSUA_MITZTABERET_36_HODASHIM = TSUA_MITZTABERET_36_HODASHIM;
	}

	@Column(name = "TSUA_MITZTABERET_60_HODASHIM", precision = 16, scale = 6)
	public Double getTSUA_MITZTABERET_60_HODASHIM() {
		return this.TSUA_MITZTABERET_60_HODASHIM;
	}

	public void setTSUA_MITZTABERET_60_HODASHIM(
			Double TSUA_MITZTABERET_60_HODASHIM) {
		this.TSUA_MITZTABERET_60_HODASHIM = TSUA_MITZTABERET_60_HODASHIM;
	}

	@Column(name = "TSUA_SHNATIT_MEMUZAAT_3_SHANIM", precision = 16, scale = 6)
	public Double getTSUA_SHNATIT_MEMUZAAT_3_SHANIM() {
		return this.TSUA_SHNATIT_MEMUZAAT_3_SHANIM;
	}

	public void setTSUA_SHNATIT_MEMUZAAT_3_SHANIM(
			Double TSUA_SHNATIT_MEMUZAAT_3_SHANIM) {
		this.TSUA_SHNATIT_MEMUZAAT_3_SHANIM = TSUA_SHNATIT_MEMUZAAT_3_SHANIM;
	}

	@Column(name = "TSUA_SHNATIT_MEMUZAAT_5_SHANIM", precision = 16, scale = 6)
	public Double getTSUA_SHNATIT_MEMUZAAT_5_SHANIM() {
		return this.TSUA_SHNATIT_MEMUZAAT_5_SHANIM;
	}

	public void setTSUA_SHNATIT_MEMUZAAT_5_SHANIM(
			Double TSUA_SHNATIT_MEMUZAAT_5_SHANIM) {
		this.TSUA_SHNATIT_MEMUZAAT_5_SHANIM = TSUA_SHNATIT_MEMUZAAT_5_SHANIM;
	}

	@Column(name = "YAHAS_NEZILUT", precision = 16, scale = 6)
	public Double getYAHAS_NEZILUT() {
		return this.YAHAS_NEZILUT;
	}

	public void setYAHAS_NEZILUT(Double YAHAS_NEZILUT) {
		this.YAHAS_NEZILUT = YAHAS_NEZILUT;
	}

	@Column(name = "TSUA_MITZ_LE_TKUFA", precision = 16, scale = 6)
	public Double getTSUA_MITZ_LE_TKUFA() {
		return this.TSUA_MITZ_LE_TKUFA;
	}

	public void setTSUA_MITZ_LE_TKUFA(Double TSUA_MITZ_LE_TKUFA) {
		this.TSUA_MITZ_LE_TKUFA = TSUA_MITZ_LE_TKUFA;
	}

	@Column(name = "SHARP_ANAF", precision = 16, scale = 6)
	public Double getSHARP_ANAF() {
		return this.SHARP_ANAF;
	}

	public void setSHARP_ANAF(Double SHARP_ANAF) {
		this.SHARP_ANAF = SHARP_ANAF;
	}

	@Column(name = "HAFKADOT_LLO_HAAVAROT", precision = 16, scale = 6)
	public Double getHAFKADOT_LLO_HAAVAROT() {
		return this.HAFKADOT_LLO_HAAVAROT;
	}

	public void setHAFKADOT_LLO_HAAVAROT(Double HAFKADOT_LLO_HAAVAROT) {
		this.HAFKADOT_LLO_HAAVAROT = HAFKADOT_LLO_HAAVAROT;
	}

	@Column(name = "MATZAV_DIVUACH", length = 250)
	public String getMATZAV_DIVUACH() {
		return this.MATZAV_DIVUACH;
	}

	public void setMATZAV_DIVUACH(String MATZAV_DIVUACH) {
		this.MATZAV_DIVUACH = MATZAV_DIVUACH;
	}

	@Column(name = "MSHICHOT_LLO_HAAVAROT", precision = 16, scale = 6)
	public Double getMSHICHOT_LLO_HAAVAROT() {
		return this.MSHICHOT_LLO_HAAVAROT;
	}

	public void setMSHICHOT_LLO_HAAVAROT(Double MSHICHOT_LLO_HAAVAROT) {
		this.MSHICHOT_LLO_HAAVAROT = MSHICHOT_LLO_HAAVAROT;
	}

	@Column(name = "SUG_TAAGID_SHOLET", length = 250)
	public String getSUG_TAAGID_SHOLET() {
		return this.SUG_TAAGID_SHOLET;
	}

	public void setSUG_TAAGID_SHOLET(String SUG_TAAGID_SHOLET) {
		this.SUG_TAAGID_SHOLET = SUG_TAAGID_SHOLET;
	}

	@Column(name = "SHM_HEVRA_MENAHELET", length = 250)
	public String getSHM_HEVRA_MENAHELET() {
		return this.SHM_HEVRA_MENAHELET;
	}

	public void setSHM_HEVRA_MENAHELET(String SHM_HEVRA_MENAHELET) {
		this.SHM_HEVRA_MENAHELET = SHM_HEVRA_MENAHELET;
	}

	@Column(name = "SHM_TAAGID_SHOLET", length = 250)
	public String getSHM_TAAGID_SHOLET() {
		return this.SHM_TAAGID_SHOLET;
	}

	public void setSHM_TAAGID_SHOLET(String SHM_TAAGID_SHOLET) {
		this.SHM_TAAGID_SHOLET = SHM_TAAGID_SHOLET;
	}

	@Column(name = "SHIUR_DMEI_NIHUL_AHARON", precision = 16, scale = 6)
	public Double getSHIUR_DMEI_NIHUL_AHARON() {
		return this.SHIUR_DMEI_NIHUL_AHARON;
	}

	public void setSHIUR_DMEI_NIHUL_AHARON(Double SHIUR_DMEI_NIHUL_AHARON) {
		this.SHIUR_DMEI_NIHUL_AHARON = SHIUR_DMEI_NIHUL_AHARON;
	}

	@Column(name = "TZVIRA_NETO", precision = 16, scale = 6)
	public Double getTZVIRA_NETO() {
		return this.TZVIRA_NETO;
	}

	public void setTZVIRA_NETO(Double TZVIRA_NETO) {
		this.TZVIRA_NETO = TZVIRA_NETO;
	}

	@Column(name = "YITRAT_NCHASIM_LSOF_TKUFA", precision = 16, scale = 6)
	public Double getYITRAT_NCHASIM_LSOF_TKUFA() {
		return this.YITRAT_NCHASIM_LSOF_TKUFA;
	}

	public void setYITRAT_NCHASIM_LSOF_TKUFA(Double YITRAT_NCHASIM_LSOF_TKUFA) {
		this.YITRAT_NCHASIM_LSOF_TKUFA = YITRAT_NCHASIM_LSOF_TKUFA;
	}

	@Column(name = "SUG", length = 250)
	public String getSUG() {
		return this.SUG;
	}

	public void setSUG(String SUG) {
		this.SUG = SUG;
	}

	@Column(name = "HAAVAROT", precision = 16, scale = 6)
	public Double getHAAVAROT() {
		return this.HAAVAROT;
	}

	public void setHAAVAROT(Double HAAVAROT) {
		this.HAAVAROT = HAAVAROT;
	}

	@Column(name = "SHARP_ALL", precision = 16, scale = 6)
	public Double getSHARP_ALL() {
		return this.SHARP_ALL;
	}

	public void setSHARP_ALL(Double SHARP_ALL) {
		this.SHARP_ALL = SHARP_ALL;
	}

	@Column(name = "BETA_AGACH_KONTZERNIOT_TZMUDOT", precision = 16, scale = 6)
	public Double getBETA_AGACH_KONTZERNIOT_TZMUDOT() {
		return this.BETA_AGACH_KONTZERNIOT_TZMUDOT;
	}

	public void setBETA_AGACH_KONTZERNIOT_TZMUDOT(
			Double BETA_AGACH_KONTZERNIOT_TZMUDOT) {
		this.BETA_AGACH_KONTZERNIOT_TZMUDOT = BETA_AGACH_KONTZERNIOT_TZMUDOT;
	}

	@Column(name = "BETA_AGACH_MEMSHALTIOT_TZMUDOT", precision = 16, scale = 6)
	public Double getBETA_AGACH_MEMSHALTIOT_TZMUDOT() {
		return this.BETA_AGACH_MEMSHALTIOT_TZMUDOT;
	}

	public void setBETA_AGACH_MEMSHALTIOT_TZMUDOT(
			Double BETA_AGACH_MEMSHALTIOT_TZMUDOT) {
		this.BETA_AGACH_MEMSHALTIOT_TZMUDOT = BETA_AGACH_MEMSHALTIOT_TZMUDOT;
	}

	@Column(name = "BETA_AGACH_MEM_LO_TZMUDOT", precision = 16, scale = 6)
	public Double getBETA_AGACH_MEM_LO_TZMUDOT() {
		return this.BETA_AGACH_MEM_LO_TZMUDOT;
	}

	public void setBETA_AGACH_MEM_LO_TZMUDOT(Double BETA_AGACH_MEM_LO_TZMUDOT) {
		this.BETA_AGACH_MEM_LO_TZMUDOT = BETA_AGACH_MEM_LO_TZMUDOT;
	}

	@Column(name = "BETA_HUTZ_LAARETZ", precision = 16, scale = 6)
	public Double getBETA_HUTZ_LAARETZ() {
		return this.BETA_HUTZ_LAARETZ;
	}

	public void setBETA_HUTZ_LAARETZ(Double BETA_HUTZ_LAARETZ) {
		this.BETA_HUTZ_LAARETZ = BETA_HUTZ_LAARETZ;
	}

	@Column(name = "BETA_TA100", precision = 16, scale = 6)
	public Double getBETA_TA100() {
		return this.BETA_TA100;
	}

	public void setBETA_TA100(Double BETA_TA100) {
		this.BETA_TA100 = BETA_TA100;
	}

	@Column(name = "SHIUR_D_NIHUL_HAFKADOT", precision = 16, scale = 6)
	public Double getSHIUR_D_NIHUL_HAFKADOT() {
		return this.SHIUR_D_NIHUL_HAFKADOT;
	}

	public void setSHIUR_D_NIHUL_HAFKADOT(Double SHIUR_D_NIHUL_HAFKADOT) {
		this.SHIUR_D_NIHUL_HAFKADOT = SHIUR_D_NIHUL_HAFKADOT;
	}

	@Column(name = "SHIUR_D_NIHUL_NECHASIM", precision = 16, scale = 6)
	public Double getSHIUR_D_NIHUL_NECHASIM() {
		return this.SHIUR_D_NIHUL_NECHASIM;
	}

	public void setSHIUR_D_NIHUL_NECHASIM(Double SHIUR_D_NIHUL_NECHASIM) {
		this.SHIUR_D_NIHUL_NECHASIM = SHIUR_D_NIHUL_NECHASIM;
	}

	@Column(name = "HITMAHUT_MISHNIT", length = 250)
	public String getHITMAHUT_MISHNIT() {
		return this.HITMAHUT_MISHNIT;
	}

	public void setHITMAHUT_MISHNIT(String HITMAHUT_MISHNIT) {
		this.HITMAHUT_MISHNIT = HITMAHUT_MISHNIT;
	}

	@Column(name = "HITMAHUT_RASHIT", length = 250)
	public String getHITMAHUT_RASHIT() {
		return this.HITMAHUT_RASHIT;
	}

	public void setHITMAHUT_RASHIT(String HITMAHUT_RASHIT) {
		this.HITMAHUT_RASHIT = HITMAHUT_RASHIT;
	}

	@Column(name = "MISPAR_KUPA_AV", length = 250)
	public String getMISPAR_KUPA_AV() {
		return this.MISPAR_KUPA_AV;
	}

	public void setMISPAR_KUPA_AV(String MISPAR_KUPA_AV) {
		this.MISPAR_KUPA_AV = MISPAR_KUPA_AV;
	}

	@Column(name = "UCHLUSIYAT_YAAD", length = 250)
	public String getUCHLUSIYAT_YAAD() {
		return this.UCHLUSIYAT_YAAD;
	}

	public void setUCHLUSIYAT_YAAD(String UCHLUSIYAT_YAAD) {
		this.UCHLUSIYAT_YAAD = UCHLUSIYAT_YAAD;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "TAARICH_HAKAMA", length = 10)
	public Date getTAARICH_HAKAMA() {
		return this.TAARICH_HAKAMA;
	}

	public void setTAARICH_HAKAMA(Date TAARICH_HAKAMA) {
		this.TAARICH_HAKAMA = TAARICH_HAKAMA;
	}

	@Column(name = "TSUA_MEMUZAT_HASHKAOT_HOFSHIOT", precision = 16, scale = 6)
	public Double getTSUA_MEMUZAT_HASHKAOT_HOFSHIOT() {
		return this.TSUA_MEMUZAT_HASHKAOT_HOFSHIOT;
	}

	public void setTSUA_MEMUZAT_HASHKAOT_HOFSHIOT(
			Double TSUA_MEMUZAT_HASHKAOT_HOFSHIOT) {
		this.TSUA_MEMUZAT_HASHKAOT_HOFSHIOT = TSUA_MEMUZAT_HASHKAOT_HOFSHIOT;
	}

	@Column(name = "ODEF_GIRAON_ACTUARI_AHARON", precision = 16, scale = 6)
	public Double getODEF_GIRAON_ACTUARI_AHARON() {
		return this.ODEF_GIRAON_ACTUARI_AHARON;
	}

	public void setODEF_GIRAON_ACTUARI_AHARON(Double ODEF_GIRAON_ACTUARI_AHARON) {
		this.ODEF_GIRAON_ACTUARI_AHARON = ODEF_GIRAON_ACTUARI_AHARON;
	}

	@Column(name = "ID_HEVRA", length = 250)
	public String getID_HEVRA() {
		return this.ID_HEVRA;
	}

	public void setID_HEVRA(String ID_HEVRA) {
		this.ID_HEVRA = ID_HEVRA;
	}

	@Column(name = "R_SQUARED", precision = 16, scale = 6)
	public Double getR_SQUARED() {
		return this.R_SQUARED;
	}

	public void setR_SQUARED(Double R_SQUARED) {
		this.R_SQUARED = R_SQUARED;
	}

	@Column(name = "TKUFAT_HAKAMA", length = 250)
	public String getTKUFAT_HAKAMA() {
		return this.TKUFAT_HAKAMA;
	}

	public void setTKUFAT_HAKAMA(String TKUFAT_HAKAMA) {
		this.TKUFAT_HAKAMA = TKUFAT_HAKAMA;
	}

	@Column(name = "YIT_NCHASIM_BFOAL", precision = 16, scale = 6)
	public Double getYIT_NCHASIM_BFOAL() {
		return this.YIT_NCHASIM_BFOAL;
	}

	public void setYIT_NCHASIM_BFOAL(Double YIT_NCHASIM_BFOAL) {
		this.YIT_NCHASIM_BFOAL = YIT_NCHASIM_BFOAL;
	}

	@Column(name = "NUM_HEVRA", length = 250)
	public String getNUM_HEVRA() {
		return this.NUM_HEVRA;
	}

	public void setNUM_HEVRA(String NUM_HEVRA) {
		this.NUM_HEVRA = NUM_HEVRA;
	}

	@Column(name = "TSUA_MITZT_MI_THILAT_SHANA", precision = 16, scale = 6)
	public Double getTSUA_MITZT_MI_THILAT_SHANA() {
		return this.TSUA_MITZT_MI_THILAT_SHANA;
	}

	public void setTSUA_MITZT_MI_THILAT_SHANA(Double TSUA_MITZT_MI_THILAT_SHANA) {
		this.TSUA_MITZT_MI_THILAT_SHANA = TSUA_MITZT_MI_THILAT_SHANA;
	}

	@Column(name = "TKUFAT_DIVUACH", length = 250)
	public String getTKUFAT_DIVUACH() {
		return this.TKUFAT_DIVUACH;
	}

	public void setTKUFAT_DIVUACH(String TKUFAT_DIVUACH) {
		this.TKUFAT_DIVUACH = TKUFAT_DIVUACH;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "TAARICH_SIUM_PEILUT", length = 10)
	public Date getTAARICH_SIUM_PEILUT() {
		return this.TAARICH_SIUM_PEILUT;
	}

	public void setTAARICH_SIUM_PEILUT(Date TAARICH_SIUM_PEILUT) {
		this.TAARICH_SIUM_PEILUT = TAARICH_SIUM_PEILUT;
	}

	@Column(name = "TSUA_NOM_HASHKAOT_HOFSHIOT", precision = 16, scale = 6)
	public Double getTSUA_NOM_HASHKAOT_HOFSHIOT() {
		return this.TSUA_NOM_HASHKAOT_HOFSHIOT;
	}

	public void setTSUA_NOM_HASHKAOT_HOFSHIOT(Double TSUA_NOM_HASHKAOT_HOFSHIOT) {
		this.TSUA_NOM_HASHKAOT_HOFSHIOT = TSUA_NOM_HASHKAOT_HOFSHIOT;
	}

	@Column(name = "YR_ODEF_GIRAON_ACTUARI_AHARON", length = 250)
	public String getYR_ODEF_GIRAON_ACTUARI_AHARON() {
		return this.YR_ODEF_GIRAON_ACTUARI_AHARON;
	}

	public void setYR_ODEF_GIRAON_ACTUARI_AHARON(
			String YR_ODEF_GIRAON_ACTUARI_AHARON) {
		this.YR_ODEF_GIRAON_ACTUARI_AHARON = YR_ODEF_GIRAON_ACTUARI_AHARON;
	}

	@Column(name = "ODEF_GIRAON_ACTUARI_LETKUFA", length = 250)
	public String getODEF_GIRAON_ACTUARI_LETKUFA() {
		return ODEF_GIRAON_ACTUARI_LETKUFA;
	}

	public void setODEF_GIRAON_ACTUARI_LETKUFA(
			String oDEF_GIRAON_ACTUARI_LETKUFA) {
		ODEF_GIRAON_ACTUARI_LETKUFA = oDEF_GIRAON_ACTUARI_LETKUFA;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "fund")
	public Set<AssetFullDetailedReport> getAssetFullDetailedReports() {
		return assetFullDetailedReports;
	}

	public void setAssetFullDetailedReports(
			Set<AssetFullDetailedReport> assetFullDetailedReports) {
		this.assetFullDetailedReports = assetFullDetailedReports;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "fund")
	public Set<AssetMainGroupDetailedReport> getAssetMainGroupDetailedReports() {
		return assetMainGroupDetailedReports;
	}

	public void setAssetMainGroupDetailedReports(
			Set<AssetMainGroupDetailedReport> assetMainGroupDetailedReports) {
		this.assetMainGroupDetailedReports = assetMainGroupDetailedReports;
	}

	@Override
	public String toString() {
		try {
			Class<Fund> fundClass = Fund.class;
			Field[] fundFields = fundClass.getDeclaredFields();
			String fieldName;
			Object fieldValue = null;
			StringBuffer sb = new StringBuffer();
			for (int i = 1 /* skip serialVersionUID */; i < fundFields.length; i++) {
				fieldName = fundFields[i].getName();
				if (fieldName.equalsIgnoreCase("updateddate")
						|| fieldName
								.equalsIgnoreCase("assetFullDetailedReports")
						|| fieldName
								.equalsIgnoreCase("assetMainGroupDetailedReports"))
					continue;
				try {
					fieldValue = "" + fundFields[i].get(this);
				} catch (IllegalArgumentException e) {
					fieldValue = "N\\A";
				} catch (IllegalAccessException e) {
					fieldValue = "N\\A";
				}
				sb.append(fieldName + "=" + fieldValue + ", ");
			}
			return sb.toString();

		} catch (Exception e) {
			e.printStackTrace();
		}
		return "ERROR Fund.toString()";
	}
}