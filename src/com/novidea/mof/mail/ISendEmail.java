package com.novidea.mof.mail;


public interface ISendEmail {
	public void sendEmail(String msgSubject, String msgBody);
}