package com.novidea.mof.mail;

import java.util.Properties;

import javax.mail.*;
import javax.mail.internet.*;

import com.novidea.mof.Log;

/**
 * 
 * @author Galil.Zussman
 * @date Dec 1, 2014
 */
public class AmazonSES implements ISendEmail {

	private static final String FROM = "support@novideasoft.com"; // Replace with your "From" address. This address must be verified.
	private static final String TO = "support@novideasoft.com"; // Replace with a "To" address. If you have not yet requested
	// production access, this address must be verified.

	// Supply your SMTP credentials below. Note that your SMTP credentials are different from your AWS credentials.
	private static final String SMTP_USERNAME = "AKIAJFGTELL55NXC7ZVQ";
	private static final String SMTP_PASSWORD = "AuK433oXlrBLHvPmTLL4XHiz4Mc3gvxwyxVr1+nO2+Oy";

	// Amazon SES SMTP host name. This example uses the us-east-1 region.
	private static final String HOST = "email-smtp.us-east-1.amazonaws.com";

	// Port we will connect to on the Amazon SES SMTP endpoint. We are choosing port 25 because we will use
	// STARTTLS to encrypt the connection.
	private static final int PORT = 25;

	public void sendEmail(String msgSubject, String msgBody) {

		// Create a Properties object to contain connection configuration information.
		Properties props = System.getProperties();
		props.put("mail.transport.protocol", "smtp");
		props.put("mail.smtp.port", PORT);

		// Set properties indicating that we want to use STARTTLS to encrypt the connection.
		// The SMTP session will begin on an unencrypted connection, and then the client
		// will issue a STARTTLS command to upgrade to an encrypted connection.
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.starttls.enable", "true");
		props.put("mail.smtp.starttls.required", "true");

		// Create a Session object to represent a mail session with the specified properties. 
		Session session = Session.getDefaultInstance(props);
		Transport transport = null;
		try {

			// Create a message with the specified information. 
			MimeMessage msg = new MimeMessage(session);
			msg.setFrom(new InternetAddress(FROM));
			msg.setRecipient(Message.RecipientType.TO, new InternetAddress(TO));
			msg.setSubject(msgSubject);
			msg.setContent(msgBody, "text/plain");

			// Create a transport.        
			transport = session.getTransport();
			// Send the message.

			Log.info("Attempting to send an email through the Amazon SES SMTP interface...");

			// Connect to Amazon SES using the SMTP username and password you specified above.
			transport.connect(HOST, SMTP_USERNAME, SMTP_PASSWORD);

			// Send the email.
			transport.sendMessage(msg, msg.getAllRecipients());
			Log.info("Email sent!");
		} catch (Exception ex) {
			Log.error("The email was not sent.");
			Log.error("Error message: " + ex.getMessage());
		} finally {
			// Close and terminate the connection.
			try {
				if (transport != null)
					transport.close();
			} catch (MessagingException e) {
				Log.error("Error message: " + e.getMessage());
			}
		}
	}
}
