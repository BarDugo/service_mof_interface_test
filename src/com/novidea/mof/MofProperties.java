package com.novidea.mof;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**   
 * @author Galil.Zussman
 * @date   Dec 9, 2014
 */
public class MofProperties {
	static Properties prop;

	static {
		init();
	}

	private static void init() {
		prop = new Properties();
		InputStream input = null;
		try {

			String filename = "config.properties";
			input = MofProperties.class.getClassLoader().getResourceAsStream(
					filename);
			if (input == null) {
				System.out.println("Sorry, unable to find " + filename);
				return;
			}
			//load a properties file from class path, inside static method
			prop.load(input);

		} catch (IOException ex) {
			ex.printStackTrace();
		} finally {
			if (input != null) {
				try {
					input.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public static String get(String key) {
		return prop.getProperty(key);
	}
}
