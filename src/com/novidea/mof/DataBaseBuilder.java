package com.novidea.mof;

import java.io.IOException;
import java.io.StringReader;
import java.lang.reflect.Method;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.xml.parsers.DocumentBuilderFactory;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.joda.time.DateTime;
import org.joda.time.Duration;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import com.novidea.mof.entities.ReportType;
import com.novidea.mof.entities.dao.http.HttpDaoHelper;
import com.novidea.mof.entities.dao.sql.FundDaoHelper;
import com.novidea.mof.entities.model.AssetFullDetailedReport;
import com.novidea.mof.entities.model.AssetMainGroupDetailedReport;
import com.novidea.mof.entities.model.AssetYieldsAndBalancesReport;
import com.novidea.mof.entities.model.DataApiNamesMap;
import com.novidea.mof.entities.model.DateCoverage;
import com.novidea.mof.entities.model.Fund;
import com.novidea.mof.entities.service.ServiceHolder;
import com.novidea.mof.exception.FailedToGetReportTypeException;
import com.novidea.mof.exception.FundMethodInvocationException;
import com.novidea.mof.exception.FundMethodReflectException;
import com.novidea.mof.exception.FundXMLParseException;
import com.novidea.mof.hibernate.HibernateSessionFactory;
import com.novidea.mof.mail.AmazonSES;
import com.novidea.mof.mail.ISendEmail;

/**
 * 
 * @author Galil.Zussman
 * @date Aug 1, 2014
 */
public class DataBaseBuilder {

	/**  global variables for quick developing  **/
	private static final int YEARS_BEFORE = 7;/* how many years back for building the DB - DEFAULT 7 */
	private static final int MINUS_MONTHS = 1;// this month - var for debug (should be '1')
	private static final int START_COUNT_MONTH = 0;//DEBUG (should be 0 !!!)

	ServiceHolder serviceHolder = ServiceHolder.getInstance();

	public static final Set<String> FUND_MOF_DATE_FIELDS;
	static {
		FUND_MOF_DATE_FIELDS = new HashSet<String>();
		FUND_MOF_DATE_FIELDS.add("TAARICH_HAFAKAT_HADOCH");
		FUND_MOF_DATE_FIELDS.add("TAARICH_HAKAMA");
		FUND_MOF_DATE_FIELDS.add("TAARICH_SIUM_PEILUT");
	}

	private Date executionStart;
	
	public static void execute(Integer yearsBack) {
		execute(yearsBack,null);
	}

	public static void execute(Integer yearsBack, Integer monthsBack) {
		DataBaseBuilder baseBuilder = new DataBaseBuilder();
		baseBuilder.executionStart = DateTime.now().toDate();
		baseBuilder.buildDataBase(yearsBack,monthsBack);
		baseBuilder.sendEmailReport();
	}

	// OPTIONAL: for testing through JSF page 
	public static void func() {
		//ReportHolder.getLastReportedMonth();
		//	DataBaseBuilder baseBuilder = new DataBaseBuilder();
		//	baseBuilder.getGemelLastMonthResults();
	}

	// //////////////////////////////////////////////////////////////////////
	// OPERATION
	// //////////////////////////////////////////////////////////////////////////

	private boolean rooledback = false;
	private StringBuilder sbSuccess;
	private StringBuilder sbFail;
	private StringBuilder sbLogger;

	/**
	 * running this method takes at least 100min (environment depended) for a
	 * full DataBase build up. adds months that aren't exist (follow example in
	 * method body)
	 */
	public void buildDataBase(Integer yearsBack, Integer monthsBack) {
		sbSuccess = new StringBuilder();
		sbFail = new StringBuilder();
		sbLogger = new StringBuilder();
		List<DateCoverage> dateCoverages;
		List<Fund> funds;
		List<AssetFullDetailedReport> assetFullDetailedReports;
		List<AssetMainGroupDetailedReport> assetMainGroupDetailedReports;
		List<AssetYieldsAndBalancesReport> assetYieldsAndBalancesReports;

		// 1.1.(thisYear - yearsBefore)
		//
		// === example of operation to 24.5.2014 date ===
		//
		// if date is 24.5.2014 and yearsBefore = 10 ====>> 1.1.2004
		// will run until 1.12.2013 (include)
		int yearsBefore = (yearsBack == null ? YEARS_BEFORE : yearsBack);
		DateTime startDate = DateTime.now().minusYears(yearsBefore)
				.minusMonths(DateTime.now().getMonthOfYear() - 1)
				.minusDays((DateTime.now().getDayOfMonth() - 1))
				.withTimeAtStartOfDay();

		/* COMMENT TO THE END OF THE LOOP for quick build for only this year  months */

		for (int i = 0; i < yearsBefore; i++) {
			for (int j = 0; j < 12; j++) {
				dateCoverages = new ArrayList<DateCoverage>();
				funds = new ArrayList<Fund>();
				assetFullDetailedReports = new ArrayList<AssetFullDetailedReport>();
				assetMainGroupDetailedReports = new ArrayList<AssetMainGroupDetailedReport>();
				assetYieldsAndBalancesReports = new ArrayList<AssetYieldsAndBalancesReport>();

				if (fetchMonthOfYearObjects(dateCoverages, funds,
						assetFullDetailedReports,
						assetMainGroupDetailedReports,
						assetYieldsAndBalancesReports, startDate))
					saveMonthlyResults(dateCoverages, funds,
							assetFullDetailedReports,
							assetMainGroupDetailedReports,
							assetYieldsAndBalancesReports);

				startDate = startDate.plusMonths(1);
			}
		}

		// the rest of this year 1.1.2014 to 1.4.2014 

		startDate = DateTime
				.now()
				.minusMonths(
						DateTime.now().getMonthOfYear() - START_COUNT_MONTH - 1)
				.minusDays((DateTime.now().getDayOfMonth() - 1))
				.withTimeAtStartOfDay();

		int untilMonth = DateTime.now().minusMonths(monthsBack==null?MINUS_MONTHS:monthsBack).getMonthOfYear();
		for (int j = START_COUNT_MONTH; j < untilMonth; j++) {
			// initialize new instances for a monthly iteration of all report types
			dateCoverages = new ArrayList<DateCoverage>();
			funds = new ArrayList<Fund>();
			assetFullDetailedReports = new ArrayList<AssetFullDetailedReport>();
			assetMainGroupDetailedReports = new ArrayList<AssetMainGroupDetailedReport>();
			assetYieldsAndBalancesReports = new ArrayList<AssetYieldsAndBalancesReport>();

			if (fetchMonthOfYearObjects(dateCoverages, funds,
					assetFullDetailedReports, assetMainGroupDetailedReports,
					assetYieldsAndBalancesReports, startDate))
				saveMonthlyResults(dateCoverages, funds,
						assetFullDetailedReports,
						assetMainGroupDetailedReports,
						assetYieldsAndBalancesReports);
			startDate = startDate.plusMonths(1);
		}

	}

	void sendEmailReport() {
		StringBuilder sb = new StringBuilder();
		sb.append("Starting time of DataBase build/refresh execution: "
				+ executionStart + "\n\n");
		sb.append("-----------------------------------------------------------------------------------------------------");
		sb.append("\nLOGGER - logging the Http DataSource flow process, even if not inserted\n");
		sb.append(sbLogger);
		sb.append("-----------------------------------------------------------------------------------------------------"
				+ "\n\n");
		sb.append("Summary of data inserted to the DataBase\n");
		sb.append("Success at: \n" + sbSuccess + "\n");
		sb.append("Fail at: \n" + sbFail + "\n\n\n");
		sb.append("Missing fields: " + missingFields + "\n\n");
		Duration duration = new Duration(executionStart.getTime(), DateTime
				.now().getMillis());
		sb.append("Total execution duration time [mm]: "
				+ duration.getStandardMinutes());

		ISendEmail sendEmail = new AmazonSES();
		sendEmail.sendEmail(
				"MofInterfaceWS DataBase build or refresh summary report",
				"Execution summary: \n\n" + sb);

		Log.info("Execution summary: \n\n" + sb);
	}

	private void saveMonthlyResults(List<DateCoverage> dateCoverages,
			List<Fund> funds,
			List<AssetFullDetailedReport> assetFullDetailedReports,
			List<AssetMainGroupDetailedReport> assetMainGroupDetailedReports,
			List<AssetYieldsAndBalancesReport> assetYieldsAndBalancesReports) {
		Session session = null;
		Transaction tx = null;
		// we take control of the transaction cause in the first build we have a
		// mass of records
		try {
			session = HibernateSessionFactory.getSession();
			tx = session.beginTransaction();
			// tx.setTimeout(5);

			serviceHolder.fundService.saveOrUpdateAll(funds);
			/*
				wireDetailedReport(funds, assetFullDetailedReports);
				wireMainGroupReport(funds, assetMainGroupDetailedReports);
			*/
			serviceHolder.assetFullDetailedReportService
					.saveOrUpdateAll(assetFullDetailedReports);
			serviceHolder.assetMainGroupDetailedReportService
					.saveOrUpdateAll(assetMainGroupDetailedReports);
			serviceHolder.assetYieldsAndBalancesReportService
					.saveOrUpdateAll(assetYieldsAndBalancesReports);

			// won't happen in case of an error
			// mof records dateCoverages are very important
			serviceHolder.dateCoverageService.saveOrUpdateAll(dateCoverages);

			tx.commit();

		} catch (RuntimeException e) {
			try {
				tx.rollback();
				rooledback = true;
			} catch (RuntimeException rbe) {
				Log.error("Couldn’t roll back transaction", rbe);
			}
			throw e;
		} finally {
			if (session != null) {
				//session.close();
				if (rooledback)// FAIL
					for (DateCoverage dateCoverage : dateCoverages)
						sbFail.append(dateCoverage.getMonth() + "/"
								+ dateCoverage.getYear() + " "
								+ dateCoverage.getType() + "\n");

				else
					// SUCCESS
					for (DateCoverage dateCoverage : dateCoverages)
						sbSuccess.append(dateCoverage.getMonth() + "/"
								+ dateCoverage.getYear() + " "
								+ dateCoverage.getType() + "\n");

			}
		}
	}

	/*
	protected void wireMainGroupReport(List<Fund> funds,
			List<AssetMainGroupDetailedReport> assetMainGroupDetailedReports) {

		for (AssetMainGroupDetailedReport report : assetMainGroupDetailedReports) {

			if (ReportTypeCodeMapper.MAIN_GROUP_REPORT_ID_CODES.contains(report.getID_SUG_NECHES())) {
				for (Fund fund : funds) {
					if (fund.getID() == report.getID()
							&& checkPrefix(report.getReportType(),
									fund.getReportType())) {
						report.setFund(fund);
						break;
					}
				}
			}

		}

	}

	protected void wireDetailedReport(List<Fund> funds,
			List<AssetFullDetailedReport> assetFullDetailedReports) {
		for (AssetFullDetailedReport assetFullDetailedReport : assetFullDetailedReports) {

			if (ReportTypeCodeMapper.FULL_DETAILED_REPORT_ID_CODES.contains(assetFullDetailedReport
					.getID_NATUN())) {
				for (Fund fund : funds) {
					if (fund.getID() == assetFullDetailedReport.getID()
							&& checkPrefix(
									assetFullDetailedReport.getReportType(),
									fund.getReportType())) {
						assetFullDetailedReport.setFund(fund);
						break;
					}
				}
			}
		}

	}
	private boolean checkPrefix(String reportType, String fundReportType) {
		boolean samePrefix = reportType.substring(0, 5).contains(
				fundReportType.substring(0, 5));
		return samePrefix;
	}
	 */

	/**
	 * go through all the report types for some month of year
	 * and return true if and only if all of the types exist
	 */
	@SuppressWarnings("unchecked")
	private boolean fetchMonthOfYearObjects(List<DateCoverage> dateCoverages,
			List<Fund> funds,
			List<AssetFullDetailedReport> assetFullDetailedReports,
			List<AssetMainGroupDetailedReport> assetMainGroupDetailedReports,
			List<AssetYieldsAndBalancesReport> assetYieldsAndBalancesReports,
			DateTime startDate) {
		List<Fund> tempFunds;
		List<AssetFullDetailedReport> tempFullDetailedReports;
		List<AssetMainGroupDetailedReport> tempMainGroupReports;
		List<AssetYieldsAndBalancesReport> tempYieldsAndBalancesReports;

		for (ReportType reportType : ReportType.values()) {
			if (serviceHolder.dateCoverageService
					.findMonthOfYearAndType(startDate.getYear(),
							startDate.getMonthOfYear(), reportType).size() == 0) {
				try {
					if (isDetailedReport(reportType)) { // Funds

						tempFunds = (List<Fund>) getFunds(startDate, startDate,
								reportType);
						// set the month of the sampling
						for (Fund fund : tempFunds)
							fund.setHODESH_DIVUACH(startDate.toDate());

						if (tempFunds.size() > 0)
							funds.addAll(tempFunds);
						else
							return false;

					} else if (isAssetsFullDetailedReport(reportType)) { // AssetsFullDetailedReport

						tempFullDetailedReports = (List<AssetFullDetailedReport>) getFunds(
								startDate, startDate, reportType);
						// set the month of the sampling
						for (AssetFullDetailedReport report : tempFullDetailedReports)
							report.setHODESH_DIVUACH(startDate.toDate());

						if (tempFullDetailedReports.size() > 0)
							assetFullDetailedReports
									.addAll(tempFullDetailedReports);
						else
							return false;

					} else if (isAssetsMainGroupDetailedReport(reportType)) { // AssetsFullDetailedReport

						tempMainGroupReports = (List<AssetMainGroupDetailedReport>) getFunds(
								startDate, startDate, reportType);
						// set the month of the sampling
						for (AssetMainGroupDetailedReport report : tempMainGroupReports)
							report.setHODESH_DIVUACH(startDate.toDate());

						if (tempMainGroupReports.size() > 0)
							assetMainGroupDetailedReports
									.addAll(tempMainGroupReports);
						else
							return false;

					} else if (isAssetYieldsAndBalancesReport(reportType)) { // AssetsFullDetailedReport

						tempYieldsAndBalancesReports = (List<AssetYieldsAndBalancesReport>) getFunds(
								startDate, startDate, reportType);
						// set the month of the sampling
						for (AssetYieldsAndBalancesReport report : tempYieldsAndBalancesReports)
							report.setHODESH_DIVUACH(startDate.toDate());

						if (tempYieldsAndBalancesReports.size() > 0)
							assetYieldsAndBalancesReports
									.addAll(tempYieldsAndBalancesReports);
						else
							return false;
					}
				} catch (FailedToGetReportTypeException e) {
					String errMsg = "Faild to fatch data from MOF for fund: Type: "
							+ reportType
							+ ", Year: "
							+ startDate.getYear()
							+ ", Month: "
							+ startDate.getMonthOfYear()
							+ "\nError: " + e;
					sbLogger.append(errMsg + "\n");
					Log.error(errMsg);
					return false;

				} catch (IOException | FundXMLParseException
						| FundMethodReflectException
						| FundMethodInvocationException e) {
					// e.printStackTrace();
					String errMsg = e.getMessage();
					sbLogger.append(errMsg + "\n");
					Log.error(e.getMessage(), e);
					return false;
				}

				//success at getting this monthly report should be saved
				dateCoverages.add(new DateCoverage(startDate.getYear(),
						startDate.getMonthOfYear(), reportType.getValue(),
						DateTime.now().toDate()));
			}
		}
		return true;
	}

	/**
	 * retrieves all funds by startDate, endDate, reportType
	 * 
	 * @param reportType
	 * @return
	 * @throws IOException
	 * @throws FundXMLParseException
	 * @throws FundMethodReflectException
	 * @throws FundMethodInvocationException
	 * @throws FailedToGetReportTypeException
	 */
	private List<?> getFunds(DateTime startDate, DateTime endDate,
			ReportType reportType) throws IOException, FundXMLParseException,
			FundMethodReflectException, FundMethodInvocationException,
			FailedToGetReportTypeException {
		String objsXml = null;
		List<?> objects = new ArrayList<>();

		// log the process for the final email report 
		sbLogger.append("----------------------\n");
		sbLogger.append("Retrieving data: " + startDate.getMonthOfYear() + "/"
				+ endDate.getYear() + "\n");
		sbLogger.append("Type:" + reportType.getValue() + "\n");

		switch (reportType) {

		// DETAILED_REPORT REPORT
			case GEMEL_DETAILED_REPORT:
				objsXml = HttpDaoHelper.loadGemelFundsDao(startDate, endDate,
						sbLogger);
				break;
			case PENSION_DETAILED_REPORT:
				objsXml = HttpDaoHelper.loadPesionFundsDao(startDate, endDate,
						sbLogger);
				break;
			case INSURANCE_DETAILED_REPORT:
				objsXml = HttpDaoHelper.loadInsuranceFundsDao(startDate,
						endDate, sbLogger);
				break;

			// ASSETS_MAIN_GROUP_DETAILED_REPORT
			case GEMEL_ASSETS_MAIN_GROUP_DETAILED_REPORT:
				objsXml = HttpDaoHelper.loadGemelAssetsMainGroupDetailedDao(
						startDate, endDate, sbLogger);
				break;
			case PENSION_ASSETS_MAIN_GROUP_DETAILED_REPORT:
				objsXml = HttpDaoHelper.loadPensionAssetsMainGroupDetailedDao(
						startDate, endDate, sbLogger);
				break;
			case INSURANCE_ASSETS_MAIN_GROUP_DETAILED_REPORT:
				objsXml = HttpDaoHelper
						.loadInsuranceAssetsMainGroupDetailedDao(startDate,
								endDate, sbLogger);
				break;

			// ASSETS_FULL_DETAILED_REPORT
			case GEMEL_ASSETS_FULL_DETAILED_REPORT:
				objsXml = HttpDaoHelper.loadGemelAssetsFullDetailedDao(
						startDate, endDate, sbLogger);
				break;
			case PENSION_ASSETS_FULL_DETAILED_REPORT:
				objsXml = HttpDaoHelper.loadPensionAssetsFullDetailedDao(
						startDate, endDate, sbLogger);
				break;

			case INSURANCE_ASSETS_FULL_DETAILED_REPORT:
				objsXml = HttpDaoHelper.loadInsuranceAssetsFullDetailedDao(
						startDate, endDate, sbLogger);
				break;

			// ASSETS_YIELDS_AND_BALANCES_REPORT
			case GEMEL_ASSETS_YIELDS_AND_BALANCES_REPORT:
				objsXml = HttpDaoHelper
						.loadGemelAssetsYieldsAndBalancesReportsDao(startDate,
								endDate, sbLogger);
				break;
			case PENSION_ASSETS_YIELDS_AND_BALANCES_REPORT:
				objsXml = HttpDaoHelper.loadPensionAssetYieldsAndBalancesDao(
						startDate, endDate, sbLogger);
				break;

			case INSURANCE_ASSETS_YIELDS_AND_BALANCES_REPORT:
				objsXml = HttpDaoHelper.loadInsuranceAssetYieldsAndBalancesDao(
						startDate, endDate, sbLogger);
				break;

			default:
				break;

		}

		if ((isDetailedReport(reportType)) && objsXml != null) {
			objects = loadRowItemsFromXml(reportType, objsXml, Fund.class);
			FundDaoHelper.changeElementsByReportTypeFilter(reportType, objects);
		} else if ((isAssetsFullDetailedReport(reportType)) && objsXml != null) {
			objects = loadRowItemsFromXml(reportType, objsXml,
					AssetFullDetailedReport.class);
			FundDaoHelper.changeElementsByReportTypeFilter(reportType, objects);
		} else if ((isAssetsMainGroupDetailedReport(reportType))
				&& objsXml != null) {
			objects = loadRowItemsFromXml(reportType, objsXml,
					AssetMainGroupDetailedReport.class);
			FundDaoHelper.changeElementsByReportTypeFilter(reportType, objects);

		} else if ((isAssetYieldsAndBalancesReport(reportType))
				&& objsXml != null) {
			objects = loadRowItemsFromXml(reportType, objsXml,
					AssetYieldsAndBalancesReport.class);
			FundDaoHelper.changeElementsByReportTypeFilter(reportType, objects);
		}

		// close this month summary
		sbLogger.append("\n");

		return objects;
	}

	private Set<String> missingFields = new HashSet<String>();

	/**
	 * build the return List<Fund> from the retrieved XML string and XML type.
	 * 
	 * @param reportType
	 * @param xml
	 * @return
	 * @throws FundXMLParseException
	 * @throws FundMethodReflectException
	 * @throws FundMethodInvocationException
	 */
	private List<?> loadRowItemsFromXml(ReportType reportType, String xml,
			Class<?> clazz) throws FundXMLParseException,
			FundMethodReflectException, FundMethodInvocationException {
		List<Object> objsList = new ArrayList<Object>();
		InputSource is = new InputSource(new StringReader(xml));
		Document doc;
		try {
			doc = DocumentBuilderFactory.newInstance().newDocumentBuilder()
					.parse(is);
		} catch (Exception e) {
			// can be SAXException, IOException or ParserConfigurationException
			String message = "Error parsing " + reportType.getValue()
					+ " funds xml.";
			Log.error(message, e);
			throw new FundXMLParseException(e);
		}
		// raniz change 2/10/13: for education funds the interface changed to be
		// Row instead Of ROW
		NodeList rowList = null;
		if ((rowList = doc.getElementsByTagName("ROW")).getLength() == 0)
			rowList = doc.getElementsByTagName("Row");
		sbLogger.append("Rows retrived: " + rowList.getLength());
		Log.info("reportType.getValue()=" + reportType.getValue() + " Count: "
				+ rowList.getLength());
		try {
			objsList.addAll(extractObjectsFromDocument(reportType, clazz,
					rowList));
		} catch (InstantiationException | IllegalAccessException e) {
			Log.error("Faild to load objects from XML\n" + e.getMessage());
			e.printStackTrace();
		}
		return objsList;

	}

	/**
	 * extract Objects list from the Document by reportType and entity
	 * definition
	 * 
	 * @param reportType
	 *            - the report type of the document
	 * @param clazz
	 *            - the desire class of the report object presentation
	 * @param rowList
	 *            - list of nodes from the document
	 * @return extracted Objects list from the Document
	 * @throws FundMethodReflectException
	 * @throws FundMethodInvocationException
	 * @throws InstantiationException
	 * @throws IllegalAccessException
	 */
	private List<?> extractObjectsFromDocument(ReportType reportType,
			Class<?> clazz, NodeList rowList)
			throws FundMethodReflectException, FundMethodInvocationException,
			InstantiationException, IllegalAccessException {
		List<Object> objsList = new ArrayList<>();
		for (int i = 0; i < rowList.getLength(); i++) {
			Object obj = clazz.newInstance();
			for (int j = 0; j < rowList.item(i).getChildNodes().getLength(); j++) {
				Node node = rowList.item(i).getChildNodes().item(j);
				String mofAttrNodeName = node.getNodeName();
				String attrApiName = DataApiNamesMap.getApiNameByAttribute(
						reportType.getValue(), mofAttrNodeName);
				if (attrApiName == null) {

					missingFields.add(reportType.getValue() + ","
							+ mofAttrNodeName);
					if (!(mofAttrNodeName.equals("#text") || node
							.getTextContent().equals("")))
						Log.info("Missing attribute handling for fund of type: "
								+ reportType.getValue()
								+ "\nAttribute name:  "
								+ mofAttrNodeName
								+ "\nAttribute value: "
								+ node.getTextContent());
					else {
						// Log.info("Skipping blanks");
					}
					continue;
				}
				String attrValue = node.getTextContent();
				Method getMethod = null, setMethod = null;
				Class<?> retDataType;
				try {
					getMethod = clazz.getMethod("get" + attrApiName,
							(Class[]) null);
					retDataType = getMethod.getReturnType();
					setMethod = clazz.getMethod("set" + attrApiName,
							retDataType);
				} catch (Exception e) {
					// can be SecurityException or NoSuchMethodException
					String message = "Error in finding a get or set Fund method. Either "
							+ (getMethod != null ? getMethod.getName() : "get"
									+ mofAttrNodeName)
							+ " or "
							+ (setMethod != null ? setMethod.getName() : "set"
									+ mofAttrNodeName);
					Log.error(message, e);
					throw new FundMethodReflectException(message, e);
				}

				// attribute value is always string
				try {
					if (!FUND_MOF_DATE_FIELDS.contains(attrApiName)) {
						if (retDataType.getName().equals("java.lang.Double"))
							setMethod
									.invoke(obj,
											attrValue == null
													|| attrValue.equals("") ? null
													: Double.valueOf(attrValue));
						else if (retDataType.getName().equals(
								"java.lang.Integer"))
							setMethod
									.invoke(obj,
											attrValue == null
													|| attrValue.equals("") ? null
													: Integer
															.valueOf(attrValue));
						else
							setMethod.invoke(obj, attrValue);
					} else { // switching Mof date string format to Date object
						SimpleDateFormat mofDateFormat = new SimpleDateFormat(
								"dd/MM/yyyy");
						try {
							if (attrApiName.equals("TAARICH_HAFAKAT_HADOCH"))
								mofDateFormat = new SimpleDateFormat(
										"MM/dd/yyyy");
							Date date = attrValue == null
									|| attrValue.equals("") ? null
									: mofDateFormat.parse(attrValue);
							setMethod.invoke(obj, date);

						} catch (ParseException e) {
							e.printStackTrace();
						}
					}
				} catch (Exception e) {
					// can be IllegalArgumentException, IllegalAccessException
					// or InvocationTargetException
					String message = "Exception invoking a method "
							+ setMethod.getName();
					Log.error(message, e);
					throw new FundMethodInvocationException(message, e);
				}
			}
			tweakObjectTypeFromDocument(reportType, obj);
			objsList.add(obj);
		}
		return objsList;
	}

	/**
	 * set the ReportType/UpdatedDate/setCreatedDate of the newly persistence
	 * object
	 * 
	 * @param reportType
	 * @param obj
	 */
	private void tweakObjectTypeFromDocument(ReportType reportType, Object obj) {
		if (obj instanceof Fund) {
			Fund fund = (Fund) obj;

			if (reportType == ReportType.GEMEL_DETAILED_REPORT
					&& fund.getMISPAR_KUPA_AV() != null
					&& !fund.getMISPAR_KUPA_AV().equals("0")) {
				if (!fund.getMISPAR_KUPA_AV().equals("1463")) {
					//Fund parentFund = new Fund();
					//parentFund.setID(Integer.valueOf(fund.getMISPAR_KUPA_AV()));
				}
			}

			if (reportType == ReportType.INSURANCE_DETAILED_REPORT)
				fund.setSUG(fund.getTKUFAT_HAKAMA());

			fund.setReportType(reportType.getValue());
			fund.setUpdatedDate(DateTime.now().toDate());
			fund.setCreatedDate(DateTime.now().toDate());

		} else if (obj instanceof AssetFullDetailedReport) {
			AssetFullDetailedReport report = (AssetFullDetailedReport) obj;

			report.setReportType(reportType.getValue());
			report.setUpdatedDate(DateTime.now().toDate());
			report.setCreatedDate(DateTime.now().toDate());

		} else if (obj instanceof AssetMainGroupDetailedReport) {
			AssetMainGroupDetailedReport report = (AssetMainGroupDetailedReport) obj;

			report.setReportType(reportType.getValue());
			report.setUpdatedDate(DateTime.now().toDate());
			report.setCreatedDate(DateTime.now().toDate());

		} else if (obj instanceof AssetYieldsAndBalancesReport) {
			AssetYieldsAndBalancesReport report = (AssetYieldsAndBalancesReport) obj;

			report.setReportType(reportType.getValue());
			report.setUpdatedDate(DateTime.now().toDate());
			report.setCreatedDate(DateTime.now().toDate());

		}
	}

	/**
	 * isDetailedReport group
	 * 
	 * @param reportType
	 * @return
	 */
	public static boolean isDetailedReport(ReportType reportType) {
		return reportType == ReportType.GEMEL_DETAILED_REPORT
				|| reportType == ReportType.PENSION_DETAILED_REPORT
				|| reportType == ReportType.INSURANCE_DETAILED_REPORT;
	}

	/**
	 * isAssetsFullDetailedReport group
	 * 
	 * @param reportType
	 * @return
	 */
	public static boolean isAssetsFullDetailedReport(ReportType reportType) {
		return reportType == ReportType.GEMEL_ASSETS_FULL_DETAILED_REPORT
				|| reportType == ReportType.PENSION_ASSETS_FULL_DETAILED_REPORT
				|| reportType == ReportType.INSURANCE_ASSETS_FULL_DETAILED_REPORT;
	}

	/**
	 * isAssetsMainGroupDetailedReport group
	 * 
	 * @param reportType
	 * @return
	 */
	public static boolean isAssetsMainGroupDetailedReport(ReportType reportType) {
		return reportType == ReportType.PENSION_ASSETS_MAIN_GROUP_DETAILED_REPORT
				|| reportType == ReportType.GEMEL_ASSETS_MAIN_GROUP_DETAILED_REPORT
				|| reportType == ReportType.INSURANCE_ASSETS_MAIN_GROUP_DETAILED_REPORT;
	}

	/**
	 * isAssetsMainGroupDetailedReport group
	 * 
	 * @param reportType
	 * @return
	 */
	public static boolean isAssetYieldsAndBalancesReport(ReportType reportType) {
		return reportType == ReportType.PENSION_ASSETS_YIELDS_AND_BALANCES_REPORT
				|| reportType == ReportType.GEMEL_ASSETS_YIELDS_AND_BALANCES_REPORT
				|| reportType == ReportType.INSURANCE_ASSETS_YIELDS_AND_BALANCES_REPORT;
	}

	public static void main(String[] args) {
		DataBaseBuilder.execute(1);
	}
}
