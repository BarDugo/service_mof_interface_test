package com.novidea.mof;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

public class Log {
	private static Logger log = Logger.getLogger("org.novidea.mof");

	public static boolean isTrace() {
		return log.getLevel() == Level.TRACE;
	}

	public static void trace(String message) {
		log.trace(message);
	}

	public static void debug(String message) {
		log.debug(message);
	}

	public static void warn(String message) {
		log.warn(message);
	}

	public static void info(String message) {
		log.info(message);
	}

	public static void error(String message) {
		log.error(message);
	}

	public static void error(String message, Throwable t) {
		log.error(message, t);
	}
}
