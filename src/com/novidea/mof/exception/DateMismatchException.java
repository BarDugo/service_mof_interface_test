package com.novidea.mof.exception;
/**
 * 
 * @author Galil.Zussman
 * @date Dec 16, 2014
 */
public class DateMismatchException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public DateMismatchException() {
		super();
	}

	public DateMismatchException(String message, Throwable cause,
			boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	public DateMismatchException(String message, Throwable cause) {
		super(message, cause);
	}

	public DateMismatchException(String message) {
		super(message);
	}

	public DateMismatchException(Throwable cause) {
		super(cause);
	}


}
