package com.novidea.mof.exception;

/**
 * 
 * @author Galil.Zussman
 * @date 05.08.2014
 */
public class FailedToGetReportTypeException extends Exception {

	private static final long serialVersionUID = 1L;

	public FailedToGetReportTypeException() {
		super();
	}

	public FailedToGetReportTypeException(String message) {
		super(message);
	}

	public FailedToGetReportTypeException(Throwable cause) {
		super(cause);
	}

	public FailedToGetReportTypeException(String message, Throwable cause) {
		super(message, cause);
	}

	public FailedToGetReportTypeException(String message, Throwable cause,
			boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

}
