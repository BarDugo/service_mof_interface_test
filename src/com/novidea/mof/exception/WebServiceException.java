package com.novidea.mof.exception;

public class WebServiceException extends Exception {

	private static final long serialVersionUID = -2950873837643224502L;

	public WebServiceException() {
	}

	public WebServiceException(String message) {
		super(message);
	}

	public WebServiceException(Throwable cause) {
		super(cause);
	}

	public WebServiceException(String message, Throwable cause) {
		super(message, cause);
	}

}
