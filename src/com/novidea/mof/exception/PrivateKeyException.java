package com.novidea.mof.exception;

public class PrivateKeyException extends Exception {

 	private static final long serialVersionUID = -5919458496580559657L;

	public PrivateKeyException() {

	}

	public PrivateKeyException(String message) {
		super(message);

	}

	public PrivateKeyException(Throwable cause) {
		super(cause);

	}

	public PrivateKeyException(String message, Throwable cause) {
		super(message, cause);

	}

}
