package com.novidea.mof.exception;

/**
 * @author Galil.Zussman
 * @date Sep 15, 2014
 */

public class PrematureEOFException extends Exception {

	private static final long serialVersionUID = 8672978273176837821L;

	public PrematureEOFException() {

	}

	public PrematureEOFException(String message) {
		super(message);

	}

	public PrematureEOFException(Throwable cause) {
		super(cause);

	}

	public PrematureEOFException(String message, Throwable cause) {
		super(message, cause);

	}

}
