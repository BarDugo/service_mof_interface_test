package com.novidea.mof.exception;

public class FundMethodInvocationException extends Exception {

	private static final long serialVersionUID = -8757139613123795441L;

	public FundMethodInvocationException() {
		
	}

	public FundMethodInvocationException(String message) {
		super(message);
		
	}

	public FundMethodInvocationException(Throwable cause) {
		super(cause);
		
	}

	public FundMethodInvocationException(String message, Throwable cause) {
		super(message, cause);
		
	}

}
