package com.novidea.mof.exception;

public class FundMethodReflectException extends Exception {

	private static final long serialVersionUID = -5957229393815115423L;

	public FundMethodReflectException() {
		
	}

	public FundMethodReflectException(String message) {
		super(message);
		
	}

	public FundMethodReflectException(Throwable cause) {
		super(cause);
		
	}

	public FundMethodReflectException(String message, Throwable cause) {
		super(message, cause);
		
	}

}
