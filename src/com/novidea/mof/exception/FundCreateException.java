package com.novidea.mof.exception;

public class FundCreateException extends Exception {

	private static final long serialVersionUID = 8672978273176837821L;

	public FundCreateException() {
		
	}

	public FundCreateException(String message) {
		super(message);
		
	}

	public FundCreateException(Throwable cause) {
		super(cause);
		
	}

	public FundCreateException(String message, Throwable cause) {
		super(message, cause);
		
	}

}
