package com.novidea.mof.exception;
/**
 * 
 * @author Galil.Zussman
 * @date Oct 22, 2014
 */
public class IdMismatchException extends RuntimeException {

	public IdMismatchException() {
		super();
	}

	public IdMismatchException(String message, Throwable cause,
			boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	public IdMismatchException(String message, Throwable cause) {
		super(message, cause);
	}

	public IdMismatchException(String message) {
		super(message);
	}

	public IdMismatchException(Throwable cause) {
		super(cause);
	}

	private static final long serialVersionUID = 1L;

}
