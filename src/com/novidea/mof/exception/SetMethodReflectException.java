package com.novidea.mof.exception;

public class SetMethodReflectException extends FundMethodReflectException {


	private static final long serialVersionUID = -7435488238311100463L;

	public SetMethodReflectException() {
	}

	public SetMethodReflectException(String message) {
		super(message);
	}

	public SetMethodReflectException(Throwable cause) {
		super(cause);
	}

	public SetMethodReflectException(String message, Throwable cause) {
		super(message, cause);
	}

}
