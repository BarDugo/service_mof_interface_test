package com.novidea.mof.exception;

public class DBCommunicationException extends Exception {

	private static final long serialVersionUID = 363692535493827430L;

	public DBCommunicationException() {
	}

	public DBCommunicationException(String message) {
		super(message);
	}

	public DBCommunicationException(Throwable cause) {
		super(cause);
	}

	public DBCommunicationException(String message, Throwable cause) {
		super(message, cause);
	}

}
