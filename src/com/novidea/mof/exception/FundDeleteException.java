package com.novidea.mof.exception;

public class FundDeleteException extends Exception {


	private static final long serialVersionUID = 213430638420998734L;

	public FundDeleteException() {
		
	}

	public FundDeleteException(String message) {
		super(message);
		
	}

	public FundDeleteException(Throwable cause) {
		super(cause);
		
	}

	public FundDeleteException(String message, Throwable cause) {
		super(message, cause);
		
	}

}
