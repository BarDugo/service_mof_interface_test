package com.novidea.mof.exception;

public class FundXMLParseException extends Exception {

	private static final long serialVersionUID = 2875723951231657401L;

	public FundXMLParseException(String message, Throwable cause) {
		super(message, cause);
	}

	public FundXMLParseException(String message) {
		super(message);
	}

	public FundXMLParseException(Throwable cause) {
		super(cause);
	}

	public FundXMLParseException() {
	}

}
