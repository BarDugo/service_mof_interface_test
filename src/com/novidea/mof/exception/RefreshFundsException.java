package com.novidea.mof.exception;

public class RefreshFundsException extends Exception {

	private static final long serialVersionUID = 4790085288510447719L;

	public RefreshFundsException() {
		
	}

	public RefreshFundsException(String message) {
		super(message);
		
	}

	public RefreshFundsException(Throwable cause) {
		super(cause);
		
	}

	public RefreshFundsException(String message, Throwable cause) {
		super(message, cause);
		
	}

}
