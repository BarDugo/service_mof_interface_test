package com.novidea.mof.view;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;

import org.joda.time.DateTime;

import com.novidea.mof.MofProperties;
import com.novidea.mof.entities.model.Fund;
import com.novidea.mof.exception.RefreshFundsException;
import com.novidea.mof.ws.core.MofInterface;
import com.novidea.mof.ws.core.MofServiceImpl;

/**
 * @author Galil.Zussman
 * @date Aug 29, 2014
 */
@ManagedBean
@RequestScoped
public class MofBean implements Serializable {

	private static final long serialVersionUID = 1L;

	public class MustSetwhichPageParamException extends Exception {
		private static final long serialVersionUID = 1L;

		MustSetwhichPageParamException(String msg) {
			super(msg);
		}
	}

	public final static Boolean DEBUG = true;

	private final static String MAX_RECORDS = "50000";
	private final static String PENSION_TYPE = "PensionDetailedReport";
	private final static String GEMEL_TYPE = "GemelDetailedReport";
	private final static String INSURANCE_TYPE = "InsuranceDetailedReport";

	private static MofInterface mofInterface = new MofServiceImpl();

	public String getDebugQuery() {
		return debugQuery;
	}

	public void setDebugQuery(String debugQuery) {
		this.debugQuery = debugQuery;
	}

	public MofCalcData getControllerData() {
		return controllerData;
	}

	public void setControllerData(MofCalcData controllerData) {
		this.controllerData = controllerData;
	}

	private String debugQuery = "";

	/** dynamicComponent section */
	private MofCalcData controllerData = new MofCalcData();

	public MofBean() throws MustSetwhichPageParamException {
		init();
	}

	void init() throws MustSetwhichPageParamException {

		controllerData
				.setWhichPage(FacesContext.getCurrentInstance()
						.getExternalContext().getRequestParameterMap()
						.get("whichPage"));
		controllerData.setFillterStartDate(DateTime.now().dayOfMonth()
				.withMinimumValue().minusYears(1).minusMonths(1)
				.withTime(0, 0, 0, 0).toDate());
		controllerData.setFillterEndDate(DateTime.now().dayOfMonth()
				.withMinimumValue().withTime(0, 0, 0, 0).minusMonths(2)
				.toDate());
		controllerData.setQueryString(buildQuery(controllerData.getWhichPage(),
				controllerData.getFillterStartDate(),
				controllerData.getFillterEndDate(), ""));
		if (isPension()) {
			List<Fund> rtnfunds = mofInterface
					.getFundsByNativeQuery("SELECT DISTINCT SUG FROM Fund where ReportType='"
							+ PENSION_TYPE + "';");
			initSugOptionsByFillterdList(rtnfunds, "הכל");
		} else if (isGemel()) {
			List<Fund> rtnfunds = mofInterface
					.getFundsByNativeQuery("SELECT DISTINCT SUG FROM Fund where ReportType=\""
							+ GEMEL_TYPE + "\";");
			initSugOptionsByFillterdList(rtnfunds, "הכל");
		} else if (isInsurance()) {
			controllerData.setMainDatePeriodRadio(36);
		} else if (controllerData.getWhichPage() != null)
			throw new MustSetwhichPageParamException(
					"whichPage param must be set - Pension, Gemel, Insurance");

		executeCustomQuery();

	}

	void initSugOptionsByFillterdList(List<Fund> sugReturnedFunds,
			String firstValue) {
		controllerData.setSugOptions(new ArrayList<SelectItem>());
		if (firstValue != null)
			controllerData.getSugOptions().add(new SelectItem("0", firstValue));
		for (Fund fund : sugReturnedFunds) {
			controllerData.getSugOptions().add(
					new SelectItem(fund.getSUG(), fund.getSUG()));
		}
	}

	/**
	 * native SQL filtered custom query for the general view of the data table
	 */
	public void executeCustomQuery() {
		// String jsonStr =
		// getJSONStringFromParts(MofWebService.port.getFundsByNativeQueryPartsJSON(queryString));
		// // PARTS
		rebuildFilteredQueryString();
		if (isPension()) {
			executePensionCustomQuery();
		} else if (isGemel()) {
			executeGemelCustomQuery();
		} else if (isInsurance()) {
			executeInsuranceCustomQuery();
		}
		// controllerData.dataJson =
		// MofWebService.port.getFundsByNativeQueryJSON(controllerData.queryString);
	}

	public void doThat() {
		try {
			mofInterface.refreshFunds(1, MofProperties.get("PRIVATE_KEY"));
		} catch (RefreshFundsException e) {
			e.printStackTrace();
		}
	}

	/**
	 * native SQL filtered custom query for "Gemel" funds
	 */
	public void executeGemelCustomQuery() {
		controllerData
				.setDataJson(mofInterface
						.getGemelFundsByNativeQueryJSON(controllerData
								.getQueryString()));
	}

	/**
	 * native SQL filtered custom query for "Pension" funds
	 */
	public void executePensionCustomQuery() {
		controllerData.setDataJson(mofInterface
				.getPensionFundsByNativeQueryJSON(controllerData
						.getQueryString()));
	}

	/**
	 * native SQL filtered custom query for "Insurance" funds
	 */
	public void executeInsuranceCustomQuery() {
		controllerData.setDataJson(mofInterface
				.getInsuranceFundsByNativeQueryJSON(controllerData
						.getQueryString()));
	}

	/**
	 * the SQL custom query string for the funds, selecting only needed fields
	 */
	private String buildQuery(String ReportType, Date startDate, Date endDate,
			String conditions) {
		return "SELECT ID,HODESH_DIVUACH,SHEM,SUG,SHM_TAAGID_SHOLET,TZVIRA_NETO,SHIUR_DMEI_NIHUL_AHARON,"
				+ "TSUA_HODSHIT,SHARP_ALL,YITRAT_NCHASIM_LSOF_TKUFA,TAARICH_HAKAMA,SHARP_RIBIT_HASRAT_SIKUN "
				+ "FROM Fund WHERE ReportType = '"
				+ ReportType
				+ "' "
				+ "AND ( HODESH_DIVUACH BETWEEN '"
				+ startDate
				+ "' AND '"
				+ endDate
				+ "') "
				+ conditions
				+ "ORDER BY ID ASC ,HODESH_DIVUACH ASC LIMIT 0,"
				+ MAX_RECORDS
				+ ";";
	}

	private void rebuildFilteredQueryString() {
		String conditions = "";
		if (controllerData.getSelectedSugOption() != null
				&& controllerData.getSelectedSugOption() != "0")
			conditions += " AND SUG = '"
					+ controllerData.getSelectedSugOption() + "' ";
		if (controllerData.getMainDatePeriodRadio() == 0
				&& controllerData.getSubDatePeriodRadio() == 1) {// filter by
																	// date
			// range
			controllerData
					.setFillterStartDate((new DateTime(Integer.valueOf(
							controllerData.getSelectedFromYearOption())
							.intValue(), Integer.valueOf(
							controllerData.getSelectedFromMonthOption())
							.intValue(), 1, 0, 0)).toDate());
			controllerData.setFillterEndDate((new DateTime(Integer.valueOf(
					controllerData.getSelectedToYearOption()).intValue(),
					Integer.valueOf(controllerData.getSelectedToMonthOption())
							.intValue(), 1, 0, 0)).toDate());
		}
		controllerData.setQueryString(buildQuery(controllerData.getWhichPage(),
				controllerData.getFillterStartDate(),
				controllerData.getFillterEndDate(), conditions));
	}

	public MofBean getThis() {
		return this;
	}

	public void selectChanged() {
		rebuildFilteredQueryString();
		executeCustomQuery();
	}

	public Boolean getDebug() {
		return DEBUG;
	}

	/**
	 * @param parts
	 *            - Array of the splitted JSON string
	 * @return JSON string
	 */
	@SuppressWarnings("unused")
	private String getJSONStringFromParts(String[] parts) {
		String str = "";
		for (String part : parts) {
			str += part;
		}
		return str;
	}

	public boolean isPension() {
		return controllerData.getWhichPage().equals(PENSION_TYPE);
	}

	public boolean isGemel() {
		return controllerData.getWhichPage().equals(GEMEL_TYPE);
	}

	public boolean isInsurance() {
		return controllerData.getWhichPage().equals(INSURANCE_TYPE);
	}

}
