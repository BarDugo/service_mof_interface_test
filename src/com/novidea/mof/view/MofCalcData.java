package com.novidea.mof.view;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.faces.model.SelectItem;

import org.joda.time.DateTime;

import com.novidea.mof.entities.model.Fund;

/**
 * @author Galil.Zussman
 * @date Aug 29, 2014
 */

public class MofCalcData {
	private final static String[] MONTHES = new String[] { "ינואר", "פברואר",
			"מרץ", "אפריל", "מאי", "יוני", "יולי", "ספטמבר", "אוקטובר",
			"נובמבר", "דצמבר" };
	private final static Integer YEARS_BACK = 7;
	private final static List<SelectItem> monthOptions;
	private final static List<SelectItem> yearOptions;
	private final static List<SelectItem> yearReversOptions;

	static {
		monthOptions = new ArrayList<SelectItem>();
		yearOptions = new ArrayList<SelectItem>();
		yearReversOptions = new ArrayList<SelectItem>();
		for (Integer i = 0; i < MONTHES.length; i++)
			monthOptions.add(new SelectItem(String.valueOf(i + 1), MONTHES[i]));
		for (Integer j = DateTime.now().getYear(), i = DateTime.now().getYear()
				- YEARS_BACK + 1; i <= DateTime.now().getYear(); i++) {
			yearOptions
					.add(new SelectItem(String.valueOf(i), String.valueOf(i)));
			yearReversOptions.add(new SelectItem(String.valueOf(j), String
					.valueOf(j)));
			j--;
		}
	}
	private String queryString;
	private String dataJson;
	private List<Fund> funds;
	private String whichPage;

	private List<SelectItem> sugOptions;
	private String selectedSugOption;

	/* 2nd filtter set */
	private String selectedInvestOption;
	private String selectedTypeRatingOption;

	/* 3rd filtter set */
	private Integer mainDatePeriodRadio;
	private Integer subDatePeriodRadio;
	private String selectedFromMonthOption;
	private String selectedFromYearOption;
	private String selectedToMonthOption;
	private String selectedToYearOption;

	private String[] selectedYearPeriods;
	private Integer selectedMonthPeriod;
	private Integer otherMonthPeriod;
	private Date fillterStartDate;
	private Date fillterEndDate;

	public MofCalcData() {

		selectedFromMonthOption = String.valueOf(DateTime.now()
				.getMonthOfYear() - 1);
		selectedFromYearOption = String.valueOf(DateTime.now().getYear() - 1);
		selectedToMonthOption = String
				.valueOf(DateTime.now().getMonthOfYear() - 1);
		selectedToYearOption = String.valueOf(DateTime.now().getYear());
		selectedMonthPeriod = 12;
		mainDatePeriodRadio = 12;
		selectedSugOption = "0";
	}

	public String getQueryString() {
		return queryString;
	}

	public void setQueryString(String queryString) {
		this.queryString = queryString;
	}

	public String getDataJson() {
		return dataJson;
	}

	public void setDataJson(String dataJson) {
		this.dataJson = dataJson;
	}

	public List<Fund> getFunds() {
		return funds;
	}

	public void setFunds(List<Fund> funds) {
		this.funds = funds;
	}

	public String getWhichPage() {
		return whichPage;
	}

	public void setWhichPage(String whichPage) {
		this.whichPage = whichPage;
	}

	public List<SelectItem> getSugOptions() {
		return sugOptions;
	}

	public void setSugOptions(List<SelectItem> sugOptions) {
		this.sugOptions = sugOptions;
	}

	public String getSelectedSugOption() {
		return selectedSugOption;
	}

	public void setSelectedSugOption(String selectedSugOption) {
		this.selectedSugOption = selectedSugOption;
	}

	public String getSelectedInvestOption() {
		return selectedInvestOption;
	}

	public void setSelectedInvestOption(String selectedInvestOption) {
		this.selectedInvestOption = selectedInvestOption;
	}

	public String getSelectedTypeRatingOption() {
		return selectedTypeRatingOption;
	}

	public void setSelectedTypeRatingOption(String selectedTypeRatingOption) {
		this.selectedTypeRatingOption = selectedTypeRatingOption;
	}

	public Integer getMainDatePeriodRadio() {
		return mainDatePeriodRadio;
	}

	public void setMainDatePeriodRadio(Integer mainDatePeriodRadio) {
		this.mainDatePeriodRadio = mainDatePeriodRadio;
	}

	public Integer getSubDatePeriodRadio() {
		return subDatePeriodRadio;
	}

	public void setSubDatePeriodRadio(Integer subDatePeriodRadio) {
		this.subDatePeriodRadio = subDatePeriodRadio;
	}

	public String getSelectedFromMonthOption() {
		return selectedFromMonthOption;
	}

	public void setSelectedFromMonthOption(String selectedFromMonthOption) {
		this.selectedFromMonthOption = selectedFromMonthOption;
	}

	public String getSelectedFromYearOption() {
		return selectedFromYearOption;
	}

	public void setSelectedFromYearOption(String selectedFromYearOption) {
		this.selectedFromYearOption = selectedFromYearOption;
	}

	public String getSelectedToMonthOption() {
		return selectedToMonthOption;
	}

	public void setSelectedToMonthOption(String selectedToMonthOption) {
		this.selectedToMonthOption = selectedToMonthOption;
	}

	public String getSelectedToYearOption() {
		return selectedToYearOption;
	}

	public void setSelectedToYearOption(String selectedToYearOption) {
		this.selectedToYearOption = selectedToYearOption;
	}

	public String[] getSelectedYearPeriods() {
		return selectedYearPeriods;
	}

	public void setSelectedYearPeriods(String[] selectedYearPeriods) {
		this.selectedYearPeriods = selectedYearPeriods;
	}

	public Integer getSelectedMonthPeriod() {
		return selectedMonthPeriod;
	}

	public void setSelectedMonthPeriod(Integer selectedMonthPeriod) {
		this.selectedMonthPeriod = selectedMonthPeriod;
	}

	public Integer getOtherMonthPeriod() {
		return otherMonthPeriod;
	}

	public void setOtherMonthPeriod(Integer otherMonthPeriod) {
		this.otherMonthPeriod = otherMonthPeriod;
	}

	public Date getFillterStartDate() {
		return fillterStartDate;
	}

	public void setFillterStartDate(Date fillterStartDate) {
		this.fillterStartDate = fillterStartDate;
	}

	public Date getFillterEndDate() {
		return fillterEndDate;
	}

	public void setFillterEndDate(Date fillterEndDate) {
		this.fillterEndDate = fillterEndDate;
	}

	public static String[] getMonthes() {
		return MONTHES;
	}

	public static Integer getYearsBack() {
		return YEARS_BACK;
	}

	public static List<SelectItem> getMonthoptions() {
		return monthOptions;
	}

	public static List<SelectItem> getYearoptions() {
		return yearOptions;
	}

	public static List<SelectItem> getYearreversoptions() {
		return yearReversOptions;
	}
}
