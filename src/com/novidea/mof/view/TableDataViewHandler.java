package com.novidea.mof.view;

import java.io.Serializable;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;

import org.joda.time.DateTime;

import com.google.gson.Gson;
import com.novidea.mof.entities.model.Fund;
import com.novidea.mof.entities.service.ServiceHolder;

/**
 * 
 * @author Galil.Zussman
 * @date May 18, 2014
 */
@ManagedBean
@RequestScoped
public class TableDataViewHandler implements Serializable {

	private static final long serialVersionUID = 1L;
	ServiceHolder serviceHolder = ServiceHolder.getInstance();
	private List<Fund> funds;
	private String queryString = "SELECT * from Fund LIMIT 0,1000;";
	private DateTime startDate;
	@SuppressWarnings("unused")
	private DateTime endDate;

	// Constructor
	public TableDataViewHandler() {
		this.funds = serviceHolder.fundService
				.executeNativeSqlCustomQuery(queryString);
		// this.projectedDataModels = serviceHolder.fundService.flattenAll();

		this.startDate = DateTime.now().minusYears(1)
				.minusMonths(DateTime.now().getMonthOfYear() - 1)
				.minusDays((DateTime.now().getDayOfMonth() - 1))
				.withTimeAtStartOfDay();
		this.endDate = startDate.plusYears(1);

	}

	public String generateReport() {
		// this.projectedDataModels =
		// serviceHolder.fundService.getReportBetweenDates(
		// startDate.toDate(), endDate.toDate());
		return null;
	}

	public String executeCustomQuery() {
		try {
			this.funds = serviceHolder.fundService
					.executeNativeSqlCustomQuery(queryString);
		} catch (Exception exception) {
			FacesContext context = FacesContext.getCurrentInstance();

			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR, "Error - executeCustomQuery",
					exception.getMessage()));

		}
		return null;

	}


	public String getDataJson() {
		ViewHalper.resetNullFieldsFunds(funds);

		String jsonStr = new Gson().toJson(funds);
		return jsonStr;
	}

	public String getQueryString() {
		return queryString;
	}

	public void setQueryString(String queryString) {
		this.queryString = queryString;
	}

	/**
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		TableDataViewHandler dataViewHandler = new TableDataViewHandler();
		dataViewHandler.getDataJson();
	}
}
