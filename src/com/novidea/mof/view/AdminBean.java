package com.novidea.mof.view;

import java.io.Serializable;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import com.novidea.mof.DataBaseBuilder;
import com.novidea.mof.MofProperties;
import com.novidea.mof.exception.RefreshFundsException;
import com.novidea.mof.ws.MofInterfaceExpose;
import com.novidea.mof.ws.core.MofServiceImpl;

/**
 * @author Galil.Zussman
 * @date Sep 02, 2014
 */
@ManagedBean
@SessionScoped
public class AdminBean implements Serializable {

	private static final long serialVersionUID = 1L;
	private static MofInterfaceExpose mofInterface = new MofServiceImpl();

	public AdminBean() {
	}

	public void doThat() {
		try {
			mofInterface.refreshFunds(1,MofProperties.get("PRIVATE_KEY"));
		} catch (RefreshFundsException e) {
			e.printStackTrace();
		}
	}

	public void test() {
		DataBaseBuilder.func();
		// ReportHolder reportHolder .assetFullDetailedReports.get(0);
		// AssetFullDetailedReport assetFullDetailedReport = ServiceFactory
		// 		.getInstance().assetFullDetailedReportService.findById(23l);
		// Fund fundd = ServiceFactory.getInstance().fundService.findById(914l);
		// System.out.print(assetFullDetailedReport);
		// System.out.print(fundd);
		// System.out.print(fundd.toString());
		// for (AssetFullDetailedReport iterable_element : fundd
		// 		.getAssetFullDetailedReports()) {
		// 	iterable_element.getID();
		// }
	}

}
