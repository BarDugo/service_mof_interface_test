package com.novidea.mof.view;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.List;

import com.novidea.mof.entities.model.Fund;

/**
 * ViewHalper.java
 * 
 * @author Galil.Zussman
 * @date Jun 1, 2014
 * 
 */
public class ViewHalper {

	public static void resetNullFieldsFunds(List<Fund> funds) {
		// reset null String fields to ""
		for (Fund fund : funds) {
			for (Method setMethod : Fund.class.getMethods())
				if (setMethod.getName().startsWith("set")
						&& setMethod.getParameterTypes().length == 1)
					try {
						Method getMethod = Fund.class.getMethod("get"
								+ setMethod.getName().substring(3,
										setMethod.getName().length()));
						Object object = getMethod.invoke(fund);
						if (object == null)
							setMethod.invoke(fund, "");
					} catch (IllegalAccessException | IllegalArgumentException
							| InvocationTargetException | NoSuchMethodException
							| SecurityException e) {
						e.printStackTrace();
					}
		}
	}

}
