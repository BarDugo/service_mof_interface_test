package com.novidea.mof.hibernate;

import java.io.Serializable;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.hibernate.Criteria;
import org.hibernate.LockOptions;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Galil.Zussman
 * @date 20 May 2014
 */
public abstract class AbstractHibernateDAO<T extends Serializable> {

	protected static Logger log;

	private final Class<T> clazz;


	public AbstractHibernateDAO(final Class<T> clazzToSet) {
		this.clazz = clazzToSet;
		log = LoggerFactory.getLogger(clazz);
	}

	public final Session getSession() {
		return HibernateSessionFactory.getSession();
	}

	@SuppressWarnings("unchecked")
	public T findById(final Long id) {
		Transaction tx = getSession().beginTransaction();
		T type = (T) this.getSession().get(this.clazz, id);
		tx.commit();
		return type;
	}

	@SuppressWarnings("unchecked")
	public List<T> findAll() {
		Transaction tx = getSession().beginTransaction();
		List<T> list = (List<T>) this.getSession()
				.createQuery("from " + this.clazz.getName()).list();
		tx.commit();
		return list;
	}

	@SuppressWarnings("unchecked")
	public List<T> findAll(int offset, int rows) {
		Transaction tx = getSession().beginTransaction();
		List<T> list = (List<T>) this
				.getSession()
				.createQuery(
						"from " + this.clazz.getName() + " LIMIT " + offset
								+ "," + rows).list();
		tx.commit();
		return list;
	}

	/*
	 * @SuppressWarnings("unchecked") public List<T> findRowsBetween(int offset,
	 * int numOfRows) { List<T> list = (List<T>) this .getSession()
	 * .createQuery( "from " + this.clazz.getName() + " ORDER BY ID OFFSET " +
	 * offset + " ROWS FETCH NEXT " + numOfRows + " ROWS ONLY ").list(); return
	 * list; }
	 */
	public void create(T entity) {
		log.debug("create" + this.clazz.getName() + " instance");
		try {
			Transaction tx = getSession().beginTransaction();
			this.getSession().persist(entity);
			tx.commit();
			log.info("create successful");
		} catch (RuntimeException re) {
			log.error("create failed", re);
			throw re;
		}
	}

	public void update(T entity) {
		log.debug("update " + this.clazz.getName() + " instance");
		try {
			Transaction tx = getSession().beginTransaction();
			this.getSession().update(entity);
			tx.commit();
			log.debug("update successful");
		} catch (RuntimeException re) {
			log.error("update failed", re);
			throw re;
		}
	}

	@SuppressWarnings("unchecked")
	public T merge(T entity) {
		log.debug("merge " + this.clazz.getName() + " instance");
		try {
			Transaction tx = getSession().beginTransaction();
			T t = (T) this.getSession().merge(entity);
			tx.commit();
			log.debug("merge successful");
			return t;
		} catch (RuntimeException re) {
			log.error("merge failed", re);
			throw re;
		}
	}

	public void delete(T entity) {
		log.debug("delete " + this.clazz.getName() + " instance");
		try {
			Transaction tx = getSession().beginTransaction();
			this.getSession().delete(entity);
			tx.commit();
			log.debug("delete successful");
		} catch (RuntimeException re) {
			log.error("delete failed", re);
			throw re;
		}
	}

	public void save(T entity) {
		log.debug("saving " + this.clazz.getName() + " instance");
		try {
			Transaction tx = getSession().beginTransaction();
			this.getSession().save(entity);
			tx.commit();
			log.debug("save successful");
		} catch (RuntimeException re) {
			log.error("save failed", re);
			throw re;
		}
	}

	public void deleteById(Long entityId) {
		// NOT in use
		// final T entity = this.findById(entityId);
		// this.delete(entity);
	}

	public void saveOrUpdate(T entity) {
		log.debug("saveOrUpdate " + this.clazz.getName() + " instance");
		try {
			Transaction tx = getSession().beginTransaction();
			this.getSession().saveOrUpdate(entity);
			tx.commit();
			log.debug("saveOrUpdate successful");
		} catch (RuntimeException re) {
			log.error("saveOrUpdate failed", re);
			throw re;
		}
	}

	public void saveOrUpdateAll(List<T> entities) {
		log.debug("saveOrUpdateAll " + this.clazz.getName() + " instances");
		try {
			//Transaction tx = getSession().beginTransaction();
			for (T entity : entities) {
				getSession().saveOrUpdate(entity);
			}
			//tx.commit();
			log.debug("save successful");
		} catch (RuntimeException re) {
			log.error("save failed", re);
			throw re;
		}
	}

	@SuppressWarnings("unchecked")
	public List<T> findByProperty(final String propertyName, final Object value) {
		log.debug("finding " + this.clazz.getName()
				+ " instance with property: " + propertyName + ", value: "
				+ value);
		try {
			Transaction tx = getSession().beginTransaction();
			String queryString = "from " + this.clazz.getName()
					+ " as model where model." + propertyName + "= " + value;
			List<T> list = (List<T>) getSession().createQuery(queryString)
					.list();
			tx.commit();
			return list;
		} catch (RuntimeException re) {
			log.error("find by property name failed", re);
			throw re;
		}
	}

	public List<T> executeNativeSqlCustomQuery(final String theQueryString) {
		System.out.println("executeNativeSqlCustomQuery for entity:  "
				+ this.clazz.getName() + " \nFull Query to be execute:\t"
				+ theQueryString);
		try {
			Transaction tx = getSession().beginTransaction();
			//new 
			// I do this because the result is built on full object reflection where it is possible
			// that the custom query string wont be holding the entire filed set @see Scalar queries : http://www.tutorialspoint.com/hibernate/hibernate_native_sql.htm
			SQLQuery query = getSession().createSQLQuery(theQueryString);
			query.setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP);
			List<?> results = query.list();

			tx.commit();
			//the next section will transform back to T object each of the row map field set 
			List<T> tempList = new ArrayList<T>();
			T typedInstance;
			Map<?, ?> row;
			try {
				for (Object object : results) {
					row = (Map<?, ?>) object;
					Set<?> keySet = row.keySet();
					typedInstance = clazz.newInstance();
					for (Object key : keySet) {
						Method getMethod = null, setMethod = null;

						getMethod = clazz
								.getMethod("get" + key, (Class[]) null);
						Class<?> retDataType = getMethod.getReturnType();
						setMethod = clazz.getMethod("set" + key, retDataType);
						Object value = row.get(key);
						if (value != null) {
							if (value.getClass().getName()
									.equals("java.lang.Integer")
									&& !key.equals("ID") // case - primary Key
									&& !key.equals("ID_HEVRA"))
								setMethod.invoke(typedInstance,
										Long.valueOf(row.get(key).toString()));
							else if (value.getClass().getName()
									.equals("java.lang.Integer")
									&& key.equals("ID"))
								setMethod.invoke(typedInstance, Integer
										.valueOf(row.get(key).toString()));
							else if (value.getClass().getName()
									.equals("java.math.BigDecimal"))
								setMethod.invoke(typedInstance,
										((java.math.BigDecimal) row.get(key))
												.doubleValue());
							else
								// java.lang.String | java.sql.Date 
								setMethod.invoke(typedInstance, row.get(key));
						}
					}
					tempList.add(typedInstance);

				}
			} catch (InstantiationException | IllegalAccessException
					| NoSuchMethodException | InvocationTargetException e) {
				e.printStackTrace();
			}
			return tempList;
			// old will work only with Entity queries (queries like: 'SELECT * From BlaBla WHERE(....)' )
			// return (List<T>) getSession().createSQLQuery(theQueryString)
			//		.addEntity(this.clazz).list();
		} catch (RuntimeException re) {
			log.error("executeCustomQuery failed", re);
			throw re;
		}
	}

	public void deleteAll(T[] entities) {
		log.debug("deleting " + this.clazz.getName() + " instance");
		try {
			Transaction tx = getSession().beginTransaction();

			for (int i = 0; i < entities.length; i++) {
				this.delete(entities[i]);
			}
			tx.commit();
			log.debug("deleteAll successful");
		} catch (RuntimeException re) {
			log.error("deleteAll failed", re);
			throw re;
		}
	}

	@SuppressWarnings("unchecked")
	public List<T> findWithNamedQuery(final String namedQueryName) {
		Transaction tx = getSession().beginTransaction();
		List<T> list = (List<T>) this.getSession()
				.getNamedQuery(namedQueryName).list();
		tx.commit();
		return list;
	}

	public void attachDirty(final T instance) {
		log.debug("attaching dirty " + this.clazz.getName() + " instance");
		try {
			Transaction tx = getSession().beginTransaction();
			getSession().saveOrUpdate(instance);
			log.debug("attach successful");
			tx.commit();
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}

	public void attachClean(T instance) {
		log.debug("attaching clean " + this.clazz.getName() + " instance");
		try {
			Transaction tx = getSession().beginTransaction();
			getSession().buildLockRequest(LockOptions.NONE).lock(instance);
			tx.commit();
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}
}
