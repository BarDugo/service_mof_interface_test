package com.novidea.mof.mail;

import org.junit.Test;

public class AmazonSESTest {

	static final String BODY = "This email was sent through the Amazon SES SMTP interface by using Java.";
	static final String SUBJECT = "Amazon SES **JUNIT** test (SMTP interface accessed using Java)";

	@Test
	public void testSendEmail() {
		ISendEmail sendEmail = new AmazonSES();
		sendEmail.sendEmail(SUBJECT, BODY);
	}

}
