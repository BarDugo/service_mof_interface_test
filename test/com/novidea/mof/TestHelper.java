package com.novidea.mof;

import static org.junit.Assert.assertEquals;

import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.lang.reflect.Field;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.io.IOUtils;
import org.joda.time.DateTime;
import org.junit.Test;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.google.gson.reflect.TypeToken;
import com.novidea.mof.calculations.result.GemelResult;
import com.novidea.mof.calculations.result.InsuranceResult;
import com.novidea.mof.hibernate.HibernateSessionFactory;
import com.novidea.mof.ws.MofInterfaceExpose;
import com.novidea.mof.ws.core.MofServiceImpl;
import com.sun.corba.se.impl.io.TypeMismatchException;

/**   
 * @author Galil.Zussman
 * @date   Jan 28, 2015
 */
public class TestHelper {

	Date startDate = (new DateTime(2013, 12, 1, 0, 0)).toDate();
	Date endDate = (new DateTime(2014, 11, 1, 0, 0)).toDate();

	public TestHelper() {
		super();
		HibernateSessionFactory.initTestConfig();
	}
	/**
	* Convert a JSON string to pretty print version
	* @param jsonString
	* @return
	*/
	public static String toPrettyFormat(String jsonString) {
		JsonParser parser = new JsonParser();
		JsonElement json = parser.parse(jsonString);

		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		String prettyJson = gson.toJson(json);

		return prettyJson;
	}

	@Test
	public void gemelTest() {
		MofInterfaceExpose mofInterfaceExpose = new MofServiceImpl();
		String gemelResultsJSON = mofInterfaceExpose.getGemelFundsByParamJSON(
				new ArrayList<Integer>(), "GemelDetailedReport", "", 0,
				startDate, endDate, 50000, MofProperties.get("PRIVATE_KEY"));

		Gson gson = new GsonBuilder().create();
		Type collectionTypeInsurance = new TypeToken<List<GemelResult>>() {
		}.getType();
		List<GemelResult> gemelResultsFromServer = gson.fromJson(
				gemelResultsJSON, collectionTypeInsurance);
		InputStream inputStream;
		StringWriter writer = new StringWriter();
		try {

			writer = new StringWriter();
			inputStream = TestHelper.class
					.getResourceAsStream("/GemelResults.json");
			IOUtils.copy(inputStream, writer);
			String theInputStreamString = writer.toString();
			List<GemelResult> gemelResultsFromFile = gson.fromJson(
					theInputStreamString, collectionTypeInsurance);

			// output Actual and Expected results 
			System.out.println("GemelResultsJSON CALCULATED:\t"
					+ gemelResultsJSON);
			System.out.println("gemelResultsFromServer CACHED:\t"
					+ gson.toJson(gemelResultsFromServer));

			assertEquals("results are not the same size!",
					gemelResultsFromFile.size(), gemelResultsFromServer.size());

			for (int i = 0; i < gemelResultsFromServer.size(); i++) {
				compareGeneralObject(gemelResultsFromServer.get(i),
						gemelResultsFromFile.get(i));
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Test
	public void insuranceTest() {
		MofInterfaceExpose mofInterfaceExpose = new MofServiceImpl();

		String insuranceResultsJSON = mofInterfaceExpose
				.getInsuranceFundsByParamJSON(new ArrayList<Integer>(),
						"InsuranceDetailedReport", "", 0, startDate, endDate,
						50000, MofProperties.get("PRIVATE_KEY"));

		Gson gson = new GsonBuilder().create();
		Type collectionTypeInsurance = new TypeToken<List<InsuranceResult>>() {
		}.getType();
		List<InsuranceResult> insuranceResultsFromServer = gson.fromJson(
				insuranceResultsJSON, collectionTypeInsurance);

		InputStream inputStream;
		StringWriter writer = new StringWriter();
		try {
			inputStream = TestHelper.class
					.getResourceAsStream("/InsuranceResults.json");
			IOUtils.copy(inputStream, writer);

			String theInputStreamString = writer.toString();
			List<InsuranceResult> insuranceResultsFromFile = gson.fromJson(
					theInputStreamString, collectionTypeInsurance);

			// output Actual and Expected results 
			System.out.println("InsuranceResultsJSON CALCULATED:\t"
					+ insuranceResultsJSON);
			System.out.println("InsuranceResultsFromServer CACHED:\t"
					+ theInputStreamString);

			assertEquals("results are not the same size!",
					insuranceResultsFromFile.size(),
					insuranceResultsFromServer.size());

			for (int i = 0; i < insuranceResultsFromServer.size(); i++) {
				compareGeneralObject(insuranceResultsFromServer.get(i),
						insuranceResultsFromFile.get(i));
			}

		} catch (IOException e) {
			e.printStackTrace();
		}
	}


	/**
	 * both parameters have to be from the same type
	 * @param thisObj
	 * @param thatObj
	 */
	@SuppressWarnings("rawtypes")
	void compareGeneralObject(Object thisObj, Object thatObj) {
		Class<?> clazz = thisObj.getClass();
		Field[] fields = clazz.getFields();

		for (Field field : fields) {
			System.out.println(field.getName());
			System.out.println(field.getType().getName());
			try {
				String typeName = field.getType().getName();
				if (typeName.contains("java.lang")
						|| typeName.contains("java.util.Date")) {// simple type field 
					assertEquals("field name: " + field.getName()
							+ " NOT EQUELS", field.get(thisObj),
							field.get(thatObj));
				} else if (typeName.contains("java.util.List")) { // list type field 
					assertEquals("field name: " + field.getName()
							+ " list size  NOT EQUELS",
							((java.util.List) field.get(thisObj)).size(),
							((java.util.List) field.get(thatObj)).size());
					for (int j = 0; j < ((java.util.List) field.get(thisObj))
							.size(); j++) {
						Object thisSubObj = ((java.util.List) field
								.get(thisObj)).get(j);
						Object thatSubObj = ((java.util.List) field
								.get(thatObj)).get(j);
						compareGeneralObject(thisSubObj, thatSubObj);
					}
				} else if (typeName.contains("com.novidea")
						&& !typeName.contains("TsuaRecord")) { // user type field 
					Object thisUserObj = field.get(thisObj);
					Object thatUserObj = field.get(thatObj);
					compareGeneralObject(thisUserObj, thatUserObj);
				} else if (typeName.contains("TsuaRecord")) {
					if (((com.novidea.mof.calculations.result.LastMonthResult.TsuaRecord[]) field
							.get(thisObj)) != null) {
						assertEquals(
								"field name: " + field.getName()
										+ " list size  NOT EQUELS",
								((com.novidea.mof.calculations.result.LastMonthResult.TsuaRecord[]) field
										.get(thisObj)).length,
								((com.novidea.mof.calculations.result.LastMonthResult.TsuaRecord[]) field
										.get(thatObj)).length);
						for (int j = 0; j < ((com.novidea.mof.calculations.result.LastMonthResult.TsuaRecord[]) field
								.get(thisObj)).length; j++) {
							Object thisSubObj = ((com.novidea.mof.calculations.result.LastMonthResult.TsuaRecord[]) field
									.get(thisObj))[j];
							Object thatSubObj = ((com.novidea.mof.calculations.result.LastMonthResult.TsuaRecord[]) field
									.get(thatObj))[j];
							compareGeneralObject(thisSubObj, thatSubObj);
						}
					}
				} else
					throw new TypeMismatchException("Unsupported type "
							+ typeName + " for field " + field.getName());

			} catch (IllegalArgumentException | IllegalAccessException e) {
				e.printStackTrace();
			}

		}
	}
}
