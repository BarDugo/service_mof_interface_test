-- prepare the Test DB 

DELETE FROM `mof_test`.`fund`
  WHERE  	
		NOT(	( ID = 101 AND ReportType ='GemelDetailedReport')
			 OR ( ID = 103 AND ReportType ='GemelDetailedReport')
			 OR ( ID = 106 AND ReportType ='GemelDetailedReport')
			 OR ( ID = 120 AND ReportType ='GemelDetailedReport')
			 OR ( ID = 124 AND ReportType ='GemelDetailedReport')
			 OR ( ID = 128 AND ReportType ='GemelDetailedReport')
			 OR ( ID = 1   AND ReportType ='InsuranceDetailedReport')
			 OR ( ID = 2   AND ReportType ='InsuranceDetailedReport')
			 OR ( ID = 6   AND ReportType ='InsuranceDetailedReport')
			 OR ( ID = 3   AND ReportType ='InsuranceDetailedReport')
			 OR ( ID = 46  AND ReportType ='InsuranceDetailedReport')
			 OR ( ID = 50  AND ReportType ='InsuranceDetailedReport')
			
			 OR ( ID = 131 AND ReportType ='PensionDetailedReport')
			 OR ( ID = 162 AND ReportType ='PensionDetailedReport')
             );
         
         
DELETE FROM `mof_test`.`asset_full_detailed_report`
  WHERE 
		NOT(
					( ID = 101 AND ReportType ='GemelAssetsFullDetailedReport')
				 OR ( ID = 103 AND ReportType ='GemelAssetsFullDetailedReport')
				 OR ( ID = 106 AND ReportType ='GemelAssetsFullDetailedReport')
				 OR ( ID = 120 AND ReportType ='GemelAssetsFullDetailedReport')
				 OR ( ID = 124 AND ReportType ='GemelAssetsFullDetailedReport')
				 OR ( ID = 128 AND ReportType ='GemelAssetsFullDetailedReport')
				 
				 OR ( ID = 1   AND ReportType ='InsuranceAssetsFullDetailedReport')
				 OR ( ID = 2   AND ReportType ='InsuranceAssetsFullDetailedReport')
				 OR ( ID = 6   AND ReportType ='InsuranceAssetsFullDetailedReport')
				 OR ( ID = 3   AND ReportType ='InsuranceAssetsFullDetailedReport')
				 OR ( ID = 46  AND ReportType ='InsuranceAssetsFullDetailedReport')
				 OR ( ID = 50  AND ReportType ='InsuranceAssetsFullDetailedReport')
			
				 OR ( ID = 131 AND ReportType ='PensionAssetsFullDetailedReport')
				 OR ( ID = 162 AND ReportType ='PensionAssetsFullDetailedReport')
         );

DELETE FROM `mof_test`.`asset_main_group_detailed_report`
  WHERE 
		NOT(
					( ID = 101 AND ReportType ='GemelAssetsMainGroupDetailedReport')
				 OR ( ID = 103 AND ReportType ='GemelAssetsMainGroupDetailedReport')
				 OR ( ID = 106 AND ReportType ='GemelAssetsMainGroupDetailedReport')
				 OR ( ID = 120 AND ReportType ='GemelAssetsMainGroupDetailedReport')
				 OR ( ID = 124 AND ReportType ='GemelAssetsMainGroupDetailedReport')
				 OR ( ID = 128 AND ReportType ='GemelAssetsMainGroupDetailedReport')
				
				 OR ( ID = 1   AND ReportType ='InsuranceAssetsMainGroupDetailedReport')
				 OR ( ID = 2   AND ReportType ='InsuranceAssetsMainGroupDetailedReport')
				 OR ( ID = 6   AND ReportType ='InsuranceAssetsMainGroupDetailedReport')
				 OR ( ID = 3   AND ReportType ='InsuranceAssetsMainGroupDetailedReport')
				 OR ( ID = 46  AND ReportType ='InsuranceAssetsMainGroupDetailedReport')
				 OR ( ID = 50  AND ReportType ='InsuranceAssetsMainGroupDetailedReport')
				 
				 OR ( ID = 131 AND ReportType ='PensionAssetsMainGroupDetailedReport')
				 OR ( ID = 162 AND ReportType ='PensionAssetsMainGroupDetailedReport')
         );
         
         