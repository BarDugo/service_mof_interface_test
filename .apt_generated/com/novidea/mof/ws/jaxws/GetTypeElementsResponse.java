
package com.novidea.mof.ws.jaxws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement(name = "getTypeElementsResponse", namespace = "http://ws.mof.novidea.com/")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getTypeElementsResponse", namespace = "http://ws.mof.novidea.com/")
public class GetTypeElementsResponse {

    @XmlElement(name = "TypeElements", namespace = "")
    private com.novidea.mof.calculations.result.TypeElements typeElements;

    /**
     * 
     * @return
     *     returns TypeElements
     */
    public com.novidea.mof.calculations.result.TypeElements getTypeElements() {
        return this.typeElements;
    }

    /**
     * 
     * @param typeElements
     *     the value for the typeElements property
     */
    public void setTypeElements(com.novidea.mof.calculations.result.TypeElements typeElements) {
        this.typeElements = typeElements;
    }

}
