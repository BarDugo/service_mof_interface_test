
package com.novidea.mof.ws.jaxws;

import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement(name = "getGemelFundsByParamResponse", namespace = "http://ws.mof.novidea.com/")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getGemelFundsByParamResponse", namespace = "http://ws.mof.novidea.com/")
public class GetGemelFundsByParamResponse {

    @XmlElement(name = "GemelResult", namespace = "")
    private List<com.novidea.mof.calculations.result.GemelResult> gemelResult;

    /**
     * 
     * @return
     *     returns List<GemelResult>
     */
    public List<com.novidea.mof.calculations.result.GemelResult> getGemelResult() {
        return this.gemelResult;
    }

    /**
     * 
     * @param gemelResult
     *     the value for the gemelResult property
     */
    public void setGemelResult(List<com.novidea.mof.calculations.result.GemelResult> gemelResult) {
        this.gemelResult = gemelResult;
    }

}
