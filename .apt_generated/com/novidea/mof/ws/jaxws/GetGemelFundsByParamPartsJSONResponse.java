
package com.novidea.mof.ws.jaxws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement(name = "getGemelFundsByParamPartsJSONResponse", namespace = "http://ws.mof.novidea.com/")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getGemelFundsByParamPartsJSONResponse", namespace = "http://ws.mof.novidea.com/")
public class GetGemelFundsByParamPartsJSONResponse {

    @XmlElement(name = "GEMEL_RESULT_PART_JSON", namespace = "", nillable = true)
    private String[] gemelRESULTPARTJSON;

    /**
     * 
     * @return
     *     returns String[]
     */
    public String[] getGemelRESULTPARTJSON() {
        return this.gemelRESULTPARTJSON;
    }

    /**
     * 
     * @param gemelRESULTPARTJSON
     *     the value for the gemelRESULTPARTJSON property
     */
    public void setGemelRESULTPARTJSON(String[] gemelRESULTPARTJSON) {
        this.gemelRESULTPARTJSON = gemelRESULTPARTJSON;
    }

}
