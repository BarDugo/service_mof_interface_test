
package com.novidea.mof.ws.jaxws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement(name = "getFundById", namespace = "http://ws.mof.novidea.com/")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getFundById", namespace = "http://ws.mof.novidea.com/", propOrder = {
    "id",
    "privateKey"
})
public class GetFundById {

    @XmlElement(name = "id", namespace = "")
    private String id;
    @XmlElement(name = "privateKey", namespace = "")
    private String privateKey;

    /**
     * 
     * @return
     *     returns String
     */
    public String getId() {
        return this.id;
    }

    /**
     * 
     * @param id
     *     the value for the id property
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * 
     * @return
     *     returns String
     */
    public String getPrivateKey() {
        return this.privateKey;
    }

    /**
     * 
     * @param privateKey
     *     the value for the privateKey property
     */
    public void setPrivateKey(String privateKey) {
        this.privateKey = privateKey;
    }

}
