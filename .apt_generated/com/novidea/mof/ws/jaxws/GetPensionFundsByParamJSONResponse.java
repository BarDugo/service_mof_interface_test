
package com.novidea.mof.ws.jaxws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement(name = "getPensionFundsByParamJSONResponse", namespace = "http://ws.mof.novidea.com/")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getPensionFundsByParamJSONResponse", namespace = "http://ws.mof.novidea.com/")
public class GetPensionFundsByParamJSONResponse {

    @XmlElement(name = "PensionResultsJSON", namespace = "")
    private String pensionResultsJSON;

    /**
     * 
     * @return
     *     returns String
     */
    public String getPensionResultsJSON() {
        return this.pensionResultsJSON;
    }

    /**
     * 
     * @param pensionResultsJSON
     *     the value for the pensionResultsJSON property
     */
    public void setPensionResultsJSON(String pensionResultsJSON) {
        this.pensionResultsJSON = pensionResultsJSON;
    }

}
