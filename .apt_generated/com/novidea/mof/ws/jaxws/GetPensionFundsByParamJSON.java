
package com.novidea.mof.ws.jaxws;

import java.util.Date;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement(name = "getPensionFundsByParamJSON", namespace = "http://ws.mof.novidea.com/")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getPensionFundsByParamJSON", namespace = "http://ws.mof.novidea.com/", propOrder = {
    "id",
    "reportType",
    "sug",
    "maslulHashkaa",
    "startDate",
    "endDate",
    "maxRecords",
    "privateKey"
})
public class GetPensionFundsByParamJSON {

    @XmlElement(name = "id", namespace = "")
    private List<Integer> id;
    @XmlElement(name = "reportType", namespace = "")
    private String reportType;
    @XmlElement(name = "sug", namespace = "")
    private String sug;
    @XmlElement(name = "maslulHashkaa", namespace = "")
    private Integer maslulHashkaa;
    @XmlElement(name = "startDate", namespace = "")
    private Date startDate;
    @XmlElement(name = "endDate", namespace = "")
    private Date endDate;
    @XmlElement(name = "maxRecords", namespace = "")
    private int maxRecords;
    @XmlElement(name = "privateKey", namespace = "")
    private String privateKey;

    /**
     * 
     * @return
     *     returns List<Integer>
     */
    public List<Integer> getId() {
        return this.id;
    }

    /**
     * 
     * @param id
     *     the value for the id property
     */
    public void setId(List<Integer> id) {
        this.id = id;
    }

    /**
     * 
     * @return
     *     returns String
     */
    public String getReportType() {
        return this.reportType;
    }

    /**
     * 
     * @param reportType
     *     the value for the reportType property
     */
    public void setReportType(String reportType) {
        this.reportType = reportType;
    }

    /**
     * 
     * @return
     *     returns String
     */
    public String getSug() {
        return this.sug;
    }

    /**
     * 
     * @param sug
     *     the value for the sug property
     */
    public void setSug(String sug) {
        this.sug = sug;
    }

    /**
     * 
     * @return
     *     returns Integer
     */
    public Integer getMaslulHashkaa() {
        return this.maslulHashkaa;
    }

    /**
     * 
     * @param maslulHashkaa
     *     the value for the maslulHashkaa property
     */
    public void setMaslulHashkaa(Integer maslulHashkaa) {
        this.maslulHashkaa = maslulHashkaa;
    }

    /**
     * 
     * @return
     *     returns Date
     */
    public Date getStartDate() {
        return this.startDate;
    }

    /**
     * 
     * @param startDate
     *     the value for the startDate property
     */
    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    /**
     * 
     * @return
     *     returns Date
     */
    public Date getEndDate() {
        return this.endDate;
    }

    /**
     * 
     * @param endDate
     *     the value for the endDate property
     */
    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    /**
     * 
     * @return
     *     returns int
     */
    public int getMaxRecords() {
        return this.maxRecords;
    }

    /**
     * 
     * @param maxRecords
     *     the value for the maxRecords property
     */
    public void setMaxRecords(int maxRecords) {
        this.maxRecords = maxRecords;
    }

    /**
     * 
     * @return
     *     returns String
     */
    public String getPrivateKey() {
        return this.privateKey;
    }

    /**
     * 
     * @param privateKey
     *     the value for the privateKey property
     */
    public void setPrivateKey(String privateKey) {
        this.privateKey = privateKey;
    }

}
