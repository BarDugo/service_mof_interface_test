
package com.novidea.mof.ws.jaxws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement(name = "refreshFunds", namespace = "http://ws.mof.novidea.com/")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "refreshFunds", namespace = "http://ws.mof.novidea.com/", propOrder = {
    "yearsBack",
    "privateKey"
})
public class RefreshFunds {

    @XmlElement(name = "yearsBack", namespace = "")
    private Integer yearsBack;
    @XmlElement(name = "privateKey", namespace = "")
    private String privateKey;

    /**
     * 
     * @return
     *     returns Integer
     */
    public Integer getYearsBack() {
        return this.yearsBack;
    }

    /**
     * 
     * @param yearsBack
     *     the value for the yearsBack property
     */
    public void setYearsBack(Integer yearsBack) {
        this.yearsBack = yearsBack;
    }

    /**
     * 
     * @return
     *     returns String
     */
    public String getPrivateKey() {
        return this.privateKey;
    }

    /**
     * 
     * @param privateKey
     *     the value for the privateKey property
     */
    public void setPrivateKey(String privateKey) {
        this.privateKey = privateKey;
    }

}
