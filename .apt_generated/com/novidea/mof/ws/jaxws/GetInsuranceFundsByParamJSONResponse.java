
package com.novidea.mof.ws.jaxws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement(name = "getInsuranceFundsByParamJSONResponse", namespace = "http://ws.mof.novidea.com/")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getInsuranceFundsByParamJSONResponse", namespace = "http://ws.mof.novidea.com/")
public class GetInsuranceFundsByParamJSONResponse {

    @XmlElement(name = "InsuranceResultsJSON", namespace = "")
    private String insuranceResultsJSON;

    /**
     * 
     * @return
     *     returns String
     */
    public String getInsuranceResultsJSON() {
        return this.insuranceResultsJSON;
    }

    /**
     * 
     * @param insuranceResultsJSON
     *     the value for the insuranceResultsJSON property
     */
    public void setInsuranceResultsJSON(String insuranceResultsJSON) {
        this.insuranceResultsJSON = insuranceResultsJSON;
    }

}
