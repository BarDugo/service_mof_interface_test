
package com.novidea.mof.ws.jaxws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement(name = "getGemelFundsByParamJSONResponse", namespace = "http://ws.mof.novidea.com/")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getGemelFundsByParamJSONResponse", namespace = "http://ws.mof.novidea.com/")
public class GetGemelFundsByParamJSONResponse {

    @XmlElement(name = "GemelResultsJSON", namespace = "")
    private String gemelResultsJSON;

    /**
     * 
     * @return
     *     returns String
     */
    public String getGemelResultsJSON() {
        return this.gemelResultsJSON;
    }

    /**
     * 
     * @param gemelResultsJSON
     *     the value for the gemelResultsJSON property
     */
    public void setGemelResultsJSON(String gemelResultsJSON) {
        this.gemelResultsJSON = gemelResultsJSON;
    }

}
