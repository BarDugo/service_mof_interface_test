
package com.novidea.mof.ws.jaxws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement(name = "getTypeElements", namespace = "http://ws.mof.novidea.com/")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getTypeElements", namespace = "http://ws.mof.novidea.com/", propOrder = {
    "reportType",
    "privateKey"
})
public class GetTypeElements {

    @XmlElement(name = "reportType", namespace = "")
    private String reportType;
    @XmlElement(name = "privateKey", namespace = "")
    private String privateKey;

    /**
     * 
     * @return
     *     returns String
     */
    public String getReportType() {
        return this.reportType;
    }

    /**
     * 
     * @param reportType
     *     the value for the reportType property
     */
    public void setReportType(String reportType) {
        this.reportType = reportType;
    }

    /**
     * 
     * @return
     *     returns String
     */
    public String getPrivateKey() {
        return this.privateKey;
    }

    /**
     * 
     * @param privateKey
     *     the value for the privateKey property
     */
    public void setPrivateKey(String privateKey) {
        this.privateKey = privateKey;
    }

}
