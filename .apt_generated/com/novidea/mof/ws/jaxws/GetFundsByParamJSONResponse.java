
package com.novidea.mof.ws.jaxws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement(name = "getFundsByParamJSONResponse", namespace = "http://ws.mof.novidea.com/")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getFundsByParamJSONResponse", namespace = "http://ws.mof.novidea.com/")
public class GetFundsByParamJSONResponse {

    @XmlElement(name = "FUNDS_JSON", namespace = "")
    private String fundsJSON;

    /**
     * 
     * @return
     *     returns String
     */
    public String getFundsJSON() {
        return this.fundsJSON;
    }

    /**
     * 
     * @param fundsJSON
     *     the value for the fundsJSON property
     */
    public void setFundsJSON(String fundsJSON) {
        this.fundsJSON = fundsJSON;
    }

}
