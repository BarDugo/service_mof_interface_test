
package com.novidea.mof.ws.jaxws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement(name = "getFundByIdResponse", namespace = "http://ws.mof.novidea.com/")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getFundByIdResponse", namespace = "http://ws.mof.novidea.com/")
public class GetFundByIdResponse {

    @XmlElement(name = "return", namespace = "")
    private com.novidea.mof.entities.model.Fund _return;

    /**
     * 
     * @return
     *     returns Fund
     */
    public com.novidea.mof.entities.model.Fund getReturn() {
        return this._return;
    }

    /**
     * 
     * @param _return
     *     the value for the _return property
     */
    public void setReturn(com.novidea.mof.entities.model.Fund _return) {
        this._return = _return;
    }

}
